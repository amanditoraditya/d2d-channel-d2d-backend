'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')
const Next = use('Adonis/Addons/Next')
const handler = Next.getRequestHandler()

Route.get('/dashboard', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/dashboard', query)
})

Route.get('/forbidden', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/forbidden', query)
})

Route.get('/users', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/users/index', query)
})
Route.get('/users/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/users/create', query)
})
Route.get('/users/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/users/edit', query)
})

Route.get('/role', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/role/index', query)
})
Route.get('/role/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/role/create', query)
})
Route.get('/role/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/role/edit', query)
})

Route.get('/setting', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/setting/index', query)
})
Route.get('/setting/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/setting/create', query)
})
Route.get('/setting/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/setting/edit', query)
})

Route.get('/activity-log', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/activity-log/index', query)
})


Route.get('/channel', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channel/index', query)
})
Route.get('/channel/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channel/create', query)
})
Route.get('/channel/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel/edit', query)
})

Route.get('/channelroles', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channelroles/index', query)
})
Route.get('/channelroles/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channelroles/create', query)
})
Route.get('/channelroles/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channelroles/edit', query)
})

Route.get('/menu', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/menu/index', query)
})

Route.get('/menu/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/menu/create', query)
})

Route.get('/menu/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/menu/edit', query)
})

// -------------------------- user channel ----------------------------------------------
Route.get('/user-channel', ({params, request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/user-channel/index', query)
})
// Route.get('/user-channel/detail/:channelid', ({params, request, response }) => {
// 	let qparams = {
// 		channelid: params.channelid
// 	}
// 	const query = qparams
// 	return Next.render(request.request, response.response, '/module/user-channel/index', query)
// })
Route.get('/user-channel/create', ({params, request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/user-channel/create', query)
})
Route.get('/user-channel/edit/:id', ({ params, request, response }) => {
	let qparams = {
		// channelid: params.channelid,
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/user-channel/edit', query)
})

// -------------------------- channel role ----------------------------------------------

Route.get('/channelroles', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channelroles/index', query)
})

Route.get('/channelroles/create', ({ request, response }) => {
	const query = qparams
	return Next.render(request.request, response.response, '/module/channelroles/create', query)
})

Route.get('/channelroles/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channelroles/edit', query)
})

// -------------------------- channel job --------------------------------------------------

Route.get('/channeljob', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channeljob/index', query)
})

Route.get('/channeljob/create', ({ request, response }) => {
	const query = qparams
	return Next.render(request.request, response.response, '/module/channeljob/create', query)
})

Route.get('/channeljob/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channeljob/edit', query)
})