'use strict'

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Http api routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route')

Route.group(() => {
	Route.get('/', ({ request, response }) => {
		response.header('Content-type', 'application/json')
		response.type('application/json')
		const data = {
			title: 'Welcome to Empty Engine',
			description: 'Empty Engine API Services',
			version: '1.0',
			copyright: 'PT Global Urban Esensial'
		}
		return response.send(data)
	})

	// Channel
	Route.get('channel', 'Api/V1/ChannelController.get').middleware(['apiauthenticator','apipreparation']).formats(['json'])
	Route.get('channel/:xid', 'Api/V1/ChannelController.get').middleware(['apiauthenticator','apipreparation']).formats(['json'])
	
	// Channel Subscriber
	Route.post('channel/:xid/subscription', 'Api/V1/ChannelSubscriberController.subscribe').middleware(['apiauthenticator','apipreparation']).formats(['json'])
	Route.delete('channel/:xid/subscription', 'Api/V1/ChannelSubscriberController.unsubscribe').middleware(['apiauthenticator','apipreparation']).formats(['json'])

	// Channel Conference
	Route.get('channel/conference/calendar/:tz', 'Api/V1/ChannelConferenceController.calendar').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
	Route.post('channel/conference/:xid/register', 'Api/V1/ChannelConferenceController.register').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
	Route.post('channel/conference/:xid/join', 'Api/V1/ChannelConferenceController.join').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
	Route.post('channel/conference/:xid/notify', 'Api/V1/ChannelConferenceController.notify').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])

	// Channel Forum
	Route.get('channel/forum/:tz', 'Api/V1/ChannelForumController.get').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
	Route.get('channel/forum/:xid/:tz', 'Api/V1/ChannelForumController.get').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
	Route.post('channel/forum/:xid/comment', 'Api/V1/ChannelForumController.comment').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
	Route.put('channel/forum/:xid/:action', 'Api/V1/ChannelForumController.action').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
	Route.put('channel/forum/comment/:xid/:action', 'Api/V1/ChannelForumController.commentaction').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
	Route.get('channel/forum/comment/:xid/reply/:tz', 'Api/V1/ChannelForumController.getreply').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
	Route.get('channel/forum/comment/:xid/:tz', 'Api/V1/ChannelForumController.getcommentdetail').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
	Route.get('channel/forum/:xid/comment/:tz', 'Api/V1/ChannelForumController.getcomment').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
	
	// Channel Job
	Route.get('channel/job/:tz', 'Api/V1/ChannelJobController.get').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
	Route.get('channel/job/:xid/:tz', 'Api/V1/ChannelJobController.get').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
	Route.post('channel/job/:xid', 'Api/V1/ChannelJobController.apply').middleware(['apiauthenticator','channelauthenticator','apipreparation']).formats(['json'])
})
.prefix('open-api/v1')