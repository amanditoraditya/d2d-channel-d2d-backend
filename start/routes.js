'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Next = use('Adonis/Addons/Next')
const handler = Next.getRequestHandler()

Route.group(() => {
	Route.get('/', ({ request, response }) => {
		response.header('Content-type', 'application/json')
		response.type('application/json')
		let data = {
			title: 'Welcome to Empty Engine',
			description: 'Empty Engine API Services',
			version: '1.0',
			copyright: 'PT Global Urban Esensial'
		}
		return response.send(data)
	})
	
	Route.post('/login', 'AccountController.login').formats(['json'])
	Route.post('/login-by-name', 'AccountController.loginByName').formats(['json'])
	Route.post('/checklogin', 'AccountController.checklogin').formats(['json'])
}).prefix('api/v1')

Route.group(() => {
	Route.post('/checkauth', 'AccountController.checkauth').formats(['json'])
	Route.post('/checkpass', 'AccountController.checkpass').formats(['json'])
	
	Route.get('/users/get-user', 'UserController.getUser').formats(['json'])
	Route.post('/users/datatable', 'UserController.datatable').formats(['json'])
	Route.post('/users/create', 'UserController.create').formats(['json'])
	Route.post('/users/edit', 'UserController.edit').formats(['json'])
	Route.post('/users/update/:id', 'UserController.update').formats(['json'])
	Route.post('/users/delete', 'UserController.delete').formats(['json'])
	Route.post('/users/multidelete', 'UserController.multidelete').formats(['json'])
	
	Route.get('/role/get-role', 'RoleController.getRole').formats(['json'])
	Route.get('/role/get-module-list', 'RoleController.getModuleList').formats(['json'])
	Route.post('/role/datatable', 'RoleController.datatable').formats(['json'])
	Route.post('/role/create', 'RoleController.create').formats(['json'])
	Route.post('/role/edit', 'RoleController.edit').formats(['json'])
	Route.post('/role/update/:id', 'RoleController.update').formats(['json'])
	Route.post('/role/delete', 'RoleController.delete').formats(['json'])
	Route.post('/role/multidelete', 'RoleController.multidelete').formats(['json'])
	
	Route.post('/setting/get-setting', 'SettingController.getSetting').formats(['json'])
	Route.post('/setting/get-setting-by-key', 'SettingController.getSettingByKey').formats(['json'])
	Route.post('/setting/datatable', 'SettingController.datatable').formats(['json'])
	Route.post('/setting/create', 'SettingController.create').formats(['json'])
	Route.post('/setting/edit', 'SettingController.edit').formats(['json'])
	Route.post('/setting/update/:id', 'SettingController.update').formats(['json'])
	Route.post('/setting/update-with-file/:id', 'SettingController.updateWithFile').formats(['json'])
	Route.post('/setting/delete', 'SettingController.delete').formats(['json'])
	Route.post('/setting/multidelete', 'SettingController.multidelete').formats(['json'])
	
	Route.post('/activity-log/datatable', 'SystemLogController.datatable').formats(['json'])

	Route.get('/channel/get-channel-type', 'ChannelController.getChannelType').formats(['json'])
	Route.get('/channel/get-channels', 'ChannelController.getChannels').formats(['json'])
	Route.post('/channel/datatable', 'ChannelController.datatable').formats(['json'])
	Route.post('/channel/create', 'ChannelController.create').formats(['json'])
	Route.post('/channel/edit', 'ChannelController.edit').formats(['json'])
	Route.post('/channel/update/:id', 'ChannelController.update').formats(['json'])
	Route.post('/channel/delete', 'ChannelController.delete').formats(['json'])
	Route.post('/channel/multidelete', 'ChannelController.multidelete').formats(['json'])
	
	Route.get('/menu/get-menu', 'MenuController.getMenu').formats(['json'])
	Route.post('/menu/datatable', 'MenuController.datatable').formats(['json'])
	Route.post('/menu/create', 'MenuController.create').formats(['json'])
	Route.post('/menu/edit', 'MenuController.edit').formats(['json'])
	Route.post('/menu/update/:id', 'MenuController.update').formats(['json'])
	Route.post('/menu/delete', 'MenuController.delete').formats(['json'])
	Route.post('/menu/multidelete', 'MenuController.multidelete').formats(['json'])

	// ----------------------- user channel --------------------------------
	Route.post('/user-channel/datatable', 'UserChannelController.datatable').formats(['json'])
	Route.post('/user-channel/create', 'UserChannelController.create').formats(['json'])
	Route.post('/user-channel/edit', 'UserChannelController.edit').formats(['json'])
	Route.post('/user-channel/update/:id', 'UserChannelController.update').formats(['json'])
	Route.post('/user-channel/delete', 'UserChannelController.delete').formats(['json'])
	Route.post('/user-channel/multidelete', 'UserChannelController.multidelete').formats(['json'])
	Route.get('/user-channel/get-channel-role-list/:channelid', 'UserChannelController.getChannelRoleList').formats(['json'])

	// ----------------------- channel role --------------------------------

	Route.get('/channelroles/get-channel-menu', 'ChannelRolesController.getChannelMenu').formats(['json'])
	Route.get('/channelroles/get-channel', 'ChannelRolesController.getChannel').formats(['json'])
	Route.post('/channelroles/datatable', 'ChannelRolesController.datatable').formats(['json'])
	Route.post('/channelroles/create', 'ChannelRolesController.create').formats(['json'])
	Route.post('/channelroles/edit', 'ChannelRolesController.edit').formats(['json'])
	Route.post('/channelroles/update/:id', 'ChannelRolesController.update').formats(['json'])
	Route.post('/channelroles/delete', 'ChannelRolesController.delete').formats(['json'])
	Route.post('/channelroles/multidelete', 'ChannelRolesController.multidelete').formats(['json'])

	// ----------------------- channel job ---------------------------------
	
	Route.post('/channeljob/datatable', 'ChannelJobController.datatable').formats(['json'])
	Route.post('/channeljob/create', 'ChannelJobController.create').formats(['json'])
	Route.post('/channeljob/edit', 'ChannelJobController.edit').formats(['json'])
	Route.post('/channeljob/update/:id', 'ChannelJobController.update').formats(['json'])
	Route.post('/channeljob/delete', 'ChannelJobController.delete').formats(['json'])
	Route.post('/channeljob/multidelete', 'ChannelJobController.multidelete').formats(['json'])

}).prefix('api/v1')
.middleware('auth')
.middleware('roles')
.middleware('globalparam')

Route.get('/logout', 'AccountController.logout')

Route.get('/service-worker.js', ({ request, response }) => {
	response.download('./next/.next/service-worker.js')
})

Route.get('/manifest.json', ({ request, response }) => {
	response.download('./next/public/static/one/vendor/manifest.json')
})

Route.get('/manifest.webmanifest', ({ request, response }) => {
	response.download('./next/public/static/one/vendor/manifest.webmanifest')
})

Route.get('/OneSignalSDKWorker.js', ({ request, response }) => {
	response.download('./next/public/static/one/vendor/OneSignalSDKWorker.js')
})

Route.get('/OneSignalSDKUpdaterWorker.js', ({ request, response }) => {
	response.download('./next/public/static/one/vendor/OneSignalSDKUpdaterWorker.js')
})

Route.get('/downloads', ({ request, response }) => {
	let req = request.get()
	let path = req.path
	let file = req.file
	response.download('public/uploads/' + path + file)
})

Route.get('/static/uploads/images/:file', ({ params, request, response }) => {
	response.download('./next/public/static/uploads/images/' + params.file)
})

// Router for Next.js
require('./next.js')
// Router for API
require('./api.js')

Route.get('*', ({ request, response }) =>
	new Promise((resolve, reject) => {
		handler(request.request, response.response, promise => {
			promise.then(resolve).catch(reject)
		})
	})
)