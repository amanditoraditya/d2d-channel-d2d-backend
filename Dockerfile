FROM node:12.20.1-alpine3.10

# Create app directory
ENV HOME=/apps/engine/
RUN mkdir -p /apps/engine/

WORKDIR $HOME

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json $HOME
# COPY .env $HOME

RUN npm i -g pm2
RUN npm i -g @adonisjs/cli
RUN npm install --save

# Bundle app source
COPY . .

RUN npm run build

EXPOSE 5858

# CMD [ "npm", "start" ]
CMD [ "pm2-runtime", "server.yml" ]