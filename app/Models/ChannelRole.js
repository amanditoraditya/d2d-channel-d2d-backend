'use strict'

const Model = use('Model')

class ChannelRole extends Model {
	static get table() {
		return 'channel_roles'
	}
	
	// static get rules() {
	// 	return {
	// 		role_id: 'required',
	// 		user_id: 'required'
	// 	}
	// }
	users() {
		return this
			.belongsToMany('App/Models/UserChannel')
			.pivotModel('App/Models/UserChannelRole')
	}
}

module.exports = ChannelRole