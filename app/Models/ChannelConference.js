'use strict'

const Model = use('Model')

class ChannelConference extends Model {

    _v1_channel_conference_attendees(){
		return this
			.hasOne('App/Models/ChannelConferenceAttendee', 'id', 'conference_id')
	}

}

module.exports = ChannelConference