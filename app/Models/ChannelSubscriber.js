'use strict'

const Model = use('Model')

class ChannelSubscriber extends Model {
    _v1_member(){
		return this
		.belongsTo('App/Models/TemporaryMember', 'uid', 'uid')
	}
	
}

module.exports = ChannelSubscriber