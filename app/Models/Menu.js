'use strict'

const Model = use('Model')

class Menu extends Model {
	channels() {
		return this
			.belongsToMany('App/Models/Channel')
			.pivotModel('App/Models/ChannelMenu')
	}

	_v1_channel_menus() {
		return this
			.hasOne('App/Models/ChannelMenu')
	}
}

module.exports = Menu