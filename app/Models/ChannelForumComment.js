'use strict'

const Model = use('Model')

class ChannelForumComment extends Model {

    _v1_channel_forum_like(){
		return this
		.hasOne('App/Models/ChannelForumLike', 'id', 'ref_id')
	}

	_v1_channel_forum_post(){
		return this
		.belongsTo('App/Models/ChannelForumPost')
	}

	_v1_channel_subscriber(){
		return this
		.with('_v1_member')
		.belongsTo('App/Models/ChannelSubscriber')
	}

	_v1_forum_comment_children_preview(){
		return this
		.hasMany('App/Models/ChannelForumComment', 'id', 'parent_id')
		.with('_v1_channel_subscriber')
		.where('is_spammed', 0)
		.where('is_archived', 0)
		.limit(1)
		.orderBy('id', 'desc')
	}

	_v1_forum_comment_parent(){
		return this
		.belongsTo('App/Models/ChannelForumComment', 'parent_id', 'id')
		.where('is_spammed', 0)
		.where('is_archived', 0)
		.where('parent_id', 0)
	}

}

module.exports = ChannelForumComment