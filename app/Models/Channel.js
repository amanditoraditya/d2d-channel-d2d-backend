'use strict'

const Model = use('Model')

class Channel extends Model {
		
	// static get rules() {
	// 	return {
	// 		role_id: 'required',
	// 		user_id: 'required'
	// 	}
	// }
	menus() {
		return this
			.belongsToMany('App/Models/Menu')
			.pivotModel('App/Models/ChannelMenu')
	}

	_v1_channelsubscribers(){
		return this
		.hasMany('App/Models/ChannelSubscriber')
	}

	_v1_channel_type(){
		return this
		.belongsTo('App/Models/ChannelType')
	}

	_v1_channel_menus(){
		return this
		.hasMany('App/Models/ChannelMenu')
	}

	_v1_channel_subscriber(){
		return this
			.hasOne('App/Models/ChannelSubscriber')
	}

	_v1_channel_banners(){
		return this
		.hasMany('App/Models/ChannelBanner')
		.where('is_archived', 0)
		.where('is_active', 1)
	}
}

module.exports = Channel