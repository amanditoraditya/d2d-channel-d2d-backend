'use strict'

const Model = use('Model')

class ChannelJob extends Model {

    _v1_channel_job_type(){
		return this
			.belongsTo('App/Models/ChannelJobType', 'job_type_id', 'id')
	}

	_v1_channel_job_specs(){
		return this
			.hasMany('App/Models/ChannelJobSpec')
	}

	_v1_channel_job_applicant(){
		return this
			.hasOne('App/Models/ChannelJobApplicant')
	}

}

module.exports = ChannelJob