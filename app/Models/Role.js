'use strict'

const Model = use('Model')

class Role extends Model {
	static get rules() {
		return {
			role_title: 'required|unique:roles',
			role_slug: 'required|unique:roles'
		}
	}

	/*
	|--------------------------------------------------------------------------
	| Relationship Methods
	|--------------------------------------------------------------------------
	*/
	/**
	 * Many-To-Many Relationship Method for accessing the Role.users
	 *
	 * @return Object
	*/
	users() {
		return this.belongsToMany('App/Model/User')
	}
}

module.exports = Role