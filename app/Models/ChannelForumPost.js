'use strict'

const Model = use('Model')

class ChannelForumPost extends Model {

    _v1_channel(){
		return this
		.belongsTo('App/Models/Channel')
	}

    _v1_channel_forum_attachments(){
		return this
		.hasMany('App/Models/ChannelForumAttachment', 'id', 'ref_id')
		.orderBy('id', 'desc')
	}

	_v1_channel_forum_like(){
		return this
		.hasOne('App/Models/ChannelForumLike', 'id', 'ref_id')
	}

}

module.exports = ChannelForumPost