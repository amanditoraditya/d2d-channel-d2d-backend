'use strict'
//MODELS
const ChannelSubscriber = use('App/Models/ChannelSubscriber')
// MODULES
const jwt = require('jsonwebtoken')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const ApiResponseHelper = use('App/Controllers/Http/Helper/ApiResponse')
//STATICS
const Dictionary = require('../../resources/statics/dictionary')

class ChannelAuthenticator {
	async handle ({ params, request, response }, next) {
		let success = false
		let options = {
			type : 'ERR_500',
			response_type : 'obj',
			reason : Dictionary.TR000008,
			exec_time : new Date().getTime()
		}
		try {
			const sbc = request.header('X-SBC')
			const decoded = jwt.verify(sbc, params.usersession.sec)
			if(decoded._scc) {
				params.usersession.xid = decoded._scc
				let _subscriber = await ChannelSubscriber.query()
				.where('uid',  params.usersession.uid)
				.where('xid',  params.usersession.xid)
				.where('is_unsubscribed',  0)
				.where('blocked',  0)
				.first()
				if(_subscriber){
					if(_subscriber.blocked){
						options.reason  = await MicroH.reason('TR000015', '')
					} else {
						params.usersession = _subscriber
						success = true
					}
				}
				if(success){
					await next()
				} else {
					options.type = 'ERR_AUTH_AUTH'
					const rspdata = await ApiResponseHelper.classify(options)
					response.status(rspdata.responseheader.code).json(rspdata.content)
				}
			} else {
				options.type = 'ERR_403'
				const rspdata = await ApiResponseHelper.classify(options)
				response.status(rspdata.responseheader.code).json(rspdata.content)
			}
		} catch(err) {
			options.type = 'ERR_403'
			options.reason  = await MicroH.reason('TR000001', err.message)
			const rspdata = await ApiResponseHelper.classify(options)
			response.status(rspdata.responseheader.code).json(rspdata.content)
		}
	}
}

module.exports = ChannelAuthenticator
