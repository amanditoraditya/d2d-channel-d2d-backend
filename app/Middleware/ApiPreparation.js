'use strict'
//STATICS
const Dictionary = require('../../resources/statics/dictionary')

class ApiPreparation {
	async handle ({ params }, next) {
		const _options = {
			type : 'ERR_500',
			response_type : 'obj',
			reason : Dictionary.TR000000,
			exec_time : new Date().getTime()
		}
		params.options = Object.assign({}, _options)
		await next()
	}
}

module.exports = ApiPreparation
