'use strict'

const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const MyHash = require('./Helper/Hash.js')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const Helpers = use('Helpers')
const Env = use('Env')
const Logger = use('Logger')

class SettingController {
	async datatable({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Setting', 'read', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'settings',
			sSelectSql: ['id', 'groups', 'options', 'value'],
			aSearchColumns: ['groups', 'options', 'value']
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			let id = select[0][x]['id']
			fdata.push([
				"<div class='text-center'>\
					<input type='checkbox' id='titleCheckdel' />\
					<input type='hidden' class='deldata' name='item[]' value='"+ await MyHash.encrypt(id.toString()) +"' disabled />\
				</div>\n",
				select[0][x]['id'],
				select[0][x]['groups'],
				select[0][x]['options'],
				select[0][x]['value'],
				"<div class='text-center'>\
					<div class='btn-group btn-group-sm'>\
						<a href='javascript:void(0);' data-href='/module/setting-pos/edit?id="+ await MyHash.encrypt(id.toString()) +"' data-as='/setting-pos/edit/"+ await MyHash.encrypt(id.toString()) +"' class='btn btn-sm btn-primary btn-edit' title='Edit'><i class='fa fa-pencil'></i></a>\
						<a href='javascript:void(0);' class='btn btn-sm btn-danger alertdel' id='"+ await MyHash.encrypt(id.toString()) +"' title='Delete'><i class='fa fa-times'></i></a>\
					</div>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Setting', 'create', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const { groups, options, value } = request.only(['groups', 'options', 'value'])
		
		let formData = {
			groups: groups,
			options: options,
			value: value,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			groups: 'required',
			options: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			try {
				await Setting.create(formData)
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Setting added',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Setting cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async edit({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Setting', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		let setting= await Setting.find(await MyHash.decrypt(formData.id))
		
		if (setting) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Setting found',
				data: setting
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Setting cannot be found',
				data: []
			}
			return response.send(data)
		}
	}
	
	async update({params, request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Setting', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const { groups, options, value } = request.only(['groups', 'options', 'value'])
		
		let setting= await Setting.find(await MyHash.decrypt(params.id))
		
		let formData = {
			groups: groups,
			options: options,
			value: value,
			updated_by: auth.user.id
		}
		
		let rules = {
			groups: 'required',
			options: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			if (setting) {
				await Setting.query().where('id', await MyHash.decrypt(params.id)).update(formData)
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Setting updated',
					data: []
				}
				return response.send(data)
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Setting cannot be found',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async updateWithFile({params, request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Setting', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const UpFile = request.file('value', {
			type: 'image',
			subtype: ['image/jpeg', 'image/png'],
			size: '2mb',
			extnames: ['jpg', 'jpeg', 'png']
		})
		let formData = request.post()
		
		let setting= await Setting.find(await MyHash.decrypt(params.id))
		
		let formDataIns = {
			groups: formData.groups,
			options: formData.options
		}
		
		let rules = {
			groups: 'required',
			options: 'required'
		}
		
		const validation = await validateAll(formDataIns, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			if (setting) {
				if (UpFile.type == 'image' && (UpFile.subtype == 'jpeg' || UpFile.subtype == 'png')) {
					let newfilename = Math.floor(Math.random() * 99999) + '-' + UpFile.clientName.toLowerCase().replace(/\s/g, '-')
					await UpFile.move(Helpers.appRoot() + '/next/public/static/uploads/images/', {
						name: newfilename
					})

					if (!UpFile.moved()) {
						let error = UpFile.error()
						
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let data = {
							code: '4004',
							message: error.message,
							data: []
						}
						return response.send(data)
					} else {
						await Setting.query().where('id', await MyHash.decrypt(params.id)).update({
							groups: formData.groups,
							options: formData.options,
							value: newfilename,
							updated_by: auth.user.id
						})
						
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let data = {
							code: '2000',
							message: 'Setting updated',
							data: {
								name: newfilename
							}
						}
						return response.send(data)
					}
				} else {
					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '4004',
						message: 'Please upload image file',
						data: []
					}
					return response.send(data)
				}
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Setting cannot be found',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async delete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Setting', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		let setting= await Setting.find(await MyHash.decrypt(formData.id))
		if (setting){
			try {
				await setting.delete()
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Setting success deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Setting cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Setting cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
	
	async multidelete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Setting', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			for (let i in dataitem) {
				let setting= await Setting.find(await MyHash.decrypt(dataitem[i]))
				try {
					await setting.delete()
				} catch (e) {}
			}
			
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Setting success deleted',
				data: []
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Setting cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
	
	async getSetting({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Setting', 'read', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		let setting= await Setting.query().where('groups', formData.groups).fetch()
		
		if (setting) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Setting found',
				data: setting.toJSON()
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Setting cannot be found',
				data: []
			}
			return response.send(data)
		}
	}
	
	async getSettingByKey({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Setting', 'read', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		let setting= await Setting.query().where('options', formData.key).first()
		
		if (setting) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Setting found',
				data: setting.value
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Setting cannot be found',
				data: ''
			}
			return response.send(data)
		}
	}
}

module.exports = SettingController