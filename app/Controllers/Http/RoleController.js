'use strict'

const Role = use('App/Models/Role')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const MyHash = require('./Helper/Hash.js')
const Database = use('Database')
const fs = require('fs')
const path = require('path')
const Helpers = use('Helpers')
const _lo = use('lodash')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const Env = use('Env')
const Logger = use('Logger')

class RoleController {
	async datatable({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Role', 'read', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'roles',
			sSelectSql: ['id', 'role_title', 'role_slug'],
			aSearchColumns: ['role_title', 'role_slug']
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			let id = select[0][x]['id']
			fdata.push([
				"<div class='text-center'>\
					<label class='checkbox checkbox-lg checkbox-inline'>\
						<input type='checkbox' id='titleCheckdel' />\
						<span></span>\
					</label>\
					<input type='hidden' class='deldata' name='item[]' value='"+ await MyHash.encrypt(id.toString()) +"' disabled />\
				</div>\n",
				select[0][x]['id'],
				select[0][x]['role_title'],
				select[0][x]['role_slug'],
				"<div class='text-center'>\
					<div class='btn-group btn-group-sm'>\
						<a href='javascript:void(0);' data-href='/module/role/edit?id="+ await MyHash.encrypt(id.toString()) +"' data-as='/role/edit/"+ await MyHash.encrypt(id.toString()) +"' class='btn btn-sm btn-primary btn-edit' title='Edit'><i class='fas fa-pencil-alt'></i></a>\
						<a href='javascript:void(0);' class='btn btn-sm btn-danger alertdel' id='"+ await MyHash.encrypt(id.toString()) +"' title='Delete'><i class='far fa-trash-alt'></i></a>\
					</div>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
	
	async getModuleList({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Role', 'read', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let lfiles = []
		let directoryPath = path.join(__dirname, '')
		const files = fs.readdirSync(directoryPath).map(f => f.replace('Controller.js', ''))
		lfiles.push(...files)
		
		let allFiles = []
		for (let i = 0; i < lfiles.length; i++) {
			if (lfiles[i] == 'Helper' || lfiles[i] == 'Api' || lfiles[i] == 'Module' || lfiles[i] == 'Error' || lfiles[i] == 'Merchant' || lfiles[i] == 'RequestDemo') {
			} else {
				allFiles.push(lfiles[i])
			}
		}
		
		response.header('Content-type', 'application/json')
		response.type('application/json')
		let data = {
			code: '2000',
			message: 'Module found',
			data: allFiles
		}
		return response.send(data)
	}
	
	async create({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Role', 'create', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.all()
		
		let roles = []
		let items = formData.item
		for(let x in items) {
			roles.push({
				component: items[x]['component'] ? items[x]['component'] : '-',
				create: items[x]['create'] ? '1' : '0',
				read: items[x]['read'] ? '1' : '0',
				update: items[x]['update'] ? '1' : '0',
				delete: items[x]['delete'] ? '1' : '0'
			})
		}
		
		let datas = {
			role_title: formData.role_title,
			role_slug: formData.role_slug,
			role_access: JSON.stringify(roles),
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			role_title: 'required',
			role_slug: 'required'
		}
		
		const validation = await validateAll(datas, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			try {
				await Role.create(datas)
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Role added',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Role cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async edit({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Role', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		let role = await Role.find(await MyHash.decrypt(formData.id))
		
		let lfiles = []
		let directoryPath = path.join(__dirname, '')
		const files = fs.readdirSync(directoryPath).map(f => f.replace('Controller.js', ''))
		lfiles.push(...files)

		let allFiles = []
		let itemFusion = []
		let itemFusion2 = []
		let itemFusion3 = []
		let roleState = role.role_access == '' || role.role_access == null ? false : JSON.parse(role.role_access)

		if (!roleState) {
			for (let i = 0; i < lfiles.length; i++) {
				if (lfiles[i] == 'Helper' || lfiles[i] == 'Api' || lfiles[i] == 'Module' || lfiles[i] == 'Error' || lfiles[i] == 'Merchant' || lfiles[i] == 'RequestDemo') {
				} else {
					allFiles.push({
						component: lfiles[i],
						create: '',
						read: '',
						update: '',
						delete: ''
					})
				}
			}
		} else {
			for (let x in roleState) {
				if (lfiles.indexOf(roleState[x]['component']) == -1) {
					roleState.splice(roleState[x], 1)
				}
				itemFusion2.push(roleState[x])
			}
			
			for (let i = 0; i < lfiles.length; i++) {
				if (lfiles[i] == 'Helper' || lfiles[i] == 'Api' || lfiles[i] == 'Module' || lfiles[i] == 'Error' || lfiles[i] == 'Merchant' || lfiles[i] == 'RequestDemo') {
				} else {
					itemFusion3.push({
						component: lfiles[i],
						create: '',
						read: '',
						update: '',
						delete: ''
					})
				}
			}

			for (let x in itemFusion3) {
				let chckFus = _lo.find(itemFusion2, { component: itemFusion3[x]['component'] })
				if (_lo.size(chckFus) == 0) {
					itemFusion.push(itemFusion3[x])
				}
			}
			
			allFiles = itemFusion2.concat(itemFusion)
		}

		if (role) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Role found',
				data: {
					role: role,
					files: _lo.sortBy(allFiles, ['component'])
				}
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Role cannot be found',
				data: []
			}
			return response.send(data)
		}
	}
	
	async update({params, request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Role', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.all()
		
		let roles = []
		let items = formData.item
		for(let x in items) {
			roles.push({
				component: items[x]['component'] ? items[x]['component'] : '-',
				create: items[x]['create'] ? '1' : '0',
				read: items[x]['read'] ? '1' : '0',
				update: items[x]['update'] ? '1' : '0',
				delete: items[x]['delete'] ? '1' : '0'
			})
		}
		
		let role = await Role.find(await MyHash.decrypt(params.id))
		
		let datas = {
			role_title: formData.role_title,
			role_slug: formData.role_slug,
			role_access: JSON.stringify(roles),
			updated_by: auth.user.id
		}
		
		let rules = {
			role_title: 'required',
			role_slug: 'required'
		}
		
		const validation = await validateAll(datas, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			if (role) {
				await Role.query().where('id', await MyHash.decrypt(params.id)).update(datas)
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Role updated',
					data: []
				}
				return response.send(data)
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Role cannot be found',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async delete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Role', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		let role = await Role.find(await MyHash.decrypt(formData.id))
		if (role){
			try {
				await role.delete()
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Role success deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Role cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Role cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
	
	async multidelete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Role', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			for (let i in dataitem) {
				let role = await Role.find(await MyHash.decrypt(dataitem[i]))
				try {
					await role.delete()
				} catch (e) {}
			}
			
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Role success deleted',
				data: []
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Role cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
	
	async getRole({request, response, auth, session}) {
		let req = request.get()
		let datas
		let sessionUser = session.get('users_roles')
		if (req.phrase != '') {
			if (sessionUser[0] == 'superadmin') {
				datas = await Role.query().where('role_title', 'LIKE', '%' + req.phrase + '%').limit(20).fetch()
			} else {
				datas = await Role.query().where('role_title', 'LIKE', '%' + req.phrase + '%').whereNot('role_slug', 'superadmin').limit(20).fetch()
			}
		} else {
			if (sessionUser[0] == 'superadmin') {
				datas = await Role.query().limit(20).fetch()
			} else {
				datas = await Role.query().whereNot('role_slug', 'superadmin').limit(20).fetch()
			}
		}
		let data = datas.toJSON()
		
		let result = []
		for(let i in data){
			result.push({
				value: data[i]['id'],
				label: data[i]['role_title']
			})
		}
		
		return response.send(result)
	}
}

module.exports = RoleController