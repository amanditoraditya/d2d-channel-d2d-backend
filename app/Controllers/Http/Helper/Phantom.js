const Helpers = use('Helpers')

module.exports = function (options) {
    var self = {
		url: options.url,
		filename: options.filename
	}
	
	var spawn = require('child_process').spawn;
	var args = [Helpers.appRoot() + '/phantom.js', self.url, self.filename];
	var options = {};
	
	var phantomExecutable = 'phantomjs';
	
	function Uint8ArrToString(myUint8Arr){
		return String.fromCharCode.apply(null, myUint8Arr);
	};
	
	var child = spawn(phantomExecutable, args, options);
	
	child.stdout.on('data', function(data) {
		var textData = Uint8ArrToString(data);
		//console.log(textData);
	});
	
	child.stderr.on('data', function(err) {
		var textErr = Uint8ArrToString(err);
		//console.log(textErr);
	});
	
	child.on('close', function(code) {
		//console.log('Process closed with status code: ' + code);
	});
}