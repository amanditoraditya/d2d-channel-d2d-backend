'use strict'

const Logger = use('Logger')
const StaticResponse = require('../../../../resources/statics/responses')

class ApiResponse {
	async classify(options) {
		let data
		try {
			data = {
				responseheader : {
					code : 500
				},
				content : {
					header : {
						process_time : new Date().getTime() - options.exec_time,
						message: "Internal Server Error",
						reason: "No response for type " + options.type,
						code: "ERQ-03-001"
					}
				}
			}

			if(StaticResponse[options.type]) {
				const Static = StaticResponse[options.type]
				data.responseheader.code = Static.header.code
				data.content.header.code = Static.body.code
				data.content.header.message = Static.body.message
				if (!options.reason){
					data.content.header.reason = ""
				} else {
					data.content.header.reason = options.reason
				}
			}
			
			if(options.response_type === "arr") {
				data.content.data = []
			} else {
				data.content.data = {}
			}

			if(options.dataset) {
				data.content.data = options.dataset
			}

		} catch(e) {
			data = {
				responseheader : {
					code : 500
				},
				content : {
					header : {
						process_time : 0,
						message: "Internal Server Error",
						reason:  e.message,
						code: "ERQ-03-001"
					},
					data : {}
				}
			}
		}

		return data
	}
}

module.exports = new ApiResponse