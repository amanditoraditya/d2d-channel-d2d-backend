'use strict'

const User = use('App/Models/User')
const Logger = use('Logger')
const Env = use('Env')
const _lo = use('lodash')
const moment = require('moment')
const ApiService = require('./ApiService.js')

class Notif {
	async sendNotifPushByUserId(content) {
		const options = {
			headers: {
				Authorization: 'Basic MzE1ZTJhOWUtNDRmZi00ZmU5LWIzZDAtZTQ3ODlmOGZkNzYz'
			}
		}
		
		const reqparam = {
			app_id: '8dda1c51-1b2f-46db-864c-785241be4fe5',
			contents: {
				en: content.message,
				id: content.message
			},
			headings: {
				en: content.header,
				id: content.header
			},
			data: content.data,
			channel_for_external_user_ids: 'push',
			include_external_user_ids: content.user_id,
			template_id: '4b71f498-b22d-4500-90ba-da0e2b01ae68'
		}
		
		return await ApiService.getData('https://onesignal.com/api/v1/notifications', 'POST', reqparam, options)
	}
	
	async sendNotifPushByEmail(content) {
		const options = {
			headers: {
				Authorization: 'Basic MzE1ZTJhOWUtNDRmZi00ZmU5LWIzZDAtZTQ3ODlmOGZkNzYz'
			}
		}
		
		const reqparam = {
			app_id: '8dda1c51-1b2f-46db-864c-785241be4fe5',
			contents: {
				en: content.message,
				id: content.message
			},
			headings: {
				en: content.header,
				id: content.header
			},
			tags: [{
				field: 'tag',
				key: 'email',
				relation: '=',
				value: content.email
			}],
			data: content.data,
			template_id: '4b71f498-b22d-4500-90ba-da0e2b01ae68'
		}
		
		return await ApiService.getData('https://onesignal.com/api/v1/notifications', 'POST', reqparam, options)
	}
}

module.exports = new Notif