'use strict'
const uuid = require('uuid')
const _ = require('lodash')
const Dictionary = require('../../../../resources/statics/dictionary')
// MODULES
const Validation = require('joi')
const uaparser = require('ua-parser-js')
const geoip = require('geoip-country')

class MicroHelper {
	async validation(data, auth_schema) {
		let validate
		try{
			let validation = {
				success : false,
				data : {},
				message : ''
			}
			let validationprc = auth_schema.validate(data)
			if(validationprc.error) {
				validation.message  = 'Err validate process ' + validationprc.error.details[0].message
			} else {
				validation.success = true
				validation.data = validationprc.value
			}
			return validation
			validate = validationprc
		} catch (e){
			validate = {
				success : false,
				data : {},
				message : 'Err validate process ' + e.message
			}
		}
		return validate
	}

	async reason(reasonstr, messagestr) {
		let reason = Object.assign({}, Dictionary[reasonstr]) 
		let message = messagestr
		if (!reason) {
			reason = Dictionary.GTR000000
			message = reasonstr
		}
		for (const key in reason) { reason[key] = reason[key] + message }
		return reason
	}

	async perepareData(data, usersession, exclude, dolog){
		let rtrn = {}
		try{
			let uid = uuid.v1()
			if(data.uid){
				uid = data.uid
			}
			rtrn = {
				uid : uid,
				is_active : 1,
				others : "{}",
				created_by : usersession.id,
				updated_by : usersession.id
			}
			rtrn = _.merge(rtrn, data)
			if(dolog){
				rtrn.self_logs = JSON.stringify(rtrn)
			}
			if(exclude){
				for(const key in exclude){
					const _key = exclude[key]
					delete rtrn[_key]
				}
			}
		} catch (e) {
			console.log(e)
			rtrn._error = e.message
		}
		return rtrn
	}

	async detectagent(request) {
		let data = {}
		data = {
			access_country : '-',
			ip : '-',
			browser : '-',
			device : '-',
			os : '-',
			os_version : '-',
			engine_name : '-',
			cpu_architecture : '-',
			general_ua : '-'
		}
		try{
			let uastring = request.header('User-Agent')
			if(!uastring){
				uastring = request.headers['user-agent']
			}
			if(uastring){
				data.general_ua = uastring
				let result = uaparser(uastring)
				if(result){
					if(result.browser.name){ data.browser = result.browser.name}
					if(result.device.vendor){ data.device = result.device.vendor}
					if(result.os.name){ 
						data.os = result.os.name
						if(result.os.version){ data.os_version = result.os.version }
					}
					if(result.engine.name){ data.engine_name = result.engine.name }
					if(result.cpu.architecture){ data.cpu_architecture = result.cpu.architecture }
				}
			}
			let parseIp = '-'
			try{
				if(request.header('x-forwarded-for')){
					parseIp = request.header('x-forwarded-for')
				}
				if(request.ip()){
					parseIp = request.ip()
				}
			}catch(e){console.log(e)}

			if(parseIp){
				data.ip = parseIp
				const geo = geoip.lookup(parseIp)
				if(geo){
					if(geo.country){
						data.access_country = geo.country
					}
				}
			}
		}catch(e){
			console.log(e)
		}
		return data
	}
}

module.exports = new MicroHelper