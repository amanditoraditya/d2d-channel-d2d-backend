'use strict'

const ChannelMenus = use('App/Models/ChannelMenus')
const ChannelRoleMenus = use('App/Models/ChannelRoleMenus')
const Channel 		= use('App/Models/Channel')
const ChannelRoles	= use('App/Models/ChannelRoles')
const MyHash 		 = require('./Helper/Hash.js')
const Database 		 = use('Database')
const fs 			 = require('fs')
const oss 			 = require('ali-oss');
const path 			 = require('path')
const Helpers 		 = use('Helpers')
const _lo 			 = use('lodash')
const QueryBuilder 	 = require('./Helper/DatatableBuilder.js')
const CheckAuth 	 = require('./Helper/CheckAuth.js')
const Env 			 = use('Env')
const Logger 		 = use('Logger')
const moment		 = require('moment');
const { v4: uuidv4 } = require('uuid');
const goaConfig 	 = require('../../../next/config.js')
const environment 	 = goaConfig.NODE_ENV
const crypto 		 = require('crypto')
const { validate, validateAll } = use('Validator')

class ChannelRolesController {
	async datatable({request, response, auth, session}) {
		let admin_page = true
		let checkAuth = await CheckAuth.get('ChannelRoles', 'read', auth, admin_page)

		let conditions = []
		let from =  "channel_roles \
		LEFT JOIN channels ON channel_roles.channel_id = channels.id LEFT JOIN channel_role_menus ON channel_role_menus.channel_role_id = channel_roles.id LEFT JOIN channel_menus ON channel_menus.id = channel_role_menus.channel_menu_id LEFT JOIN menus ON  menus.id = channel_menus.menu_id"

		if (!checkAuth) {
			conditions = [`channel_role_menus.is_archived = 0 AND channel_roles.is_archived = 0 AND user_channels.xid = '${auth.user.xid}'`]
			from += '\ LEFT JOIN user_channels ON channels.id = user_channels.channel_id'
		} else {
			conditions = ['channel_role_menus.is_archived = 0 AND channel_roles.is_archived = 0']
		}
		
		const formData = request.post()

		let tableDefinition = {
			sTableName: 'channel_roles',
			sSelectSql: ['channel_role_menus.id', 'channel_roles.name as channel_role_name', 'channels.channel_name', 'menus.name as menu_name', 'channel_role_menus.status'],
			aSearchColumns: ['channel_roles.name', 'channels.channel_name', 'menus.name', 'channel_role_menus.status'],
			sWhereAndSql: conditions,
			sFromSql: from
		}
			
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			let id = select[0][x]['id']
			let content = ''
			if (checkAuth) {
				content  = [
				"<div class='text-center'>\
					<label class='checkbox checkbox-lg checkbox-inline'>\
						<input type='checkbox' id='titleCheckdel' />\
						<span></span>\
					</label>\
					<input type='hidden' class='deldata' name='item[]' value='"+ await MyHash.encrypt(id.toString()) +"' disabled />\
				</div>\n",
				select[0][x]['id'],
				select[0][x]['channel_role_name'],
				select[0][x]['channel_name'],
				select[0][x]['menu_name'],
				select[0][x]['status'],
				"<div class='text-center'>\
					<div class='btn-group btn-group-sm'>\
						<a href='javascript:void(0);' data-href='/module/channelroles/edit?id="+ await MyHash.encrypt(id.toString()) +"' data-as='/channelroles/edit/"+ await MyHash.encrypt(id.toString()) +"' class='btn btn-sm btn-primary btn-edit' title='Edit'><i class='fas fa-pencil-alt'></i></a>\
						<a href='javascript:void(0);' class='btn btn-sm btn-danger alertdel' id='"+ await MyHash.encrypt(id.toString()) +"' title='Delete'><i class='far fa-trash-alt'></i></a>\
					</div>\
				</div>\n"]
			} else {
				content  = [
				"<div class='text-center'>\
					<label class='checkbox checkbox-lg checkbox-inline'>\
						<input type='checkbox' id='titleCheckdel' />\
						<span></span>\
					</label>\
					<input type='hidden' class='deldata' name='item[]' value='"+ await MyHash.encrypt(id.toString()) +"' disabled />\
				</div>\n",
				select[0][x]['id'],
				select[0][x]['channel_role_name'],
				select[0][x]['menu_name'],
				select[0][x]['status'],
				"<div class='text-center'>\
					<div class='btn-group btn-group-sm'>\
						<a href='javascript:void(0);' data-href='/module/channelroles/edit?id="+ await MyHash.encrypt(id.toString()) +"' data-as='/channelroles/edit/"+ await MyHash.encrypt(id.toString()) +"' class='btn btn-sm btn-primary btn-edit' title='Edit'><i class='fas fa-pencil-alt'></i></a>\
						<a href='javascript:void(0);' class='btn btn-sm btn-danger alertdel' id='"+ await MyHash.encrypt(id.toString()) +"' title='Delete'><i class='far fa-trash-alt'></i></a>\
					</div>\
				</div>\n"]
			}

			fdata.push(content)
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
	

	async create({request, response, auth, session}) {
		let datas2
		let channelid
		let checkAuth = await CheckAuth.get('ChannelRoles', 'create', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		let fields = [
			'name',
			'channel_id',
			'channel_menu_id',
			'read',
			'write',
			'update',
			'delete2',
			'status',
		]
		const { 
			name,
			channel_id,
			channel_menu_id,
			read,
			write,
			update,
			delete2,
			status 
		} = request.only(fields)
		
		if (checkAuth){
			try{
				channelid =  await MyHash.decrypt(channel_id)
			}catch(e){
				channelid = channel_id
			}
		}
		else
		{
			channelid = auth.user.channel_id
		}
		
		let tempxid = uuidv4()
		let formData = {
			xid: tempxid,
			name,
			channel_id: channelid,
			status,			
			updated_by: auth.user.id,
			updated_at: currentTime,
			created_by: auth.user.id,
			created_at: currentTime
		}
		
		let rules = {
			name: 'required',
			channel_id: 'required',
			status: 'required',
			//channel_menu_id: 'required'
		}

		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			try {
				await ChannelRoles.create(formData)

				datas2 = await ChannelRoles.query().where('xid', 'LIKE', '%' + tempxid + '%').limit(20).fetch()
				
				let data2 = datas2.toJSON()		

				let formData2 = {
						xid: uuidv4(),
						channel_role_id: data2[0]['id'],
						channel_menu_id,
						read,
						write,
						update,
						delete: delete2,
						status,			
						updated_by: auth.user.id,
						updated_at: currentTime,
						created_by: auth.user.id,
						created_at: currentTime
				}

				await ChannelRoleMenus.create(formData2)
			
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Roles added',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Roles cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async edit({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('ChannelRoles', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
	
		let channelroles = await ChannelRoleMenus.query()
		.select('channel_role_menus.id', 'channel_role_menus.xid', 'channel_role_menus.channel_role_id',
		'channel_role_menus.read', 'channel_role_menus.write', 'channel_role_menus.update', 'channel_role_menus.delete',
		'channel_role_menus.status', 'channel_role_menus.created_by', 'channel_role_menus.updated_by','channel_role_menus.created_at', 
		'channel_role_menus.updated_at', 'channel_roles.name as channel_roles_name', 'channels.channel_name', 'channels.id as channel_id',
		'menus.name as menu_name', 'channel_menus.id as channel_menu_id')
		.leftJoin('channel_roles', 'channel_roles.id', 'channel_role_menus.channel_role_id')
		.leftJoin('channels', 'channels.id', 'channel_roles.channel_id')
		.leftJoin('channel_menus', 'channel_menus.id', 'channel_role_menus.channel_menu_id')
		.leftJoin('menus', 'menus.id', 'channel_menus.menu_id')
		.where('channel_role_menus.id', await MyHash.decrypt(formData.id))
		.first()
		
		if (channelroles) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Channel Roles found',
				data: channelroles
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Channel Roles cannot be found',
				data: []
			}
			return response.send(data)
		}
	}
	
	async update({params, request, response, auth, session}) {
		let datas2
		let checkAuth = await CheckAuth.get('ChannelRoles', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		let currentTime = moment().format('YYYY-MM-	DD kk:mm:ss');

		let fields = [
			'name',
			'channel_role_id',
			'channel_id',
			'channel_menu_id',
			'read',
			'write',
			'update',
			'delete2',
			'status'
		]

		const { 
			name,
			channel_role_id,
			channel_id,
			channel_menu_id,
			read,
			write,
			update,
			delete2,
			status
		} = request.only(fields)

		let channelroles2 = await ChannelRoles.find(await channel_role_id)
		let formData = {
			name,
			channel_id,
			status,			
			updated_by: auth.user.id,
			updated_at: currentTime
		}
		
		let rules = {
			name: 'required',
			channel_id: 'required',
			status: 'required'
		}

		const validation = await validateAll(formData, rules)
		Logger.info('validation is %j', validation)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			if (channelroles2) {
				let id = await MyHash.decrypt(params.id)

				await ChannelRoles.query().where('id', await channel_role_id).update(formData)

				let cr = await ChannelRoleMenus.query()
							.where('id', id)
							.fetch()
				cr = _lo.isEmpty(cr) ? [] : cr.toJSON()
				id = _lo.isEmpty(id) ? [] : _lo.isArray(id)? id : [id];

				for (let row of cr) {
					//menu berkurang
					if (!id.some(item => item.value == row.id)) {
						await ChannelRoleMenus.query()
							.where('id', id)
							.andWhere('channel_role_id', channel_role_id)
							.update({
								updated_by: auth.user.id,
								updated_at: currentTime,
								is_archived:1
							})

						id = id.filter(function( obj ) {
							return obj.value != row.id;
						});
					} 
				}

				// menu bertambah
				if (cr.is_archived == 1) {
					await ChannelRoleMenus.query()
						.where('id', id)
						.andWhere('channel_role_id', channel_role_id)
						.update({
							is_archived		: 0, 
							updated_by		: auth.user.id,
							updated_at		: currentTime
						})
				} 
				else
				{
					let xid = uuidv4()
					let channel_role_menus = {
						xid,
						channel_role_id,
						channel_menu_id,
						read,
						write,
						update,
						delete: delete2,
						status,
						is_archived		: 0,
						updated_by		: auth.user.id,
						updated_at		: currentTime,
						created_by		: auth.user.id,
						created_at		: currentTime
					}
					await ChannelRoleMenus.query().where('id', id).update(channel_role_menus)
				}
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Roles updated',
					data: []
				}
				return response.send(data)
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Roles cannot be updated',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async delete({request, response, auth, session}) {
		let channelroledata
		let channelrole
		let checkAuth = await CheckAuth.get('ChannelRoles', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		const formData = request.post()
		let channelrolesmenu = await ChannelRoleMenus.find(await MyHash.decrypt(formData.id))
		if (channelrolesmenu){
			try {
				channelroledata = await ChannelRoleMenus.query().select('channel_role_id').where('id', await MyHash.decrypt(formData.id)).limit(20).fetch()
				await ChannelRoleMenus.query().where('id', await MyHash.decrypt(formData.id)).update({
					is_archived:1,
					updated_by: auth.user.id,
					updated_at: currentTime
				})
				
				channelrole = channelroledata.toJSON()

				await ChannelRoles.query().where('id', channelrole[0]['channel_role_id']).update({
					is_archived:1,
					updated_by: auth.user.id,
					updated_at: currentTime
				})

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Channel Roles Menu deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel Roles Menu cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Channel Roles Menu cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
	
	async multidelete({request, response, auth, session}) {
		let channelroledata
		let channelrole
		let checkAuth = await CheckAuth.get('ChannelRoles', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
				try {
					for (let i in dataitem) {
					let channelrolesmenu = await ChannelRoleMenus.find(await MyHash.decrypt(dataitem[i]))
					
					channelroledata = await ChannelRoleMenus.query().select('channel_role_id').where('id', await MyHash.decrypt(dataitem[i])).limit(20).fetch()
					await ChannelRoleMenus.query().where('id', await MyHash.decrypt(dataitem[i])).update({
						is_archived:1,
						updated_by: auth.user.id,
						updated_at: currentTime})
					
					channelrole = channelroledata.toJSON()

					await ChannelRoles.query().where('id', channelrole[i]['channel_role_id']).update({
						is_archived:1,
						updated_by: auth.user.id,
						updated_at: currentTime})
					}

					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '2000',
						message: 'Channel Roles Menu deleted',
						data: []
					}	
					return response.send(data)
				
				} catch (e) {
					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '4004',
						message: 'Channel Roles Menu cannot be deleted',
						data: []
					}
					return response.send(data)
				}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Select Channel Roles Menu to delete',
				data: []
			}
			return response.send(data)
		}
	}

	async getChannel({request, response, auth, session}) {		
		let admin_page = true
		let checkAuth = await CheckAuth.get('ChannelRoles', 'read', auth, admin_page)
		let req = request.get()
		let datas

		if (req.phrase != '') {
			if (!checkAuth) {
				datas = await Channel.query().where('is_admin', 'LIKE', '%0%').limit(20).fetch()
			} else {
				datas = await Channel.query().limit(20).fetch()
			}
		} 
		else 
		{
			if (!checkAuth) {
				datas = await Channel.query().select('channels.id','channels.channel_name').leftJoin('user_channels', 'channels.id', 'user_channels.channel_id').where('channels.is_archived', 'LIKE', '%0%').andWhere('channels.is_admin', 'LIKE', '%0%').andWhere('user_channels.xid', 'LIKE', `${auth.user.xid}` ).limit(20).fetch()
			} else {
				datas = await Channel.query().where('channels.is_archived', 'LIKE', '%0%').limit(20).fetch()
			}
		}

		let data = datas.toJSON()

		let result = []
		for(let i in data){
			result.push({
				value: data[i]['id'],
				label: data[i]['channel_name']
			})
		}
		return response.send(result)
	}

	async getChannelMenu({request, response, auth, session}) {
		let req = request.all()
		let datas
		let checkAuth = await CheckAuth.get('ChannelRoles', 'read', auth, true)

		//console.log(auth.user)
		//console.log(checkAuth)
		if(!checkAuth)
		{
			datas = await ChannelMenus.query().select('channel_menus.id', 'menus.name').leftJoin('menus', 'menus.id', 'channel_menus.menu_id').where('channel_menus.is_archived', 'LIKE', '%0%').andWhere('channel_menus.channel_id', 'LIKE', auth.user.channel_id).limit(20).fetch()
		}
		else
		{
			if (req.phrase != '') {
				datas = await ChannelMenus.query().select('channel_menus.id', 'menus.name').leftJoin('menus', 'menus.id', 'channel_menus.menu_id').where('channel_menus.is_archived', 'LIKE', '%0%').andWhere('channel_menus.channel_id', 'LIKE', req.channel_id).limit(20).fetch()
			} else {
				datas = await ChannelMenus.query().select('channel_menus.id', 'menus.name').leftJoin('menus', 'menus.id', 'channel_menus.menu_id').limit(20).fetch()
			}
		}
		
		let data = datas.toJSON()

		let result = []
		for(let i in data){
			result.push({
				value: data[i]['id'],
				label: data[i]['name']
			})
		}

		return response.send(result)
	}
}

module.exports = ChannelRolesController