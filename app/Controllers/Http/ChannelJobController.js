'use strict'

const Database 		= use('Database')
const fs 			= require('fs')
const path 			= require('path')
const Helpers 		= use('Helpers')
const _lo 			= use('lodash')
const Env 			= use('Env')
const Logger 		= use('Logger')
const moment		= require('moment');
const Hash 			= use('Hash')
const MyHash 		= require('./Helper/Hash.js')
const { v4: uuidv4 } = require('uuid');

const Channel 			= use('App/Models/Channel')
const ChannelJob   	    = use('App/Models/ChannelJob')
const QueryBuilder 		= require('./Helper/DatatableBuilder.js')
const CheckAuth 		= require('./Helper/CheckAuth.js')

const { validate, validateAll } = use('Validator')

class ChannelJobController {
	async datatable({request, response, auth, session}) {
		let checkIsAdmin = await CheckAuth.get('ChannelJob', 'read', auth, true)

	}
	
	async create({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('ChannelJob', 'create', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
	}
	
	async edit({request, response, auth, session}) {
		let checkAuth 	 = await CheckAuth.get('ChannelJob', 'update', auth, false)

		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
	}
	
	async update({params, request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
	}
	
	async delete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

	}
	
	async multidelete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

	}
}

module.exports = ChannelJobController