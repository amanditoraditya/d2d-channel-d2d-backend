'use strict'

const Database 		= use('Database')
const fs 			= require('fs')
const path 			= require('path')
const Helpers 		= use('Helpers')
const _lo 			= use('lodash')
const Env 			= use('Env')
const Logger 		= use('Logger')
const moment		= require('moment');
const Hash 			= use('Hash')
const MyHash 		= require('./Helper/Hash.js')
const { v4: uuidv4 } = require('uuid');

const Channel 			= use('App/Models/Channel')
const UserChannel   	= use('App/Models/UserChannel')
const ChannelRole   	= use('App/Models/ChannelRole')
const UserChannelRole   = use('App/Models/UserChannelRole')
const QueryBuilder 		= require('./Helper/DatatableBuilder.js')
const CheckAuth 		= require('./Helper/CheckAuth.js')

const { validate, validateAll } = use('Validator')

class UserChannelController {
	async datatable({request, response, auth, session}) {
		let checkIsAdmin = await CheckAuth.get('UserChannel', 'read', auth, true)
		// let checkAuth = await CheckAuth.get('UserChannel', 'read', auth, false)

		// if (!checkAuth) {
		// 	response.header('Content-type', 'application/json')
		// 	response.type('application/json')
		// 	let data = {
		// 		code: '4003',
		// 		message: 'User cannot access this module',
		// 		data: []
		// 	}
		// 	return response.send(data)
		// }

		let fdata 			= [];
		let recordsTotal 	= 0;
		let recordsFiltered = [];
		const formData 		= request.post()
		
		let data			= {
			draw: formData.draw,
			recordsTotal: 0,
			recordsFiltered: [],
			data:[]
		};

		let tableDefinition = {
			sTableName: 'user_channels uc',
			sSelectSql: ['uc.id', 'uc.xid', 'uc.channel_id',`concat(uc.first_name,' ', uc.last_name) as name`, 'uc.email' , 'uc.block'],
			aSearchColumns: ['name', 'email'],
		}

		if (checkIsAdmin) {
			tableDefinition.sWhereAndSql = [`uc.is_archived = 0`]
		} else {
			tableDefinition.sWhereAndSql = [`uc.is_archived = 0 AND uc.channel_id = ${auth.user.channel_id}`]
		}
		let queryBuilder = new QueryBuilder(tableDefinition)
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		recordsTotal = await Database.raw(queries.recordsTotal)
		recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let no = 0
		for(let row of select[0]) {
			let id = row['id']
			let content = ''
			if (checkIsAdmin) {
				content  = ["<div class='text-center'>\
						    	<label class='checkbox checkbox-lg checkbox-inline'>\
						    		<input type='checkbox' id='titleCheckdel' />\
						    		<span></span>\
						    	</label>\
						    	<input type='hidden' class='deldata' name='item[]' value='"+ await MyHash.encrypt(id.toString()) +"' disabled />\
						    </div>\n",
						    row['id'],
						    row['name'],
						    row['channel_id'],
						    row['email'],
						    row['block'],
						    "<div class='text-center'>\
						    	<div class='btn-group btn-group-sm'>\
						    		<a href='javascript:void(0);' data-href='/module/user-channel/edit?"+
									"id="+ await MyHash.encrypt(id.toString())+"' data-as='/user-channel/edit/"+ await MyHash.encrypt(id.toString()) +
									"' class='btn btn-sm btn-primary btn-edit' title='Edit'><i class='fas fa-pencil-alt'></i></a>\
						    		<a href='javascript:void(0);' class='btn btn-sm btn-danger alertdel' id='"+ await MyHash.encrypt(id.toString()) +
									"' title='Delete'><i class='far fa-trash-alt'></i></a>\
						    	</div>\
						    </div>\n" ]
			} else {
				content  = ["<div class='text-center'>\
						    	<label class='checkbox checkbox-lg checkbox-inline'>\
						    		<input type='checkbox' id='titleCheckdel' />\
						    		<span></span>\
						    	</label>\
						    	<input type='hidden' class='deldata' name='item[]' value='"+ await MyHash.encrypt(id.toString()) +"' disabled />\
						    </div>\n",
						    row['id'],
						    row['name'],
						    row['email'],
						    row['block'],
						    "<div class='text-center'>\
						    	<div class='btn-group btn-group-sm'>\
						    		<a href='javascript:void(0);' data-href='/module/user-channel/edit?"+
									"id="+ await MyHash.encrypt(id.toString())+"' data-as='/user-channel/edit/"+await MyHash.encrypt(id.toString()) +
									"' class='btn btn-sm btn-primary btn-edit' title='Edit'><i class='fas fa-pencil-alt'></i></a>\
						    		<a href='javascript:void(0);' class='btn btn-sm btn-danger alertdel' id='"+ await MyHash.encrypt(id.toString()) +
									"' title='Delete'><i class='far fa-trash-alt'></i></a>\
						    	</div>\
						    </div>\n"]
			}

			fdata.push(content)
			no++
		}
		data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		// console.log(fdata);
	
		return data;
	}
	
	async create({request, response, auth, session}) {
		// let checkAuth = await CheckAuth.get('UserChannel', 'create', auth)
		let checkIsAdmin = await CheckAuth.get('UserChannel', 'create', auth, true)
		// if (!checkAuth) {
		// 	response.header('Content-type', 'application/json')
		// 	response.type('application/json')
		// 	let data = {
		// 		code: '4003',
		// 		message: 'User cannot access this module',
		// 		data: []
		// 	}
		// 	return response.send(data)
		// }

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');

		let form = JSON.parse(request.post().data);
		let xid = uuidv4()

		let channel_id
		//if channelid not encrypted just use it
		if (checkIsAdmin) {
			try{
				channel_id = await MyHash.decrypt(form.channel_id)
			}catch(e){
				channel_id = form.channel_id
			}
		} else {
			channel_id = auth.user.channel_id
		}

		let formData = {
			xid,
			channel_id,
			email: form.email,
			password: await Hash.make(form.password),
			first_name: form.first_name,
			last_name: form.last_name,
			block: form.block,
			is_archived: '0',
			updated_by: auth.user.id,
			updated_at: currentTime,
			created_by: auth.user.id,
			created_at: currentTime
		}
		
		let rules = {
			first_name: 'required',
			last_name: 'required',
			email: 'required',
			password: 'required',
		}
		
		const validation = await validateAll(formData, rules)

		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			try {
				let newUserChannel = await UserChannel.create(formData)
				let channel_roles = form.channel_role_id
				
				for (let row of channel_roles) {
					let xid = uuidv4()
					let channel_role_data = {
						xid				: xid,
						user_channel_id	: newUserChannel.id,
						channel_role_id	: row.value,
						status			: 1,
						is_archived		: 0,
						updated_by		: auth.user.id,
						updated_at		: currentTime,
						created_by		: auth.user.id,
						created_at		: currentTime
					}
					await UserChannelRole.create(channel_role_data)
				}
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Channel added',
					data: []
				}
				return response.send(data)
			} catch (e) {
				console.log(e)
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async edit({request, response, auth, session}) {
		// let checkIsAdmin = await CheckAuth.get('UserChannel', 'update', auth)
		let checkAuth 	 = await CheckAuth.get('UserChannel', 'update', auth, false)

		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		// let channel	= await Channel.find(await MyHash.decrypt(formData.id))
		let userchannel = await UserChannel.query()
									.select('user_channels.*')
									.where('user_channels.id', await MyHash.decrypt(formData.id))
									.first()
		let get_roles = await userchannel.roles().where('user_channel_roles.is_archived', '0').fetch()
		let channel_roles_json = get_roles.toJSON()
		let channel_roles = []
		for(let i in channel_roles_json){
			channel_roles.push({
				value: channel_roles_json[i]['id'],
				label: channel_roles_json[i]['name']
			})
		}

		if (!_lo.isEmpty(channel_roles)) {
			userchannel.channel_roles = channel_roles
		}

		let get_channels = await userchannel.channels().where('channels.is_archived', '0').fetch()
		let channels_json = get_channels.toJSON()
		let channels = []
		if (!_lo.isEmpty(channels_json)) {
			channels.push({
				value: channels_json['id'],
				label: channels_json['channel_name']
			})
			userchannel.channels = channels
		}

		if (userchannel) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'User Channel found',
				data: userchannel
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'User Channel cannot be found',
				data: []
			}
			return response.send(data)
		}
	}
	
	async update({params, request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		let form = JSON.parse(request.post().data);
		
		let userChannel = await UserChannel.find(await MyHash.decrypt(params.id))
		let formData = {
			xid : form.xid,
			channel_id: form.channel_id,
			first_name: form.first_name,
			last_name: form.last_name,
			email: form.email,
			is_archived: form.is_archived,
			block:form.block,			
			updated_by: auth.user.id,
			updated_at: currentTime
		}
		
		let rules = {
			first_name: 'required',
			last_name: 'required',
			email: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			if (userChannel) {
				let id = await MyHash.decrypt(params.id)
				let updateUserChannel = await UserChannel.query().where('id', id).update(formData)
				let channel_roles = form.channel_roles;
				// delete old channel_menu
				let cr = await UserChannelRole.query()
							.where('user_channel_id', id)
							.fetch()
				cr = _lo.isEmpty(cr) ? [] : cr.toJSON()
				// channel_roles = _lo.isEmpty(channel_roles) ? [] : _lo.isArray(channel_roles)? channel_roles : [channel_roles];
				channel_roles = _lo.isEmpty(channel_roles) ? [] : channel_roles
				for (let row of cr) {
					//menu berkurang
					if (!channel_roles.some(item => item.value == row.channel_role_id)) {
						await UserChannelRole.query()
							.where('user_channel_id', id)
							.andWhere('channel_role_id', row.channel_role_id)
							.update({
								updated_by: auth.user.id,
								updated_at: currentTime,
								is_archived: 1
							})

						channel_roles = channel_roles.filter(function( obj ) {
							return obj.value != row.channel_role_id;
						});
					} 
				}
				// menu bertambah
				for (let row of channel_roles) {
					var cr_filtered = cr.filter(function(obj) {
						return obj.channel_role_id == row.value;
					});

					if (!_lo.isEmpty(cr_filtered)) {
						if (cr_filtered[0].is_archived == 1) {
							await UserChannelRole.query()
								.where('user_channel_id', id)
								.andWhere('channel_role_id', row.value)
								.update({
									is_archived		:0, 
									updated_by		: auth.user.id,
									updated_at		: currentTime
								})
						} 
					} else {
						let xid = uuidv4()
						let channel_role_data = {
							xid,
							user_channel_id	: id,
							channel_role_id	: row.value,
							is_archived		: 0,
							status			: 1,
							updated_by		: auth.user.id,
							updated_at		: currentTime,
							created_by		: auth.user.id,
							created_at		: currentTime
						}
						await UserChannelRole.create(channel_role_data)
					}
				}
				console.log('sampai akhir')
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Channel updated',
					data: []
				}
				return response.send(data)
			} else {
				console.log('err')
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel cannot be found',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async delete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		let userchannel = await UserChannel.find(await MyHash.decrypt(formData.id))
		if (userchannel){
			try {
				//soft delete => change is_archived to true
				let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
				userchannel.updated_by  = auth.user.id,
				userchannel.updated_at  = currentTime,
				userchannel.is_archived = 1
				await userchannel.save()
				// await UserChannel.query().where('id', await MyHash.decrypt(params.id)).update({is_archived:1})

				// delete old user channel role
				let cm = await UserChannelRole.query()
							.where('user_channel_id', await MyHash.decrypt(formData.id))
							.update({ is_archived: 1 })

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'User Channel success deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'User Channel cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'User Channel cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
	
	async multidelete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			try {
				for (let i in dataitem) {
					let userchannel = await UserChannel.find(await MyHash.decrypt(dataitem[i]))
					//soft delete => change is_archived to true
					// await UserChannel.query().where('id', await MyHash.decrypt(i.id)).update({is_archived:1})
					//soft delete => change is_archived to true
					let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
					userchannel.updated_by  = auth.user.id,
					userchannel.updated_at  = currentTime,
					userchannel.is_archived = 1
					await userchannel.save()
					// await UserChannel.query().where('id', await MyHash.decrypt(params.id)).update({is_archived:1})

					// delete old user channel role
					await UserChannelRole.query()
							.where('user_channel_id', await MyHash.decrypt(dataitem[i]))
							.update({ is_archived: 1 })
					} 
			}catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'User Channel cannot be deleted',
					data: []
				}
				return response.send(data)
			}
			
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'User Channel success deleted',
				data: []
			}
			return response.send(data)
			
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'User Channel cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}

	async getChannelRoleList({request, response, auth, session}) {
		let req = request.get()
		let {channelid} = request.params;
		let datas
		let decrypted_channel_id
		// let checkAuth = await CheckAuth.get('UserChannel', 'read', auth)
		// if (!checkAuth) {
		// 	response.header('Content-type', 'application/json')
		// 	response.type('application/json')
		// 	let data = {
		// 		code: '4003',
		// 		message: 'User cannot access this module',
		// 		data: []
		// 	}
		// 	return response.send(data)
		// }

		let checkIsAdmin = await CheckAuth.get('UserChannel', 'read', auth, true)
		//if channelid not encrypted just use it
		if (checkIsAdmin) {
			try{
				decrypted_channel_id = await MyHash.decrypt(channelid);
			}catch(e){
				decrypted_channel_id = channelid;
			}
		}	else {
			decrypted_channel_id = auth.user.channel_id
		}
		if (req.phrase != '') {
			// console.log('kepanggil if')
			datas = await ChannelRole.query()
			.where('name', 'LIKE', '%' + req.phrase + '%')
			.andWhere('channel_id', '=', decrypted_channel_id)
			.andWhere('is_archived', '=',0)
			.limit(20).fetch()
		} else {
			// console.log('kepanggil else')
			datas = await ChannelRole.query()
			.where('channel_id', '=', decrypted_channel_id)
			.andWhere('is_archived', '=', 0)
			.limit(20).fetch()
		}

		let data = datas.toJSON()
		
		let result = []
		for(let i in data){
			result.push({
				value: data[i]['id'],
				label: data[i]['name']
			})
		}
		
		return response.send(result)
	}
}

module.exports = UserChannelController