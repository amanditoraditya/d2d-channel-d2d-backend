'use strict'

const Menu 			 = use('App/Models/Menu')
const MyHash 		 = require('./Helper/Hash.js')
const Database 		 = use('Database')
const fs 			 = require('fs')
const oss 			 = require('ali-oss');
const path 			 = require('path')
const Helpers 		 = use('Helpers')
const _lo 			 = use('lodash')
const QueryBuilder 	 = require('./Helper/DatatableBuilder.js')
const CheckAuth 	 = require('./Helper/CheckAuth.js')
const Env 			 = use('Env')
const Logger 		 = use('Logger')
const moment		 = require('moment');
const goaConfig 	 = require('../../../next/config.js')
const environment 	 = goaConfig.NODE_ENV
const { v4: uuidv4 } = require('uuid');
const { validate, validateAll } = use('Validator')

class MenuController {
	async datatable({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Menu', 'read', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'menus',
			sSelectSql: ['menus.id', 'menus.name', 'menus.icon', 'menus.description', 'menus.status', 'menus.is_published'],
			aSearchColumns: ['menus.name', 'menus.description'],
			sWhereAndSql: ['menus.is_archived = 0'],
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let row of select[0]) {
			
			let type = `image`;
			let content = `channel`;
			let path = environment == 'production' ? '' :'dev/'
			
			let id = row['id']
			let icon = ''
			if (row['icon'] != null || row['icon'] != '') {
				icon = `<img class="pull-left img-responsive img-thumbnail" src="https://static.d2d.co.id/dev/image/channel/${row['icon']}" style="width:100px;"/>`
			}

			fdata.push([
				"<div class='text-center'>\
					<label class='checkbox checkbox-lg checkbox-inline'>\
						<input type='checkbox' id='titleCheckdel' />\
						<span></span>\
					</label>\
					<input type='hidden' class='deldata' name='item[]' value='"+ await MyHash.encrypt(id.toString()) +"' disabled />\
				</div>\n",
				row['id'],
				row['name'],
				icon,
				row['description'],
				row['status'],
				row['is_published'],
				"<div class='text-center'>\
					<div class='btn-group btn-group-sm'>\
						<a href='javascript:void(0);' data-href='/module/menu/edit?id="+ await MyHash.encrypt(id.toString()) +"' data-as='/menu/edit/"+ await MyHash.encrypt(id.toString()) +"' class='btn btn-sm btn-primary btn-edit' title='Edit'><i class='fas fa-pencil-alt'></i></a>\
						<a href='javascript:void(0);' class='btn btn-sm btn-danger alertdel' id='"+ await MyHash.encrypt(id.toString()) +"' title='Delete'><i class='far fa-trash-alt'></i></a>\
					</div>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Menu', 'create', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		// let fields = [
		// 	'name',
		// 	'icon',
		// 	'description',
		// 	'link',
		// 	'target',
		// 	'status',
		// 	'is_published',
		// 	'parent_id',
		// 	'position'
		// ]

		// const { 
		// 	name,
		// 	description,
		// 	link,
		// 	target,
		// 	status,
		// 	is_published,
		// 	parent_id,
		// 	position
		// } = request.only(fields)
		
		console.log(request.post())
		let form = JSON.parse(request.post().data)
		const icon_file = request.file('icon', {
			type: 'image',
			subtype: ['image/jpeg', 'image/png'],
			size: '2mb',
			extnames: ['jpg', 'jpeg', 'png']
		})
		
		console.log(form)
		console.log(form.status)
		console.log(icon_file)
		let xid = uuidv4()
		let formData = {
			xid,
			name: form.name,
			description: form.description,
			link: form.link,
			target: form.target,
			status: form.status,
			is_published: form.is_published,
			parent_id: form.parent_id,
			position: form.position,
			is_archived: 0,	
			updated_by: auth.user.id,
			updated_at: currentTime,
			created_by: auth.user.id,
			created_at: currentTime
		}
		if (icon_file != null) {
			formData.icon = `${xid}.${icon_file.subtype}`
		} else {
			formData.icon = ''
		}

		let rules = {
			xid: 'required',
			name: 'required',
			link: 'required',
			target: 'required'
		}
		
		const validation = await validateAll(formData, rules)

		// Logger.info('validation is %j', validation)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			console.log(validation.fails())
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			try {
				await Menu.create(formData)
				
				//#region upload logo
				if (icon_file != null) {
					try {
						let type = `image`;
						let content = `channel`;
						let path = environment == 'production' ? '' :'dev/'
				
						const client = new oss({
							region: 'oss-ap-southeast-5',
							accessKeyId: 'LTAINk4kOx5TLJT6',
							accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
							bucket: 'd2doss'
						})
						
						let result = await client.put(`${path}${type}/${content}/${xid}.${icon_file.subtype}`, icon_file.tmpPath);
						console.log(result)
					} catch(error) {
						console.log(error)
					}
				}
				
				//#endregion

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Menu added',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				console.log(e)
				let data = {
					code: '4004',
					message: 'Menu cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async edit({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Menu', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		let menu	= await Menu.find(await MyHash.decrypt(formData.id))
		
		Logger.info('menu is %j', menu)
		if (menu) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Menu found',
				data: menu
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Menu cannot be found',
				data: []
			}
			return response.send(data)
		}
	}
	
	async update({params, request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Menu', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		// let fields = [
		// 	'name',
		// 	'icon',
		// 	'description',
		// 	'link',
		// 	'target',
		// 	'status',
		// 	'is_published',
		// 	'parent_id',
		// 	'position'
		// ]

		// const { 
		// 	name,
		// 	icon,
		// 	description,
		// 	link,
		// 	target,
		// 	status,
		// 	is_published,
		// 	parent_id,
		// 	position
		// } = request.only(fields)
		
		let type = `image`;
		let content = `channel`;
		let path = environment == 'production' ? '' :'dev/'

		const client = new oss({
			region: 'oss-ap-southeast-5',
			accessKeyId: 'LTAINk4kOx5TLJT6',
			accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
			bucket: 'd2doss'
		})

		let form = JSON.parse(request.post().data)

		const icon_file = request.file('icon', {
			type: 'image',
			subtype: ['image/jpeg', 'image/png'],
			size: '2mb',
			extnames: ['jpg', 'jpeg', 'png']
		})
		
		let menu = await Menu.find(await MyHash.decrypt(params.id))
		
		let xid = uuidv4()
		// Logger.info('channel_type_id is %j', channel_type_id)
		let formData = {
			xid,
			name: form.name,
			description: form.description,
			link: form.link,
			target: form.target,
			status: form.status,
			is_published: form.is_published,
			parent_id: form.parent_id,
			position: form.position,
			is_archived: 0,
			updated_by: auth.user.id,
			updated_at: currentTime
		}

		if (icon_file != null) {
			formData.icon = `${xid}.${icon_file.subtype}`
		} else {
			formData.icon = ''
		}

		let rules = {
			xid: 'required',
			name: 'required',
			link: 'required',
			target: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		// Logger.info('formData is %j', formData)
		// Logger.info('rules is %j', rules)
		// Logger.info('validation is %j', validation)
		if (validation.fails()) {
			console.log(validation)
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			if (menu) {
				if (form.remove_image) {
					//remove
					await client.delete(`${path}${type}/${content}/${menu.icon}`);
					formData.icon = ''
				}
				await Menu.query().where('id', await MyHash.decrypt(params.id)).update(formData)
				
				//#region upload logo
				if (icon_file != null) {
        			await client.put(`${path}${type}/${content}/${xid}.${icon_file.subtype}`, icon_file.tmpPath);
					// console.log(result)
				}
				//#endregion

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Menu updated',
					data: []
				}
				return response.send(data)
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Menu cannot be found',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async delete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Menu', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		let menu = await Menu.find(await MyHash.decrypt(formData.id))
		if (menu){
			try {
				let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
				menu.updated_by  = auth.user.id,
				menu.updated_at  = currentTime,
				menu.is_archived = 1
				await menu.save()
				// await Menu.query().where('id', await MyHash.decrypt(formData.id)).update({is_archived:1})
				// await menu.delete()

				//#region delete logo
				let type = `image`;
				let content = `channel`;
				let path = environment == 'production' ? '' :'dev/'
					
					
				const client = new oss({
						region: 'oss-ap-southeast-5',
						accessKeyId: 'LTAINk4kOx5TLJT6',
						accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
						bucket: 'd2doss'
				})
				await client.delete(`${path}${type}/${content}/${menu.icon}`);
				//#endregion

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Menu success deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				// Logger.info('e is %j', e)
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Menu cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Menu cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
	
	async multidelete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Menu', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			for (let i in dataitem) {
				let menu = await Menu.find(await MyHash.decrypt(dataitem[i]))
				try {
					let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
					menu.updated_by  = auth.user.id,
					menu.updated_at  = currentTime,
					menu.is_archived = 1
					await menu.save()
					// await menu.delete()

					//#region delete logo
					let type = `image`;
					let content = `channel`;
					let path = environment == 'production' ? '' :'dev/'


					const client = new oss({
							region: 'oss-ap-southeast-5',
							accessKeyId: 'LTAINk4kOx5TLJT6',
							accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
							bucket: 'd2doss'
					})
					await client.delete(`${path}${type}/${content}/${menu.icon}`);
					//#endregion
				} catch (e) {}
			}
			
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Menu success deleted',
				data: []
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Menu cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}

	async getMenu({request, response, auth, session}) {
		let req = request.get()
		let datas

		let checkAuth = await CheckAuth.get('Channel', 'read', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		if (req.phrase != '') {
			datas = await Menu.query().where('name', 'LIKE', '%' + req.phrase + '%').andWhere('is_archived', '0').limit(20).fetch()
		} else {
			datas = await Menu.query().limit(20).fetch()
		}

		let data = datas.toJSON()
		
		let result = []
		for(let i in data){
			result.push({
				value: data[i]['id'],
				label: data[i]['name']
			})
		}
		
		return response.send(result)
	}
}

module.exports = MenuController