'use strict'

const Channel 		 = use('App/Models/Channel')
const ChannelType	 = use('App/Models/ChannelType')
const ChannelMenu	 = use('App/Models/ChannelMenu')
const MyHash 		 = require('./Helper/Hash.js')
const Database 		 = use('Database')
const fs 			 = require('fs')
const oss 			 = require('ali-oss');
const path 			 = require('path')
const Helpers 		 = use('Helpers')
const _lo 			 = use('lodash')
const QueryBuilder 	 = require('./Helper/DatatableBuilder.js')
const CheckAuth 	 = require('./Helper/CheckAuth.js')
const Env 			 = use('Env')
const Logger 		 = use('Logger')
const moment		 = require('moment');
const { v4: uuidv4 } = require('uuid');
const goaConfig 	 = require('../../../next/config.js')
const environment 	 = goaConfig.NODE_ENV
const crypto 		 = require('crypto')
const { validate, validateAll } = use('Validator')

class ChannelController {
	async datatable({request, response, auth, session}) {
		let admin_page = true
		let checkAuthAdmin = await CheckAuth.get('Channel', 'read', auth, admin_page)

		let conditions = []
		let from = 'channels \
		LEFT JOIN channel_types ON channels.channel_type_id = channel_types.id'

		if (!checkAuthAdmin) {
			conditions = [`channels.is_archived = 0 AND user_channels.xid = '${auth.user.xid}'`]
			from += '\ LEFT JOIN user_channels ON channels.id = user_channels.channel_id'
		} else {
			conditions = ['channels.is_archived = 0']
		}
		
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'channels',
			sSelectSql: ['channels.id', 'channels.country_code', 'channels.channel_name', 'channels.channel_type_id', 'channels.status', 'channel_types.name as channel_type_name'],
			aSearchColumns: ['channels.channel_name', 'channels.description'],
			sWhereAndSql: conditions,
			sFromSql: from
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let row of select[0]) {
			let id = row['id']
			let encrypted = await MyHash.encrypt(id.toString())
			let actionHtml = ''
			if (!checkAuthAdmin) {
				actionHtml = "<div class='text-center'>\
					<div class='btn-group btn-group-sm'>\
						<a href='javascript:void(0);' data-href='/module/user-channel/index?channelid="+ encrypted +"' data-as='/user-channel/detail/"+ encrypted +"' class='btn btn-sm btn-primary btn-edit' title='Manage'><i class='fas fa-compass'></i></a>\
					</div>\
				</div>\n"
			} else {
				actionHtml = "<div class='text-center'>\
					<div class='btn-group btn-group-sm'>\
						<a href='javascript:void(0);' data-href='/module/channel/edit?id="+ encrypted +"' data-as='/channel/edit/"+ encrypted +"' class='btn btn-sm btn-primary btn-edit' title='Edit'><i class='fas fa-pencil-alt'></i></a>\
						<a href='javascript:void(0);' class='btn btn-sm btn-danger alertdel' id='"+ encrypted +"' title='Delete'><i class='far fa-trash-alt'></i></a>\
					</div>\
				</div>\n"
			}
			fdata.push([
				"<div class='text-center'>\
					<label class='checkbox checkbox-lg checkbox-inline'>\
						<input type='checkbox' id='titleCheckdel' />\
						<span></span>\
					</label>\
					<input type='hidden' class='deldata' name='item[]' value='"+ encrypted +"' disabled />\
				</div>\n",
				row['id'],
				row['country_code'],
				row['channel_name'],
				row['channel_type_name'],
				row['status'],
				actionHtml
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'create', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		// let fields = [
		// 	'channel_type_id',
		// 	'country_code',
		// 	'channel_name',
		// 	'is_admin',
		// 	'logo',
		// 	'description',
		// 	'status',
		// ]
		// const { 
		// 	channel_type_id,
		// 	country_code,
		// 	channel_name,
		// 	is_admin,
		// 	logo,
		// 	description,
		// 	status 
		// } = request.only(fields)
		
		let form = JSON.parse(request.post().data)
		
		const logo_file = request.file('logo', {
			type: 'image',
			subtype: ['image/jpeg', 'image/png'],
			size: '2mb',
			extnames: ['jpg', 'jpeg', 'png']
		})

		let xid = uuidv4()
		let formData = {
			xid,
			channel_type_id: form.channel_type_id,
			country_code: form.country_code,
			channel_name: form.channel_name,
			description: form.description,
			is_admin: 0,
			is_archived: 0,
			status: form.status,			
			updated_by: auth.user.id,
			updated_at: currentTime,
			created_by: auth.user.id,
			created_at: currentTime
		}
		
		if (logo_file != null) {
			formData.logo = `${xid}.${logo_file.subtype}`
		}

		let rules = {
			xid: 'required',
			country_code: 'required',
			channel_type_id: 'required',
			channel_name: 'required'
		}
		
		const validation = await validateAll(formData, rules)

		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			try {
				let newChannel = await Channel.create(formData)
				let channel_menus = form.channel_menus

				//#region menu bertambah
				for (let row of channel_menus) {
					let xid = uuidv4()
					let channel_menu_data = {
						xid,
						channel_id: newChannel.id,
						menu_id: row.value,
						position: 0,
						is_archived: 0,
						updated_by: auth.user.id,
						updated_at: currentTime,
						created_by: auth.user.id,
						created_at: currentTime
					}
					await ChannelMenu.create(channel_menu_data)
				}
				//#endregion

				//#region upload logo
				if (logo_file != null) {
					try {
						let type = `image`;
						let content = `channel`;
						let path = environment == 'production' ? '' :'dev/'
				
						const client = new oss({
							region: 'oss-ap-southeast-5',
							accessKeyId: 'LTAINk4kOx5TLJT6',
							accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
							bucket: 'd2doss'
						})
						
						let result = await client.put(`${path}${type}/${content}/${xid}.${logo_file.subtype}`, logo_file.tmpPath);
						console.log(result)
					} catch(error) {
						console.log(error)
					}
				}
				
				//#endregion

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Channel added',
					data: []
				}
				return response.send(data)
			} catch (e) {
				console.log(e)
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async edit({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		// let channel	= await Channel.find(await MyHash.decrypt(formData.id))
		let channel = await Channel.query()
								   .select('channels.*', 'channel_types.name as channel_type_name')
								   .leftJoin('channel_types', 'channels.channel_type_id', 'channel_types.id')
								   .where('channels.id', await MyHash.decrypt(formData.id))
								   .first()

		let get_menus = await channel.menus().where('channel_menus.is_archived', '0').fetch()
		let channel_menus_json = get_menus.toJSON()
		let channel_menus = []
		for(let i in channel_menus_json){
			channel_menus.push({
				value: channel_menus_json[i]['id'],
				label: channel_menus_json[i]['name']
			})
		}

		if (!_lo.isEmpty(channel_menus)) {
			channel.channel_menus = channel_menus
			console.log(channel.channel_menus)
		}

		if (channel) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Channel found',
				data: channel
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Channel cannot be found',
				data: []
			}
			return response.send(data)
		}
	}
	
	async update({params, request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		
		let type = `image`;
		let content = `channel`;
		let path = environment == 'production' ? '' :'dev/'

		const client = new oss({
			region: 'oss-ap-southeast-5',
			accessKeyId: 'LTAINk4kOx5TLJT6',
			accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
			bucket: 'd2doss'
		})

		let form = JSON.parse(request.post().data)

		const logo_file = request.file('logo', {
			type: 'image',
			subtype: ['image/jpeg', 'image/png'],
			size: '2mb',
			extnames: ['jpg', 'jpeg', 'png']
		})

		let channel = await Channel.find(await MyHash.decrypt(params.id))

		let channel_menus = form.channel_menus
		
		
		let xid = channel.xid
		let formData = {
			xid,
			channel_type_id: form.channel_type_id,
			country_code: form.country_code,
			channel_name: form.channel_name,
			description: form.description,
			is_admin: 0,
			is_archived: 0,
			status: form.status,			
			updated_by: auth.user.id,
			updated_at: currentTime
		}

		if (logo_file != null) {
			formData.logo = `${xid}.${logo_file.subtype}`
		}
		
		let rules = {
			xid: 'required',
			country_code: 'required',
			channel_type_id: 'required',
			channel_name: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			if (channel) {
				
				if (form.remove_image) {
					//remove
					await client.delete(`${path}${type}/${content}/${channel.logo}`);
					formData.logo = null
				}

				let id = await MyHash.decrypt(params.id)
				await Channel.query().where('id', id).update(formData)
				
				//#region  soft delete old channel_menu

				//get menu existing
				let cm = await ChannelMenu.query()
							.where('channel_id', id)
							.fetch()
				cm = _lo.isEmpty(cm) ? [] : cm.toJSON()
				channel_menus = _lo.isEmpty(channel_menus) ? [] : channel_menus
				for (let row of cm) {
					//if menu berkurang

					//check kalau ada menu dari form dengan menu existing di table
					if (!channel_menus.some(item => item.value == row.menu_id)) {
						//kalau ada kita hanya update archived-nya
						await ChannelMenu.query()
							.where('channel_id', id)
							.andWhere('menu_id', row.menu_id)
							.update({
								updated_by: auth.user.id,
								updated_at: currentTime,
								is_archived:1
							})

						//mengurangi menu yang didapat dari form yang akan diiterasikan untuk ditambah ke table
						channel_menus = channel_menus.filter(function( obj ) {
							return obj.value != row.menu_id;
						});
					} 
				}
				// menu bertambah
				for (let row of channel_menus) {
					
					//check kalau ada menu dari form dengan menu existing di table
					//kalau ada tidak ditambahkan lagi
					var cm_filtered = cm.filter(function(obj) {
						return obj.menu_id == row.value;
					});

					//kalau menu sudah ada di table hanya update archive
					if (!_lo.isEmpty(cm_filtered)) {
						if (cm_filtered[0].is_archived == 1) {
							await ChannelMenu.query()
								.where('channel_id', id)
								.andWhere('menu_id', row.value)
								.update({
									is_archived:0, 
									updated_by: auth.user.id,
									updated_at: currentTime
								})
						} 
					} else {
						//kalau menu belum ada di table di insert
						let xid = uuidv4()
						let channel_menu_data = {
							xid,
							channel_id: id,
							menu_id: row.value,
							position: 0,
							is_archived: 0,
							updated_by: auth.user.id,
							updated_at: currentTime,
							created_by: auth.user.id,
							created_at: currentTime
						}
						await ChannelMenu.create(channel_menu_data)
					}
				}
				//#endregion

				//#region upload logo
				if (logo_file != null) {
        			await client.put(`${path}${type}/${content}/${xid}.${logo_file.subtype}`, logo_file.tmpPath);
					// console.log(result)
				}
				//#endregion

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Channel updated',
					data: []
				}
				return response.send(data)
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel cannot be found',
					data: []
				}
				return response.send(data)
			}
		}
	}

	async delete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		let channel = await Channel.find(await MyHash.decrypt(formData.id))
		if (channel){
			try {
				let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
				channel.updated_by  = auth.user.id,
				channel.updated_at  = currentTime,
				channel.is_archived = 1
				await channel.save()
				
				// await Channel.query().where('id', await MyHash.decrypt(formData.id)).update({is_archived:1})
				// await channel.delete()
				
				// delete old channel_menu
				let cm = await ChannelMenu.query()
							.where('channel_id', await MyHash.decrypt(formData.id))
							.update({ is_archived: 1 })

				//#region delete logo
				let type = `image`;
				let content = `channel`;
				let path = environment == 'production' ? '' :'dev/'
					
					
				const client = new oss({
						region: 'oss-ap-southeast-5',
						accessKeyId: 'LTAINk4kOx5TLJT6',
						accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
						bucket: 'd2doss'
				})
				await client.delete(`${path}${type}/${content}/${channel.logo}`);
				//#endregion
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Channel success deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Channel cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
	
	async multidelete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			for (let i in dataitem) {
				let channel = await Channel.find(await MyHash.decrypt(dataitem[i]))
				try {
					let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
					channel.updated_by  = auth.user.id,
					channel.updated_at  = currentTime,
					channel.is_archived = 1
					await channel.save()
					let cm = await ChannelMenu.query()
								.where('channel_id', await MyHash.decrypt(dataitem[i]))
								.update({ is_archived: 1 })

					//#region delete logo
					let type = `image`;
					let content = `channel`;
					let path = environment == 'production' ? '' :'dev/'

					const client = new oss({
							region: 'oss-ap-southeast-5',
							accessKeyId: 'LTAINk4kOx5TLJT6',
							accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
							bucket: 'd2doss'
					})
					await client.delete(`${path}${type}/${content}/${channel.logo}`);
					//#endregion

					// await channel.delete()
				} catch (e) {}
			}
			
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Channel success deleted',
				data: []
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Channel cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}

	async getChannelType({request, response, auth, session}) {
		let req = request.get()
		let datas

		let checkAuth = await CheckAuth.get('Channel', 'read', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		if (req.phrase != '') {
			datas = await ChannelType.query().select('id', 'name').where('name', 'LIKE', '%' + req.phrase + '%').andWhere('is_archived', '0').limit(20).fetch()
		} else {
			datas = await ChannelType.query().limit(20).fetch()
		}

		let data = datas.toJSON()
		
		let result = []
		for(let i in data){
			result.push({
				value: data[i]['id'],
				label: data[i]['name']
			})
		}
		
		return response.send(result)
	}

	async getChannels({request, response, auth, session}) {
		let datas

		let checkAuth = await CheckAuth.get('Channel', 'read', auth, true)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		datas = await Channel.query().select('id', 'channel_name').limit(20).fetch()

		let data = datas.toJSON()
		
		let result = []
		for(let i in data){
			result.push({
				value: data[i]['id'],
				label: data[i]['channel_name']
			})
		}
		
		return response.send(result)
	}
}

module.exports = ChannelController