'use strict'

//MODELS
const Channel = use('App/Models/Channel')
const Menu = use('App/Models/Menu')
// MODULES
const Validation = require('joi')
const Config = use('Config')
const _ = require('lodash')
const jwt = require('jsonwebtoken')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const Dictionary = require('../../../../../../resources/statics/dictionary')

class ChannelMicro {
    //get
    async get(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'SUC_200'
            options.reason = Dictionary.TR000005
            if(data.xid){
                let q_result = await Channel
                .query()
                .select('id', 'xid', 'country_code', 'channel_type_id', 'channel_name', 'logo')
                .with('_v1_channel_type')
                .with('_v1_channel_banners')
                .with('_v1_channel_subscriber', (builder) => {
                    builder.where('uid', usersession.uid)
                })
                .where('xid', data.xid)
                .whereHas('_v1_channel_subscriber', (builder) => {
                    builder.where('uid', usersession.uid)
                })
                .first()
                if(q_result){
                    q_result = q_result.toJSON()
                    const _sbc = jwt.sign({
                        _scc: q_result._v1_channel_subscriber.xid
                    }, usersession.sec, { expiresIn: '24h' })
                    q_result.sbc = _sbc
                    q_result.type = null
                    if(q_result._v1_channel_type){
                        const _data = _.pick(q_result._v1_channel_type, ['name'])
                        q_result.type = _data
                    }
                    q_result.menus = []
                    if(q_result.type){
                        let q_result_2 = await Menu
                        .query()
                        .select('id', 'xid','name', 'icon', 'description', 'link', 'target')
                        .with('_v1_channel_menus', (builder) => {
                            builder.where('channel_id', q_result.id)
                        })
                        .where('status', 1)
                        .where('is_published', 1)
                        .orderBy('position', 'asc')
                        .fetch()
                        if(q_result_2){
                            let _menus = []
                            q_result_2 = q_result_2.toJSON()
                            for(const key in q_result_2){
                                q_result_2[key].disabled = false
                                if(!q_result_2[key]._v1_channel_menus){
                                    q_result_2[key].disabled = true
                                    q_result_2[key].link = 'FORBIDEN'
                                    q_result_2[key].target = 'FORBIDEN'
                                }
                                const _data = _.pick(q_result_2[key], ['xid','name', 'icon', 'description', 'disabled', 'link', 'target'])
                                _menus.push(_data)
                            }
                            if(_menus.length > 0){
                                q_result.menus = _menus
                            }
                        }
                    }
                    q_result.banners = []
                    if(q_result._v1_channel_banners){
                        for(const key in q_result._v1_channel_banners){
                            let _banners = q_result._v1_channel_banners[key]
                            _banners = _.pick(_banners, ['name', 'image','position', 'action', 'link', 'target'])
                            q_result.banners.push(_banners)
                        }
                    }
                    dataset = _.pick(q_result, ['xid', 'country_code', 'type', 'channel_name', 'logo', 'menus', 'banners' , 'sbc'])
                    options.reason = Dictionary.TR000004
                }
            } else {
                const auth_schema = Validation.object({
                    type: Validation.string().min(5).max(50).optional().valid('recomendation', 'subscribed' ,'fresh'),
                    terms: Validation.string().min(3).max(50).optional(),
                    paginate : Validation.string().regex(/^\d+$/).optional(),
                    limit : Validation.string().regex(/^\d+$/).optional(),
                })
                let validationprc = await MicroH.validation(data, auth_schema)
                if(validationprc.success){
                    if(!validationprc.data.paginate) {
                        validationprc.data.paginate = Config.get('variables.default_values.page_paginate')
                    }
                    if(!validationprc.data.limit) {
                        validationprc.data.limit = Config.get('variables.default_values.page_limit')
                    }
                    if(!validationprc.data.type){
                        validationprc.data.type = "default"
                    }
                    let result_1 = Channel
                    .query()
                    .select('id', 'xid', 'country_code', 'channel_name', 'description', 'logo')
                    .where('is_admin', 0)
                    .where('status', 1)
                    .where('is_archived', 0)
                    switch (validationprc.data.type) {
                        case 'fresh':
                            result_1
                            .with('_v1_channel_subscriber', (builder) => {
                                builder
                                .where('uid', usersession.uid)
                                .where('is_unsubscribed', 0)
                            })
                            .whereDoesntHave('_v1_channelsubscribers', (builder) => {
                                builder
                                .where('uid', usersession.uid)
                                .where('is_unsubscribed', 0)
                            })
                            .orderBy('id', 'desc')
                            break
                        case 'subscribed':
                            result_1
                            .with('_v1_channel_subscriber', (builder) => {
                                builder
                                .where('uid', usersession.uid)
                            })
                            .whereHas('_v1_channelsubscribers', (builder) => {
                                builder
                                .where('uid', usersession.uid)
                                .where('is_unsubscribed', 0)
                            })
                            break
                        default:
                            result_1
                            .with('_v1_channel_subscriber', (builder) => {
                                builder
                                .where('uid', usersession.uid)
                                .where('is_unsubscribed', 0)
                            })
                            .orderBy('id', 'desc')
                    }
                    if(validationprc.data.terms){
                        result_1.where((builder) => {
                            builder.where('channel_name', 'LIKE', `%${validationprc.data.terms}%`)
                            .orWhere('description', 'LIKE', `%${validationprc.data.terms}%`)
                        })
                    }
                    result_1 = await result_1
                    .paginate(validationprc.data.paginate, validationprc.data.limit)
                    if(result_1){
                        result_1 = result_1.toJSON()
                        if(result_1.data.length > 0){
                            let _r_data = []
                            for(const key in result_1.data){
                                let r_data = result_1.data[key]
                                r_data.subscribed = false
                                if(r_data._v1_channel_subscriber){
                                    r_data.subscribed = true
                                }
                                const _pick = _.pick(r_data, ['xid', 'country_code', 'channel_name', 'description', 'logo', 'subscribed'])
                                _r_data.push(_pick)
                            }
                            result_1.data = _r_data
                            options.reason = Dictionary.TR000004
                        } 
                        dataset = result_1
                    }
                } else {
                    options.type = 'ERR_412'
                    options.reason  = await MicroH.reason('TR000003', validationprc.message)
                } 
            }
            if(dataset){
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }
}

module.exports = new ChannelMicro

