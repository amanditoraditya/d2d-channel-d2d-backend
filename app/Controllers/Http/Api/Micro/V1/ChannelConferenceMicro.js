'use strict'

const Database = use('Database')
//MODELS
const ChannelConference = use('App/Models/ChannelConference')
const ChannelConferenceAttendee = use('App/Models/ChannelConferenceAttendee')
const ChannelConferenceActivity = use('App/Models/ChannelConferenceActivity')
// MODULES
const Validation = require('joi').extend(require('@joi/date'))
const Config = use('Config')
const _ = require('lodash')
const uuid = require('uuid')
const crypto = require("crypto-js")
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const Dictionary = require('../../../../../../resources/statics/dictionary')

class ChannelConferenceMicro {
    //get
    async calendar(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'SUC_200'
            options.reason = Dictionary.TR000005
            const auth_schema = Validation.object({
                tz : Validation.string().regex(/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/).required(),
                by : Validation.string().min(5).max(50).optional().valid('month', 'date', 'default')
            }).when('.by', {  /* <-- prefix with . */
                switch : [
                    { 
                        is: 'month', then: 
                        Validation.object({ 
                            month : Validation.number().integer().min(1).max(12).required(),
                            year : Validation.number().integer().min(1945).max(9999).required()
                        }) 
                    },
                    { 
                        is: 'date', then: 
                        Validation.object({ 
                            date : Validation.date().format(['YYYY/MM/DD', 'DD-MM-YYYY']).required()
                        }) 
                    },
                ]
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            if(validationprc.success){
                if(!validationprc.data.by){
                    validationprc.data.by = 'default'
                }
                let conference = ChannelConference
                .query()
                .select('id', 'xid', 'title', 'description',
                 Database.raw(
                    `
                    CONVERT_TZ(start_date, gmt_tz,?) start_at,
                    CONVERT_TZ(end_date, gmt_tz,?) end_at,
                    TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(start_date, gmt_tz,'+07:00'), now())) start_sec,
                    TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(end_date, gmt_tz,'+07:00'), now())) end_sec,
                    TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(registration_until, gmt_tz,'+07:00'), now())) end_reg
                    `, [validationprc.data.tz, validationprc.data.tz]
                ))
                .with('_v1_channel_conference_attendees', (builder) => {
                    builder
                    .where('channel_subscriber_id', usersession.id)
                    .where('blocked', 0)
                    .where('is_removed', 0)
                })
                .where('channel_id', usersession.channel_id)
                .where('is_published', 1)
                .where('is_archived', 0)
                .whereNotNull('registration_until')
                switch (validationprc.data.by) {
                    case 'month':
                        conference
                        .whereRaw('YEAR(DATE(CONVERT_TZ(start_date, gmt_tz, ?))) = ?', [validationprc.data.tz, validationprc.data.year])
                        .whereRaw('MONTH(DATE(CONVERT_TZ(start_date, gmt_tz, ?))) = ?', [validationprc.data.tz, validationprc.data.month])
                        break
                    case 'date':
                        conference
                        .whereRaw('DATE(CONVERT_TZ(start_date, gmt_tz, ?)) = ?', [validationprc.data.tz, validationprc.data.date])
                        break
                    default:
                        conference
                        .whereRaw('YEAR(DATE(CONVERT_TZ(start_date, gmt_tz, ?))) = YEAR(DATE(CONVERT_TZ(now(), ?, ?)))', [validationprc.data.tz, '+07:00',validationprc.data.tz])
                        .whereRaw('MONTH(DATE(CONVERT_TZ(start_date, gmt_tz, ?))) = MONTH(DATE(CONVERT_TZ(now(), ?, ?)))', [validationprc.data.tz, '+07:00',validationprc.data.tz])
                }
                conference = await conference.fetch()
                if(conference){
                    conference = conference.toJSON()
                    let _conference = []
                    for(const key in conference){
                        const ckey = conference[key]
                        ckey.current = 'upcoming'
                        ckey.hex = '#0096db' 
                        ckey.action = {
                            register : false,
                            join : false,
                            notify : false
                        }
                        if(ckey.start_sec > 0 && ckey.end_reg > 0){
                            ckey.action.register = true
                            if(ckey._v1_channel_conference_attendees){
                                ckey.action.notify = true
                            }
                        }
                        if(ckey.start_sec < 0 && ckey.end_sec > 0){
                            ckey.current = 'live'
                            ckey.hex = '#7dc9a4'
                            if(ckey._v1_channel_conference_attendees){
                                ckey.action.join = true
                            }
                        }
                        if(ckey.end_sec < 0){
                            ckey.current = 'passed'
                            ckey.hex = '#f2058b'
                            ckey.action.register = false
                            ckey.action.join = false
                        }
                        const _data = _.pick(ckey, ['xid', 'title', 'description', 'start_at', 'start_sec', 'end_date', 'current', 'hex', 'action'])
                        _conference.push(_data)
                    }
                    if(_conference.length > 0){
                        options.reason = Dictionary.TR000004
                    }
                    dataset = _conference
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 
            if(dataset){
                options.type = 'SUC_200'
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async register(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            const auth_schema = Validation.object({
                source: Validation.string().min(5).max(50).required().valid('mobile', 'direct', 'event'),
                xid : Validation.string().guid().required()
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            
            if(validationprc.success){
                const channelconference = await ChannelConference
                .query()
                .select('id')
                .where('xid', validationprc.data.xid)
                .where('channel_id', usersession.channel_id)
                .where('is_published', 1)
                .where('is_archived', 0)
                .whereRaw(`TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(start_date, gmt_tz,'+07:00'), now())) > 0`)
                .whereRaw(`TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(registration_until, gmt_tz,'+07:00'), now())) > 0`)
                .first()
                if(channelconference){
                    let attend = await ChannelConferenceAttendee
                    .query()
                    .where('channel_subscriber_id', usersession.id)
                    .where('conference_id', channelconference.id)
                    .first()
                    if(attend){
                        let exec_1 = null
                        attend = attend.toJSON()
                        options.type = 'ERR_409'
                        options.reason = Dictionary.TR000010
                    } else {
                        let exec_1 = null
                        const record_1 = {
                            xid : uuid.v1(),
                            conference_id : channelconference.id,
                            channel_subscriber_id : usersession.id,
                            registered_from : validationprc.data.source
                        }
                        exec_1 = await ChannelConferenceAttendee.create(record_1)
                        if(exec_1){
                            dataset = {
                                xid : exec_1.xid
                            }
                        } 
                    }
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async notify(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            const auth_schema = Validation.object({
                xid : Validation.string().guid().required()
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            
            if(validationprc.success){
                const channelconference = await ChannelConference
                .query()
                .select('id')
                .where('xid', validationprc.data.xid)
                .where('channel_id', usersession.channel_id)
                .where('is_published', 1)
                .where('is_archived', 0)
                .whereRaw(`TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(start_date, gmt_tz,'+07:00'), now())) > 0`)
                .whereRaw(`TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(registration_until, gmt_tz,'+07:00'), now())) > 0`)
                .first()
                if(channelconference){
                    let attend = await ChannelConferenceAttendee
                    .query()
                    .where('channel_subscriber_id', usersession.id)
                    .where('conference_id', channelconference.id)
                    .first()
                    if(attend){
                        if(!attend.is_notify){
                            let exec_1 = null
                            exec_1 = await ChannelConferenceAttendee.query()
                            .where('id',  attend.id)
                            .update({ 
                                is_notify : 1
                            })
                            if(exec_1){
                                dataset = {
                                    xid : attend.xid
                                }
                            } 
                        } else {
                            let exec_1 = null
                            attend = attend.toJSON()
                            options.type = 'ERR_409'
                            options.reason = Dictionary.TR000010
                        }               
                    }
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async join(data, usersession, opt, request){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            const auth_schema = Validation.object({
                tz : Validation.string().regex(/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/).required(),
                xid : Validation.string().guid().required()
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            if(validationprc.success){
                let channelconference = await ChannelConference
                .query()
                .select('id', 'xid', 'url', 'meet_key', 'meet_id')
                .where('xid', validationprc.data.xid)
                .where('channel_id', usersession.channel_id)
                .where('is_published', 1)
                .where('is_archived', 0)
                .whereRaw(`TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(start_date, gmt_tz,'+07:00'), now())) < 0`)
                .whereRaw(`TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(end_date, gmt_tz,'+07:00'), now())) > 0`)
                .first()
                if(channelconference){
                    let attend = await ChannelConferenceAttendee
                    .query()
                    .where('channel_subscriber_id', usersession.id)
                    .where('conference_id', channelconference.id)
                    .first()
                    if(attend){
                        channelconference = channelconference.toJSON()
                        let _channelconference = _.pick(channelconference, ['xid', 'url', 'meet_key', 'meet_id'])
                        _channelconference = JSON.stringify(_channelconference)
                        _channelconference = crypto.AES.encrypt(_channelconference, usersession.xid).toString()
                        if(_channelconference){
                            dataset = {
                                dmeet : _channelconference
                            }
                            this.activities(channelconference, usersession, 'join', validationprc.data.tz, request)
                        }             
                    }
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }
    
    async activities(channelconference, usersession, type, gmt_tz, request){
        try{
            if(channelconference && request){
                let exec_1 = null
                let record_1 = {
                    conference_id : channelconference.id,
                    channel_subscriber_id : usersession.id,
                    type : type,
                    gmt_tz : gmt_tz
                }
                const agent = await MicroH.detectagent(request)
                record_1 = _.merge(record_1, agent)
                exec_1 = await ChannelConferenceActivity.create(record_1)
            }
        } catch(e){
            console.log(e)
        }
    }
}

module.exports = new ChannelConferenceMicro

