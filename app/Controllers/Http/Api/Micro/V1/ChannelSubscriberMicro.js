'use strict'

//MODELS
const Channel = use('App/Models/Channel')
const ChannelSubscriber = use('App/Models/ChannelSubscriber')
const TemporaryMember = use('App/Models/TemporaryMember')
// MODULES
const Validation = require('joi')
const Config = use('Config')
const _ = require('lodash')
const uuid = require('uuid')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const Dictionary = require('../../../../../../resources/statics/dictionary')

class ChannelSubscriberMicro {
    async subscribe(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            data.uid = usersession.uid
            const auth_schema = Validation.object({
                source: Validation.string().min(5).max(50).required().valid('mobile', 'direct', 'event'),
                uid : Validation.string().alphanum().min(25).max(30).required(),
                xid : Validation.string().guid().required()
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            
            if(validationprc.success){
                const channel = await Channel
                .query()
                .select('id')
                .where('xid', validationprc.data.xid)
                .where('is_admin', 0)
                .where('status', 1)
                .where('is_archived', 0)
                .first()
                if(channel){
                    let subscribe = await ChannelSubscriber
                    .query()
                    .where('uid', validationprc.data.uid)
                    .where('channel_id', channel.id)
                    .first()
                    if(subscribe){
                        let exec_1 = null
                        subscribe = subscribe.toJSON()
                        if(subscribe.is_unsubscribed){
                            exec_1 = await ChannelSubscriber.query()
                            .where('id',  subscribe.id)
                            .update({ 
                                is_unsubscribed : 0
                            })
                            if(exec_1){
                                dataset = {
                                    xid : subscribe.xid
                                }
                                this.temp_user(usersession)
                            } 
                        } else {
                            options.type = 'ERR_409'
                            options.reason = Dictionary.TR000010
                        }
                    } else {
                        let exec_1 = null
                        const record_1 = {
                            xid : uuid.v1(),
                            uid : validationprc.data.uid,
                            channel_id : channel.id,
                            source : validationprc.data.source
                        }
                        exec_1 = await ChannelSubscriber.create(record_1)
                        if(exec_1){
                            dataset = {
                                xid : exec_1.xid
                            }
                        } 
                    }
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async unsubscribe(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            data.uid = usersession.uid
            const auth_schema = Validation.object({
                uid : Validation.string().alphanum().min(25).max(30).required(),
                xid : Validation.string().guid().required()
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            
            if(validationprc.success){
                const channel = await Channel
                .query()
                .select('id')
                .where('xid', validationprc.data.xid)
                .where('is_admin', 0)
                .where('status', 1)
                .where('is_archived', 0)
                .first()
                if(channel){
                    let subscribe = await ChannelSubscriber
                    .query()
                    .where('uid', validationprc.data.uid)
                    .where('channel_id', channel.id)
                    .first()
                    if(subscribe){
                        let exec_1 = null
                        subscribe = subscribe.toJSON()
                        if(!subscribe.is_unsubscribed){
                            exec_1 = await ChannelSubscriber.query()
                            .where('id',  subscribe.id)
                            .update({ 
                                is_unsubscribed : 1
                            })
                            if(exec_1){
                                dataset = {
                                    xid : subscribe.xid
                                }
                            } 
                        } else {
                            options.type = 'ERR_409'
                            options.reason = Dictionary.TR000010
                        }
                    } 
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async temp_user(usersession){
        const member = await TemporaryMember
        .query()
        .where('uid', usersession.uid)
        .first()
        if(!member){
            const record_1 = {
                uid : usersession.uid,
                email : usersession.firebase.email,
                name : usersession.firebase.name
            }
            try{
                TemporaryMember.create(record_1)
            }catch(e){ console.log(e) }
        }
    }
}

module.exports = new ChannelSubscriberMicro

