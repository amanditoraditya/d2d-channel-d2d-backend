'use strict'
const Env = use('Env')
const Database = use('Database')
//MODELS
const ChannelForumPost = use('App/Models/ChannelForumPost')
const ChannelForumLike = use('App/Models/ChannelForumLike')
const ChannelForumComment = use('App/Models/ChannelForumComment')
// MODULES
const Validation = require('joi')
const Config = use('Config')
const _ = require('lodash')
const jwt = require('jsonwebtoken')
const uuid = require('uuid')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const Dictionary = require('../../../../../../resources/statics/dictionary')

class ChannelForumMicro {
    //get
    async get(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'SUC_200'
            options.reason = Dictionary.TR000005
            if(data.xid){
                const auth_schema = Validation.object({
                    xid : Validation.string().guid().required(),
                    tz : Validation.string().regex(/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/).required()
                })
                let validationprc = await MicroH.validation(data, auth_schema)
                if(validationprc.success){
                    let result = await ChannelForumPost
                    .query()
                    .select('id', 'xid', 'channel_id','title', 'description', 
                        Database.raw('CONVERT_TZ(created_at,"+07:00",?) created_at', [validationprc.data.tz]),
                        Database.raw('(SELECT COUNT(id) likes from channel_forum_likes WHERE channel_forum_content_type_id = ? AND ref_id = channel_forum_posts.id AND unlike = 0) total_likes', [Env.get('FORUM_POST_CONTENT_TYPE')]),
                        Database.raw('(SELECT COUNT(id) comments from channel_forum_comments WHERE channel_forum_post_id = channel_forum_posts.id) total_comments'),
                        Database.raw('(SELECT COUNT(id) liked from channel_forum_likes WHERE channel_forum_content_type_id = ? AND ref_id = channel_forum_posts.id AND channel_subscriber_id =? AND unlike = 0) _liked', [Env.get('FORUM_POST_CONTENT_TYPE'), usersession.id])
                    )
                    .with('_v1_channel', (builder) => {
                        builder
                        .select('id', 'channel_name', 'logo')
                    })
                    .with('_v1_channel_forum_attachments', (builder) => {
                        builder
                        .select('id', 'ref_id', 'attachment_name', 'attachment_type', 'attachment_url')
                        .where('channel_forum_content_type_id', [Env.get('FORUM_POST_CONTENT_TYPE')])
                    })
                    .where('xid', data.xid)
                    .where('channel_id', usersession.channel_id)
                    .first()
                    if(result){
                        result = result.toJSON()
                        let r_data = result
                        r_data.liked = false
                        if(r_data._liked > 0){
                            r_data.liked = true
                        }
                        r_data.channel = null
                        if(r_data._v1_channel){
                            r_data.channel = _.pick(r_data._v1_channel, ['channel_name', 'logo'])
                        }
                        r_data.attachments = []
                        for(const key2 in r_data._v1_channel_forum_attachments){
                            let attachment = r_data._v1_channel_forum_attachments[key2]
                            attachment = _.pick(attachment, ['attachment_name', 'attachment_type', 'attachment_url'])
                            r_data.attachments.push(attachment)
                        }
                        const result_1 = _.pick(r_data, ['xid', 'channel','title', 'description', 'total_comments', 'total_likes' ,'liked', 'attachments', 'created_at'])
                        dataset = result_1
                        options.reason = Dictionary.TR000004
                    }
                }else {
                    options.type = 'ERR_412'
                    options.reason  = await MicroH.reason('TR000003', validationprc.message)
                } 
            } else {
                const auth_schema = Validation.object({
                    tz : Validation.string().regex(/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/).required(),
                    type: Validation.string().min(5).max(50).optional().valid('recomendation', 'fresh'),
                    terms: Validation.string().min(3).max(50).optional(),
                    paginate : Validation.string().regex(/^\d+$/).optional(),
                    limit : Validation.string().regex(/^\d+$/).optional(),
                })
                let validationprc = await MicroH.validation(data, auth_schema)
                if(validationprc.success){
                    if(!validationprc.data.paginate) {
                        validationprc.data.paginate = Config.get('variables.default_values.page_paginate')
                    }
                    if(!validationprc.data.limit) {
                        validationprc.data.limit = Config.get('variables.default_values.page_limit')
                    }
                    let result_1 = ChannelForumPost
                    .query()
                    .select('id', 'xid', 'channel_id','title', 'description', 
                        Database.raw('CONVERT_TZ(created_at,"+07:00",?) created_at', [validationprc.data.tz]),
                        Database.raw('(SELECT COUNT(id) likes from channel_forum_likes WHERE channel_forum_content_type_id = ? AND ref_id = channel_forum_posts.id AND unlike = 0) total_likes', [Env.get('FORUM_POST_CONTENT_TYPE')]),
                        Database.raw('(SELECT COUNT(id) comments from channel_forum_comments WHERE channel_forum_post_id = channel_forum_posts.id) total_comments'),
                        Database.raw('(SELECT COUNT(id) liked from channel_forum_likes WHERE channel_forum_content_type_id = ? AND ref_id = channel_forum_posts.id AND channel_subscriber_id =? AND unlike = 0) _liked', [Env.get('FORUM_POST_CONTENT_TYPE'), usersession.id])
                    )
                    .with('_v1_channel', (builder) => {
                        builder
                        .select('id', 'channel_name', 'logo')
                    })
                    .with('_v1_channel_forum_attachments', (builder) => {
                        builder
                        .select('id', 'ref_id', 'attachment_name', 'attachment_type', 'attachment_url')
                        .where('channel_forum_content_type_id', [Env.get('FORUM_POST_CONTENT_TYPE')])
                    })
                    //.where('channel_id', usersession.channel_id)
                    .where('is_published', 1)
                    .where('is_archived', 0)
                    switch (validationprc.data.type) {
                        case 'fresh':
                            result_1
                            .orderBy('id', 'desc')
                            break
                        case 'recomendation':
                            result_1
                            .orderBy('id', 'desc')
                            break
                        default:
                            result_1
                            .orderBy('id', 'desc')
                    }
                    if(validationprc.data.terms){
                        result_1.where((builder) => {
                            builder.where('title', 'LIKE', `%${validationprc.data.terms}%`)
                            .orWhere('description', 'LIKE', `%${validationprc.data.terms}%`)
                        })
                    }
                    result_1 = await result_1
                    .paginate(validationprc.data.paginate, validationprc.data.limit)
                    if(result_1){
                        result_1 = result_1.toJSON()
                        if(result_1.data.length > 0){
                            let _r_data = []
                            for(const key in result_1.data){
                                let r_data = result_1.data[key]
                                r_data.liked = false
                                if(r_data._liked > 0){
                                    r_data.liked = true
                                }
                                r_data.channel = null
                                if(r_data._v1_channel){
                                    r_data.channel = _.pick(r_data._v1_channel, ['channel_name', 'logo'])
                                }
                                r_data.attachments = []
                                for(const key2 in r_data._v1_channel_forum_attachments){
                                    let attachment = r_data._v1_channel_forum_attachments[key2]
                                    attachment = _.pick(attachment, ['attachment_name', 'attachment_type', 'attachment_url'])
                                    r_data.attachments.push(attachment)
                                }
                        
                                const _pick = _.pick(r_data, ['xid', 'channel','title', 'description', 'total_comments', 'total_likes' ,'liked', 'attachments', 'created_at'])
                                _r_data.push(_pick)
                            }
                            result_1.data = _r_data
                            options.reason = Dictionary.TR000004
                        } 
                        dataset = result_1
                    }
                } else {
                    options.type = 'ERR_412'
                    options.reason  = await MicroH.reason('TR000003', validationprc.message)
                } 
            }
            if(dataset){
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async action(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            data.uid = usersession.uid
            const auth_schema = Validation.object({
                uid : Validation.string().alphanum().min(25).max(30).required(),
                xid : Validation.string().guid().required(),
                action: Validation.string().min(4).max(7).required().valid('like', 'unlike'),
            })
            let validationprc = await MicroH.validation(data, auth_schema) 
            if(validationprc.success){
                let channelpost = await ChannelForumPost
                .query()
                .with('_v1_channel_forum_like', (builder) => {
                    builder.where('channel_forum_content_type_id', Env.get('FORUM_POST_CONTENT_TYPE'))
                    builder.where('channel_subscriber_id', usersession.id)
                })
                .where('channel_id', usersession.channel_id)
                .where('xid', validationprc.data.xid)
                .where('is_published', 1)
                .where('is_archived', 0)
                .first()
                if(channelpost){
                    channelpost = channelpost.toJSON()
                    let slike = channelpost._v1_channel_forum_like
                    if(validationprc.data.action == 'like'){
                        if(slike){
                            let exec_1 = null                    
                            if(slike.unlike){
                                exec_1 = await ChannelForumLike.query()
                                .where('id',  slike.id)
                                .update({ 
                                    unlike : 0
                                })
                                if(exec_1){
                                    dataset = {
                                        xid : channelpost.xid
                                    }
                                } 
                            } else {
                                options.type = 'ERR_409'
                                options.reason = Dictionary.TR000010
                            }
                        } else {
                            let exec_1 = null
                            const record_1 = {
                                ref_id : channelpost.id,
                                channel_forum_content_type_id : Env.get('FORUM_POST_CONTENT_TYPE'),
                                channel_subscriber_id : usersession.id,
                                like_type : 'like'
                            }
                            exec_1 = await ChannelForumLike.create(record_1)
                            if(exec_1){
                                dataset = {
                                    xid : channelpost.xid
                                }
                            } 
                        } 
                    } else if(validationprc.data.action == 'unlike') {
                        if(slike){
                            let exec_1 = null                       
                            if(!slike.unlike){
                                exec_1 = await ChannelForumLike.query()
                                .where('id',  slike.id)
                                .update({ 
                                    unlike : 1
                                })
                                if(exec_1){
                                    dataset = {
                                        xid : channelpost.xid
                                    }
                                } 
                            } else {
                                options.type = 'ERR_409'
                                options.reason = Dictionary.TR000010
                            }
                        } 
                    }
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async comment(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            data.uid = usersession.uid
            const auth_schema = Validation.object({
                uid : Validation.string().alphanum().min(25).max(30).required(),
                xid : Validation.string().guid().required(),
                comment: Validation.string().min(1).max(1000).required(),
                reply_to: Validation.string().guid().optional()
            })
            let validationprc = await MicroH.validation(data, auth_schema) 
            if(validationprc.success){
                let channelpost = await ChannelForumPost
                .query()
                .where('channel_id', usersession.channel_id)
                .where('xid', validationprc.data.xid)
                .where('is_published', 1)
                .where('is_archived', 0)
                .first()
                if(channelpost){
                    channelpost = channelpost.toJSON()
                    let reply_to_channel_subscriber_id = 0
                    let parent_id = 0
                    let reply_to = 0
                    let is_valid = false
                    if(validationprc.data.reply_to){
                        let channelpostcomment = await ChannelForumComment
                        .query()
                        .where('channel_forum_post_id', channelpost.id)
                        .where('xid', validationprc.data.reply_to)
                        .where('is_spammed', 0)
                        .where('is_archived', 0)
                        .first()
                        if(channelpostcomment){
                            if(channelpostcomment.parent_id){
                                parent_id = channelpostcomment.parent_id
                            } else {
                                parent_id = channelpostcomment.id
                            }
                            reply_to_channel_subscriber_id = channelpostcomment.channel_subscriber_id
                            reply_to = channelpostcomment.id
                            is_valid = true
                        }
                    } else { is_valid = true }

                    if(is_valid){
                        let exec_1 = null
                        const record_1 = {
                            xid : uuid.v1(),
                            parent_id : parent_id,
                            reply_to : reply_to,
                            reply_to_channel_subscriber_id : reply_to_channel_subscriber_id,
                            channel_forum_post_id : channelpost.id,
                            channel_subscriber_id : usersession.id,
                            comment : validationprc.data.comment
                        }
                        exec_1 = await ChannelForumComment.create(record_1)
                        if(exec_1){
                            dataset = {
                                xid : exec_1.xid
                            }
                        } 
                    }
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async commentaction(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            data.uid = usersession.uid
            const auth_schema = Validation.object({
                uid : Validation.string().alphanum().min(25).max(30).required(),
                xid : Validation.string().guid().required(),
                action: Validation.string().min(4).max(7).required().valid('like', 'unlike'),
            })
            let validationprc = await MicroH.validation(data, auth_schema) 
            if(validationprc.success){
                let channelforumcomment = await ChannelForumComment
                .query()
                .with('_v1_channel_forum_like', (builder) => {
                    builder.where('channel_forum_content_type_id', Env.get('FORUM_COMMENT_CONTENT_TYPE'))
                    builder.where('channel_subscriber_id', usersession.id)
                })
                .whereHas('_v1_channel_forum_post', (builder) => {
                    builder.where('channel_id', usersession.channel_id)
                })
                .where('xid', validationprc.data.xid)
                .where('is_spammed', 0)
                .where('is_archived', 0)
                .first()
                if(channelforumcomment){
                    channelforumcomment = channelforumcomment.toJSON()
                    let slike = channelforumcomment._v1_channel_forum_like
                    if(validationprc.data.action == 'like'){
                        if(slike){
                            let exec_1 = null                    
                            if(slike.unlike){
                                exec_1 = await ChannelForumLike.query()
                                .where('id',  slike.id)
                                .update({ 
                                    unlike : 0
                                })
                                if(exec_1){
                                    dataset = {
                                        xid : channelforumcomment.xid
                                    }
                                } 
                            } else {
                                options.type = 'ERR_409'
                                options.reason = Dictionary.TR000010
                            }
                        } else {
                            let exec_1 = null
                            const record_1 = {
                                ref_id : channelforumcomment.id,
                                channel_forum_content_type_id : Env.get('FORUM_COMMENT_CONTENT_TYPE'),
                                channel_subscriber_id : usersession.id,
                                like_type : 'like'
                            }
                            exec_1 = await ChannelForumLike.create(record_1)
                            if(exec_1){
                                dataset = {
                                    xid : channelforumcomment.xid
                                }
                            } 
                        } 
                    } else if(validationprc.data.action == 'unlike') {
                        if(slike){
                            let exec_1 = null                       
                            if(!slike.unlike){
                                exec_1 = await ChannelForumLike.query()
                                .where('id',  slike.id)
                                .update({ 
                                    unlike : 1
                                })
                                if(exec_1){
                                    dataset = {
                                        xid : channelforumcomment.xid
                                    }
                                } 
                            } else {
                                options.type = 'ERR_409'
                                options.reason = Dictionary.TR000010
                            }
                        } 
                    }
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async getcomment(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'SUC_200'
            options.reason = Dictionary.TR000005
            const auth_schema = Validation.object({
                xid : Validation.string().guid().required(),
                tz : Validation.string().regex(/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/).required(),
                type: Validation.string().min(5).max(50).optional().valid('recomendation', 'fresh'),
                terms: Validation.string().min(3).max(50).optional(),
                paginate : Validation.string().regex(/^\d+$/).optional(),
                limit : Validation.string().regex(/^\d+$/).optional(),
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            if(validationprc.success){
                if(!validationprc.data.paginate) {
                    validationprc.data.paginate = Config.get('variables.default_values.page_paginate')
                }
                if(!validationprc.data.limit) {
                    validationprc.data.limit = Config.get('variables.default_values.page_limit')
                }
                let result_1 = ChannelForumComment
                .query()
                .select('id', 'xid', 'comment','channel_forum_post_id', 'channel_subscriber_id', 
                    Database.raw('CONVERT_TZ(created_at,"+07:00",?) created_at', [validationprc.data.tz]),
                    Database.raw('(SELECT COUNT(id) comments from channel_forum_comments cfc WHERE cfc.parent_id = channel_forum_comments.id) total_replies'),
                    Database.raw('(SELECT COUNT(id) liked from channel_forum_likes WHERE channel_forum_content_type_id = ? AND ref_id = channel_forum_comments.id AND channel_subscriber_id =? AND unlike = 0) _liked', [Env.get('FORUM_COMMENT_CONTENT_TYPE'), usersession.id])
                )
                .with('_v1_channel_subscriber')
                .whereHas('_v1_channel_subscriber', (builder) => {
                    builder
                    .where('blocked', 0)
                })
                .with('_v1_forum_comment_children_preview', (builder) => {
                    builder
                    .select('id', 'channel_subscriber_id', 'xid', 'parent_id', 'comment', 
                        Database.raw('CONVERT_TZ(created_at,"+07:00",?) created_at', [validationprc.data.tz]),
                        Database.raw('(SELECT COUNT(id) comments from channel_forum_comments cfc WHERE cfc.parent_id = channel_forum_comments.id) total_replies'),
                        Database.raw('(SELECT COUNT(id) liked from channel_forum_likes WHERE channel_forum_content_type_id = ? AND ref_id = channel_forum_comments.id AND channel_subscriber_id =? AND unlike = 0) _liked', [Env.get('FORUM_COMMENT_CONTENT_TYPE'), usersession.id])
                    )
                })
                .whereHas('_v1_channel_forum_post', (builder) => {
                    builder
                    .where('channel_id', usersession.channel_id)
                    .where('xid', validationprc.data.xid)
                })
                .where('is_spammed', 0)
                .where('is_archived', 0)
                .where('parent_id', 0)

                switch (validationprc.data.type) {
                    case 'fresh':
                        result_1
                        .orderBy('id', 'desc')
                        break
                    case 'recomendation':
                        result_1
                        .orderBy('id', 'desc')
                        break
                    default:
                        result_1
                        .orderBy('id', 'desc')
                }
                if(validationprc.data.terms){
                    result_1.where((builder) => {
                        builder.where('comment', 'LIKE', `%${validationprc.data.terms}%`)
                    })
                }
                
                result_1 = await result_1
                .paginate(validationprc.data.paginate, validationprc.data.limit)
                if(result_1){
                    result_1 = result_1.toJSON()
                    if(result_1.data.length > 0){
                        let _r_data = []
                        for(const key in result_1.data){
                            let r_data = result_1.data[key]
                            r_data.liked = false
                            if(r_data._liked > 0){
                                r_data.liked = true
                            }
                            r_data.by = null
                            if(r_data._v1_channel_subscriber){
                                r_data.by = _.pick(r_data._v1_channel_subscriber, ['xid', 'uid'])
                            }
                            r_data.preview_replies = []
                            for(const key2 in r_data._v1_forum_comment_children_preview){
                                let prep = r_data._v1_forum_comment_children_preview[key2]
                                prep.liked = false
                                if(prep._liked > 0){
                                    prep.liked = true
                                }
                                if(prep._v1_channel_subscriber){
                                    prep.by = _.pick(prep._v1_channel_subscriber, ['xid', 'uid'])
                                }
                                prep = _.pick(prep, ['xid', 'comment', 'total_replies', 'liked', 'by', 'created_at'])
                                r_data.preview_replies.push(prep)
                            }
                    
                            const _pick = _.pick(r_data, ['xid', 'comment', 'total_replies', 'total_likes' ,'liked', 'by', 'created_at', 'preview_replies'])
                            _r_data.push(_pick)
                        }
                        result_1.data = _r_data
                        options.reason = Dictionary.TR000004
                    } 
                    dataset = result_1
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async getreply(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'SUC_200'
            options.reason = Dictionary.TR000005
            const auth_schema = Validation.object({
                xid : Validation.string().guid().required(),
                tz : Validation.string().regex(/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/).required(),
                type: Validation.string().min(5).max(50).optional().valid('recomendation', 'fresh'),
                terms: Validation.string().min(3).max(50).optional(),
                paginate : Validation.string().regex(/^\d+$/).optional(),
                limit : Validation.string().regex(/^\d+$/).optional(),
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            if(validationprc.success){
                if(!validationprc.data.paginate) {
                    validationprc.data.paginate = Config.get('variables.default_values.page_paginate')
                }
                if(!validationprc.data.limit) {
                    validationprc.data.limit = Config.get('variables.default_values.page_limit')
                }
                let result_1 = ChannelForumComment
                .query()
                .select('id', 'xid', 'parent_id', 'comment','channel_forum_post_id', 'channel_subscriber_id', 
                    Database.raw('CONVERT_TZ(created_at,"+07:00",?) created_at', [validationprc.data.tz]),
                    Database.raw('(SELECT COUNT(id) comments from channel_forum_comments cfc WHERE cfc.parent_id = channel_forum_comments.id) total_replies'),
                    Database.raw('(SELECT COUNT(id) liked from channel_forum_likes WHERE channel_forum_content_type_id = ? AND ref_id = channel_forum_comments.id AND channel_subscriber_id =? AND unlike = 0) _liked', [Env.get('FORUM_COMMENT_CONTENT_TYPE'), usersession.id])
                )
                .with('_v1_channel_subscriber')
                .whereHas('_v1_channel_subscriber', (builder) => {
                    builder
                    .where('blocked', 0)
                })
                .whereHas('_v1_channel_forum_post', (builder) => {
                    builder
                    .where('channel_id', usersession.channel_id)
                })
                .whereHas('_v1_forum_comment_parent', (builder) => {
                    builder
                    .where('xid', validationprc.data.xid)
                })
                .where('is_spammed', 0)
                .where('is_archived', 0)

                switch (validationprc.data.type) {
                    case 'fresh':
                        result_1
                        .orderBy('id', 'desc')
                        break
                    case 'recomendation':
                        result_1
                        .orderBy('id', 'desc')
                        break
                    default:
                        result_1
                        .orderBy('id', 'desc')
                }
                if(validationprc.data.terms){
                    result_1.where((builder) => {
                        builder.where('comment', 'LIKE', `%${validationprc.data.terms}%`)
                    })
                }
                
                result_1 = await result_1
                .paginate(validationprc.data.paginate, validationprc.data.limit)
                if(result_1){
                    result_1 = result_1.toJSON()
                    if(result_1.data.length > 0){
                        let _r_data = []
                        for(const key in result_1.data){
                            let r_data = result_1.data[key]
                            r_data.liked = false
                            if(r_data._liked > 0){
                                r_data.liked = true
                            }
                            r_data.by = null
                            if(r_data._v1_channel_subscriber){
                                r_data.by = _.pick(r_data._v1_channel_subscriber, ['xid', 'uid'])
                            }
                            const _pick = _.pick(r_data, ['xid', 'comment', 'total_likes' ,'liked', 'by', 'created_at'])
                            _r_data.push(_pick)
                        }
                        result_1.data = _r_data
                        options.reason = Dictionary.TR000004
                    } 
                    dataset = result_1
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async getcommentdetail(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'SUC_200'
            options.reason = Dictionary.TR000005
            const auth_schema = Validation.object({
                xid : Validation.string().guid().required(),
                tz : Validation.string().regex(/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/).required()
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            if(validationprc.success){
                let result = await ChannelForumComment
                .query()
                .select('id', 'xid', 'parent_id', 'comment','channel_forum_post_id', 'channel_subscriber_id', 
                    Database.raw('CONVERT_TZ(created_at,"+07:00",?) created_at', [validationprc.data.tz]),
                    Database.raw('(SELECT COUNT(id) comments from channel_forum_comments cfc WHERE cfc.parent_id = channel_forum_comments.id) total_replies'),
                    Database.raw('(SELECT COUNT(id) liked from channel_forum_likes WHERE channel_forum_content_type_id = ? AND ref_id = channel_forum_comments.id AND channel_subscriber_id =? AND unlike = 0) _liked', [Env.get('FORUM_COMMENT_CONTENT_TYPE'), usersession.id])
                )
                .with('_v1_channel_subscriber')
                .whereHas('_v1_channel_subscriber', (builder) => {
                    builder
                    .where('blocked', 0)
                })
                .whereHas('_v1_channel_forum_post', (builder) => {
                    builder
                    .where('channel_id', usersession.channel_id)
                })
                .where('xid', validationprc.data.xid)
                .where('is_spammed', 0)
                .where('is_archived', 0)
                .first()
                if(result){
                    result = result.toJSON()
                    let r_data = result
                    r_data.liked = false
                    if(r_data._liked > 0){
                        r_data.liked = true
                    }
                    r_data.by = null
                    if(r_data._v1_channel_subscriber){
                        r_data.by = _.pick(r_data._v1_channel_subscriber,  ['xid', 'uid'])
                    }
                    const result_1 = _.pick(r_data,  ['xid', 'comment', 'total_likes' ,'liked', 'by', 'created_at'])
                    dataset = result_1
                    options.reason = Dictionary.TR000004
                }
            }else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 
            if(dataset){
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

}

module.exports = new ChannelForumMicro

