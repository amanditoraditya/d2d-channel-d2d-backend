'use strict'

const SystemLog = use('App/Models/SystemLog')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const MyHash = require('./Helper/Hash.js')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const Env = use('Env')
const Logger = use('Logger')

class SystemLogController {
	async datatable({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('SystemLog', 'read', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'system_logs',
			sSelectSql: ['system_logs.id', 'system_logs.user_id', 'system_logs.access', 'system_logs.ip', 'system_logs.user_agent', 'system_logs.browser', 'system_logs.cpu', 'system_logs.device', 'system_logs.engine', 'system_logs.os', 'system_logs.url', 'system_logs.method', 'system_logs.param', 'system_logs.body', 'system_logs.response', 'users.fullname'],
			aSearchColumns: ['system_logs.user_id', 'system_logs.access', 'system_logs.ip', 'system_logs.user_agent', 'system_logs.browser', 'system_logs.cpu', 'system_logs.device', 'system_logs.engine', 'system_logs.os', 'system_logs.url', 'system_logs.method', 'system_logs.param', 'system_logs.body', 'system_logs.response', 'users.fullname'],
			sFromSql: "system_logs \
				LEFT JOIN users ON system_logs.user_id=users.id"
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			fdata.push([
				select[0][x]['id'],
				select[0][x]['fullname'],
				select[0][x]['access'],
				select[0][x]['ip'],
				select[0][x]['user_agent'],
				select[0][x]['browser'],
				select[0][x]['cpu'],
				select[0][x]['device'],
				select[0][x]['engine'],
				select[0][x]['os'],
				select[0][x]['url'],
				select[0][x]['method'],
				select[0][x]['param'],
				select[0][x]['body'],
				select[0][x]['response'],
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
}

module.exports = SystemLogController