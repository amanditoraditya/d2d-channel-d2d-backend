'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelTypesSchema extends Schema {
  up () {
    this.create('channel_types', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.string('name', 255).notNullable()
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_types')
  }
}

module.exports = ChannelTypesSchema
