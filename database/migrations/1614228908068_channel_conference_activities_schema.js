'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelConferenceActivitiesSchema extends Schema {
  up () {
    this.table('channel_conference_activities', (table) => {
      // alter table
      table.string('general_ua', 200).nullable().after('cpu_architecture')
    })
  }

  down () {
    this.table('channel_conference_activities', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelConferenceActivitiesSchema
