'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelForumFeaturedPostsSchema extends Schema {
  up () {
    this.create('channel_forum_featured_posts', (table) => {
      table.increments()
      table.integer('channel_forum_post_id').unsigned().references('id').inTable('channel_forum_posts')
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_forum_featured_posts')
  }
}

module.exports = ChannelForumFeaturedPostsSchema
