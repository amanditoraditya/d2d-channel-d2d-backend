'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelBannersSchema extends Schema {
  up () {
    this.create('channel_banners', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_id').unsigned().references('id').inTable('channels')
      table.integer('position', 5).defaultTo(0)
      table.string('image', 255).notNullable()
      table.string('action', 10).nullable()
      table.boolean('is_active').defaultTo(true)
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_banners')
  }
}

module.exports = ChannelBannersSchema
