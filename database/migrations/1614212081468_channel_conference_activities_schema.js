'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelConferenceActivitiesSchema extends Schema {
  up () {
    this.create('channel_conference_activities', (table) => {
      table.increments()
      table.integer('conference_id').unsigned().references('id').inTable('channel_conferences')
      table.integer('channel_subscriber_id').unsigned().references('id').inTable('channel_subscribers')
      table.string('type', 50).notNullable()
      table.string('gmt_tz', 10).nullable()
      table.string('ip', 50).nullable()
      table.string('access_country', 50).nullable()
      table.string('browser', 50).nullable()
      table.string('device', 50).nullable()
      table.string('os', 50).nullable()
      table.string('os_version', 50).nullable()
      table.string('engine_name', 50).nullable()
      table.string('cpu_architecture', 50).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_conference_activities')
  }
}

module.exports = ChannelConferenceActivitiesSchema
