'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelRoleMenusSchema extends Schema {
  up () {
    this.create('channel_role_menus', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_role_id').unsigned().references('id').inTable('channel_roles')
      table.integer('channel_menu_id').unsigned().references('id').inTable('channel_menus')
      table.boolean('read').defaultTo(false)
      table.boolean('write').defaultTo(false)
      table.boolean('update').defaultTo(false)
      table.boolean('delete').defaultTo(false)
      table.boolean('status').defaultTo(true)
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.unique(['channel_role_id', 'channel_menu_id'])
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_role_menus')
  }
}

module.exports = ChannelRoleMenusSchema
