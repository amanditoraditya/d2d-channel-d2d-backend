'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelForumCommentsSchema extends Schema {
  up () {
    this.table('channel_forum_comments', (table) => {
      // alter table
      table.integer('reply_to_channel_subscriber_id').defaultTo(0).after('channel_subscriber_id')
    })
  }

  down () {
    this.table('channel_forum_comments', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelForumCommentsSchema
