'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelsSchema extends Schema {
  up () {
    this.table('channels', (table) => {
      // alter table
      table.string('gmt_tz', 10).notNullable().defaultTo('+07:00').after('description')
    })
  }

  down () {
    this.table('channels', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelsSchema
