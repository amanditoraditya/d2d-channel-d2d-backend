'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelBannersSchema extends Schema {
  up () {
    this.table('channel_banners', (table) => {
      // alter table
      table.string('link', 255).nullable().after('image')
      table.string('target', 255).nullable().after('image')
      table.string('name', 255).nullable().after('image')
    })
  }

  down () {
    this.table('channel_banners', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelBannersSchema
