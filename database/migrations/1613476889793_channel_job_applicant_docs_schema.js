'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelJobApplicantDocsSchema extends Schema {
  up () {
    this.create('channel_job_applicant_docs', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_job_applicant_id').unsigned().references('id').inTable('channel_job_applicants')
      table.integer('ref_id', 50).defaultTo(0)
      table.string('docs', 255).notNullable()
      table.string('docs_type', 10).notNullable()
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_job_applicant_docs')
  }
}

module.exports = ChannelJobApplicantDocsSchema
