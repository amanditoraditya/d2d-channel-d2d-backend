'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelForumPostsSchema extends Schema {
  up () {
    this.create('channel_forum_posts', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_id').unsigned().references('id').inTable('channels')
      table.string('title', 255).notNullable()
      table.text('description', 'longtext').nullable()
      table.boolean('is_published').defaultTo(false)
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_forum_posts')
  }
}

module.exports = ChannelForumPostsSchema
