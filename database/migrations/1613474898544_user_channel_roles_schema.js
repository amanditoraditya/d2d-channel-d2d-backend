'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserChannelRolesSchema extends Schema {
  up () {
    this.create('user_channel_roles', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('user_channel_id').unsigned().references('id').inTable('user_channels')
      table.integer('channel_role_id').unsigned().references('id').inTable('channel_roles')
      table.boolean('status').defaultTo(true)
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('user_channel_roles')
  }
}

module.exports = UserChannelRolesSchema
