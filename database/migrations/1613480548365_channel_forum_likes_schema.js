'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelForumLikesSchema extends Schema {
  up () {
    this.create('channel_forum_likes', (table) => {
      table.increments()
      table.integer('ref_id', 100).notNullable()
      table.integer('channel_forum_content_type_id').unsigned().references('id').inTable('channel_forum_content_types')
      table.integer('channel_subscriber_id').unsigned().references('id').inTable('channel_subscribers')
      table.string('like_type', 10).notNullable()
      table.boolean('unlike').defaultTo(false)
      table.boolean('exlude').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.unique(['ref_id', 'channel_forum_content_type_id', 'channel_subscriber_id'], 'cforum_likes_refid_cforum_contenttype_id_csubscriber_id_unique')
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_forum_likes')
  }
}

module.exports = ChannelForumLikesSchema
