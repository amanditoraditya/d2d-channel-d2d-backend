'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelJobApplicantsSchema extends Schema {
  up () {
    this.create('channel_job_applicants', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_job_id').unsigned().references('id').inTable('channel_jobs')
      table.integer('channel_subscriber_id').unsigned().references('id').inTable('channel_subscribers')
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.unique(['channel_job_id', 'channel_subscriber_id'], 'channel_job_applicants_cjob_id_csubscriber_id_unique')
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_job_applicants')
  }
}

module.exports = ChannelJobApplicantsSchema
