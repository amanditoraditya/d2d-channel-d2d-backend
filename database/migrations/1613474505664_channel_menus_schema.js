'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelMenusSchema extends Schema {
  up () {
    this.create('channel_menus', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_id').unsigned().references('id').inTable('channels')
      table.integer('menu_id').unsigned().references('id').inTable('menus')
      table.integer('position', 5).defaultTo(0)
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.unique(['channel_id', 'menu_id'])
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_menus')
  }
}

module.exports = ChannelMenusSchema
