'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelConferencesSchema extends Schema {
  up () {
    this.table('channel_conferences', (table) => {
      // alter table
      table.string('gmt_tz', 10).notNullable().defaultTo('+07:00').after('end_date')
    })
  }

  down () {
    this.table('channel_conferences', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelConferencesSchema
