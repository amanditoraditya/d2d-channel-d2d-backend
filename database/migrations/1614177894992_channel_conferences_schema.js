'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelConferencesSchema extends Schema {
  up () {
    this.table('channel_conferences', (table) => {
      table.datetime('registration_until').nullable().after('gmt_tz')
      table.integer('max_attendees', 7).defaultTo(0).after('meet_id')
      // alter table
    })
  }

  down () {
    this.table('channel_conferences', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelConferencesSchema
