'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelJobsSchema extends Schema {
  up () {
    this.create('channel_jobs', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_id').unsigned().references('id').inTable('channels')
      table.string('title', 255).notNullable()
      table.integer('triggered_by', 100).defaultTo(0)
      table.text('description', 'longtext').nullable()
      table.string('image', 255).nullable()
      table.boolean('is_published').defaultTo(false)
      table.datetime('due_date').notNullable()
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_jobs')
  }
}

module.exports = ChannelJobsSchema
