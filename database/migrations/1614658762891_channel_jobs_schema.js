'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelJobsSchema extends Schema {
  up () {
    this.table('channel_jobs', (table) => {
      // alter table
      table.string('apply_button', 100).notNullable().after('image')
    })
  }

  down () {
    this.table('channel_jobs', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelJobsSchema
