-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 10, 2021 at 04:09 AM
-- Server version: 5.7.14
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `empty_engine`
--

-- --------------------------------------------------------

--
-- Table structure for table `adonis_schema`
--

DROP TABLE IF EXISTS `adonis_schema`;
CREATE TABLE IF NOT EXISTS `adonis_schema` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `batch` int(11) DEFAULT NULL,
  `migration_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `api_services`
--

DROP TABLE IF EXISTS `api_services`;
CREATE TABLE IF NOT EXISTS `api_services` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) DEFAULT NULL,
  `client_key` varchar(255) DEFAULT NULL,
  `client_secret` varchar(255) DEFAULT NULL,
  `permissions` text,
  `status` int(11) DEFAULT NULL,
  `created_by` int(10) NOT NULL DEFAULT '1',
  `updated_by` int(10) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `api_services`
--

INSERT INTO `api_services` (`id`, `client_name`, `client_key`, `client_secret`, `permissions`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Engine', 'Ha1XU73ZHCKGsh6lH4jGSL5wg80J2PE7', 'f6pwRZ5vpokaepHlF1wql1oATKtzjLMzXNOxp4GSdjkqyfcQbVV0uEr73wuQ51md', 'user', 0, 1, 1, '2018-10-08 17:53:26', '2018-10-08 17:58:46');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_title` varchar(255) DEFAULT NULL,
  `role_slug` varchar(255) DEFAULT NULL,
  `role_access` longtext,
  `created_by` int(10) DEFAULT '1',
  `updated_by` int(10) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_title`, `role_slug`, `role_access`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 'superadmin', '[{\"component\":\"Account\",\"create\":\"1\",\"read\":\"1\",\"update\":\"1\",\"delete\":\"1\"},{\"component\":\"Home\",\"create\":\"1\",\"read\":\"1\",\"update\":\"1\",\"delete\":\"1\"},{\"component\":\"Role\",\"create\":\"1\",\"read\":\"1\",\"update\":\"1\",\"delete\":\"1\"},{\"component\":\"Setting\",\"create\":\"1\",\"read\":\"1\",\"update\":\"1\",\"delete\":\"1\"},{\"component\":\"SystemLog\",\"create\":\"1\",\"read\":\"1\",\"update\":\"1\",\"delete\":\"1\"},{\"component\":\"User\",\"create\":\"1\",\"read\":\"1\",\"update\":\"1\",\"delete\":\"1\"}]', 1, 1, '2019-12-06 11:26:08', '2021-02-10 10:17:24'),
(2, 'Administrator', 'administrator', '[{\"component\":\"Account\",\"create\":\"1\",\"read\":\"1\",\"update\":\"1\",\"delete\":\"1\"},{\"component\":\"Home\",\"create\":\"1\",\"read\":\"1\",\"update\":\"1\",\"delete\":\"1\"},{\"component\":\"Role\",\"create\":\"0\",\"read\":\"0\",\"update\":\"0\",\"delete\":\"0\"},{\"component\":\"Setting\",\"create\":\"1\",\"read\":\"1\",\"update\":\"1\",\"delete\":\"1\"},{\"component\":\"SystemLog\",\"create\":\"1\",\"read\":\"1\",\"update\":\"1\",\"delete\":\"1\"},{\"component\":\"User\",\"create\":\"1\",\"read\":\"1\",\"update\":\"1\",\"delete\":\"1\"}]', 1, 1, '2019-12-06 11:42:28', '2021-02-10 10:17:34'),
(3, 'Pegawai', 'pegawai', '[{\"component\":\"Account\",\"create\":\"0\",\"read\":\"0\",\"update\":\"0\",\"delete\":\"0\"},{\"component\":\"Home\",\"create\":\"1\",\"read\":\"1\",\"update\":\"1\",\"delete\":\"1\"},{\"component\":\"Role\",\"create\":\"0\",\"read\":\"0\",\"update\":\"0\",\"delete\":\"0\"},{\"component\":\"SystemLog\",\"create\":\"0\",\"read\":\"0\",\"update\":\"0\",\"delete\":\"0\"},{\"component\":\"User\",\"create\":\"0\",\"read\":\"1\",\"update\":\"1\",\"delete\":\"1\"}]', 1, 1, '2021-02-09 15:12:44', '2021-02-09 15:21:09');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `created_by` int(10) DEFAULT '1',
  `updated_by` int(10) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, '2018-09-25 00:00:00', '2020-02-17 18:45:52'),
(2, 2, 2, 1, 1, '2018-10-04 19:51:45', '2020-02-17 18:45:45');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `groups` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'general',
  `options` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text,
  `serialized` tinyint(1) DEFAULT '0',
  `created_by` int(10) DEFAULT '1',
  `updated_by` int(10) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `groups`, `options`, `value`, `serialized`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'general', 'meta_title', 'Empty Engine Next', 0, 1, 1, '2018-03-12 15:09:43', '2018-10-09 14:05:41'),
(2, 'general', 'meta_description', 'Ini adalah engine untuk pembuatan sistem dengan NodeJS yang telah dilengkapi dengan AdonisJS, ReactJS dan NextJS untuk support SSR', 0, 1, 1, '2018-03-12 15:10:44', '2018-10-09 14:05:41'),
(3, 'general', 'meta_keyword', 'Engine', 0, 1, 1, '2018-03-12 15:12:38', '2018-10-09 14:05:41'),
(4, 'profile', 'web_name', 'Engine', 0, 1, 1, '2018-03-12 15:59:34', '2018-10-09 14:05:41'),
(5, 'profile', 'web_owner', 'PT Global Urban Esensial', 0, 1, 1, '2018-03-12 16:00:00', '2018-10-09 14:05:41'),
(6, 'profile', 'address', 'DKI Jakarta', 0, 1, 1, '2018-03-12 16:03:02', '2018-10-09 14:05:41'),
(7, 'profile', 'geocode', '', 0, 1, 1, '2018-03-12 16:05:57', '2018-10-09 14:05:41'),
(8, 'profile', 'email', 'contact@engine.com', 0, 1, 1, '2018-03-12 16:06:30', '2018-10-09 14:05:41'),
(9, 'profile', 'telephone', '', 0, 1, 1, '2018-03-12 16:07:38', '2018-10-09 14:05:41'),
(10, 'profile', 'fax', '', 0, 1, 1, '2018-03-12 16:08:03', '2018-10-09 14:05:41'),
(11, 'image', 'image', '', 0, 1, 1, '2018-03-12 16:08:40', '2018-10-09 14:05:41'),
(12, 'profile', 'openning_times', '24 hours', 0, 1, 1, '2018-03-12 16:09:33', '2018-10-09 14:05:41'),
(13, 'local', 'country', '1', 0, 1, 1, '2018-04-09 14:55:52', '2018-10-09 14:05:41'),
(14, 'local', 'province', '19', 0, 1, 1, '2018-04-09 14:56:51', '2018-10-09 14:05:41'),
(15, 'local', 'language', '1', 0, 1, 1, '2018-04-09 14:57:16', '2018-10-09 14:05:41'),
(16, 'local', 'administration_language', '1', 0, 1, 1, '2018-04-09 14:58:03', '2018-10-09 14:05:41'),
(17, 'local', 'currency', '1', 0, 1, 1, '2018-04-09 14:58:30', '2018-10-09 14:05:41'),
(18, 'local', 'auto_update_currency', '1', 0, 1, 1, '2018-04-09 14:59:13', '2018-10-09 14:05:41'),
(19, 'local', 'length_class', '1', 0, 1, 1, '2018-04-09 15:00:05', '2018-10-09 14:05:41'),
(20, 'local', 'weight_class', '1', 0, 1, 1, '2018-04-09 15:00:29', '2018-10-09 14:05:41'),
(21, 'image', 'web_logo', '', 0, 1, 1, '2018-04-09 15:57:15', '2018-10-09 14:05:41'),
(22, 'image', 'icon', '', 0, 1, 1, '2018-04-09 15:57:35', '2018-10-09 14:05:41'),
(23, 'mail', 'mail_engine', 'mail', 0, 1, 1, '2018-04-09 16:14:10', '2018-10-09 14:05:42'),
(24, 'mail', 'mail_parameters', '', 0, 1, 1, '2018-04-09 16:14:45', '2018-10-09 14:05:42'),
(25, 'mail', 'smtp_hostname', '', 0, 1, 1, '2018-04-09 16:15:17', '2018-10-09 14:05:42'),
(26, 'mail', 'smtp_username', '', 0, 1, 1, '2018-04-09 16:15:57', '2018-10-09 14:05:42'),
(27, 'mail', 'smtp_password', '', 0, 1, 1, '2018-04-09 16:16:21', '2018-10-09 14:05:42'),
(28, 'mail', 'smtp_port', '465', 0, 1, 1, '2018-04-09 16:16:46', '2018-10-09 14:05:42'),
(29, 'mail', 'smtp_timeout', '5', 0, 1, 1, '2018-04-09 16:17:22', '2018-10-09 14:05:42'),
(30, 'server', 'maintenance_mode', '0', 0, 1, 1, '2018-04-10 10:22:26', '2018-10-09 14:05:42'),
(31, 'server', 'use_seo_url', '1', 0, 1, 1, '2018-04-10 10:23:03', '2018-10-09 14:05:42'),
(32, 'server', 'robot', 'abot,dbot,ebot,hbot,kbot,lbot,mbot,nbot,obot,pbot,rbot,sbot,tbot,vbot,ybot,zbot,bot.,bot/,_bot,.bot,/bot,-bot,:bot,(bot,crawl,slurp,spider,seek,accoona,acoon,adressendeutschland,ah-ha.com,ahoy,altavista,ananzi,anthill,appie,arachnophilia,arale,araneo,aranha,architext,aretha,arks,asterias,atlocal,atn,atomz,augurfind,backrub,bannana_bot,baypup,bdfetch,big brother,biglotron,bjaaland,blackwidow,blaiz,blog,blo.,bloodhound,boitho,booch,bradley,butterfly,calif,cassandra,ccubee,cfetch,charlotte,churl,cienciaficcion,cmc,collective,comagent,combine,computingsite,csci,curl,cusco,daumoa,deepindex,delorie,depspid,deweb,die blinde kuh,digger,ditto,dmoz,docomo,download express,dtaagent,dwcp,ebiness,ebingbong,e-collector,ejupiter,emacs-w3 search engine,esther,evliya celebi,ezresult,falcon,felix ide,ferret,fetchrover,fido,findlinks,fireball,fish search,fouineur,funnelweb,gazz,gcreep,genieknows,getterroboplus,geturl,glx,goforit,golem,grabber,grapnel,gralon,griffon,gromit,grub,gulliver,hamahakki,harvest,havindex,helix,heritrix,hku www octopus,homerweb,htdig,html index,html_analyzer,htmlgobble,hubater,hyper-decontextualizer,ia_archiver,ibm_planetwide,ichiro,iconsurf,iltrovatore,image.kapsi.net,imagelock,incywincy,indexer,infobee,informant,ingrid,inktomisearch.com,inspector web,intelliagent,internet shinchakubin,ip3000,iron33,israeli-search,ivia,jack,jakarta,javabee,jetbot,jumpstation,katipo,kdd-explorer,kilroy,knowledge,kototoi,kretrieve,labelgrabber,lachesis,larbin,legs,libwww,linkalarm,link validator,linkscan,lockon,lwp,lycos,magpie,mantraagent,mapoftheinternet,marvin/,mattie,mediafox,mediapartners,mercator,merzscope,microsoft url control,minirank,miva,mj12,mnogosearch,moget,monster,moose,motor,multitext,muncher,muscatferret,mwd.search,myweb,najdi,nameprotect,nationaldirectory,nazilla,ncsa beta,nec-meshexplorer,nederland.zoek,netcarta webmap engine,netmechanic,netresearchserver,netscoop,newscan-online,nhse,nokia6682/,nomad,noyona,nutch,nzexplorer,objectssearch,occam,omni,open text,openfind,openintelligencedata,orb search,osis-project,pack rat,pageboy,pagebull,page_verifier,panscient,parasite,partnersite,patric,pear.,pegasus,peregrinator,pgp key agent,phantom,phpdig,picosearch,piltdownman,pimptrain,pinpoint,pioneer,piranha,plumtreewebaccessor,pogodak,poirot,pompos,poppelsdorf,poppi,popular iconoclast,psycheclone,publisher,python,rambler,raven search,roach,road runner,roadhouse,robbie,robofox,robozilla,rules,salty,sbider,scooter,scoutjet,scrubby,search.,searchprocess,semanticdiscovery,senrigan,sg-scout,shai\'hulud,shark,shopwiki,sidewinder,sift,silk,simmany,site searcher,site valet,sitetech-rover,skymob.com,sleek,smartwit,sna-,snappy,snooper,sohu,speedfind,sphere,sphider,spinner,spyder,steeler/,suke,suntek,supersnooper,surfnomore,sven,sygol,szukacz,tach black widow,tarantula,templeton,/teoma,t-h-u-n-d-e-r-s-t-o-n-e,theophrastus,titan,titin,tkwww,toutatis,t-rex,tutorgig,twiceler,twisted,ucsd,udmsearch,url check,updated,vagabondo,valkyrie,verticrawl,victoria,vision-search,volcano,voyager/,voyager-hc,w3c_validator,w3m2,w3mir,walker,wallpaper,wanderer,wauuu,wavefire,web core,web hopper,web wombat,webbandit,webcatcher,webcopy,webfoot,weblayers,weblinker,weblog monitor,webmirror,webmonkey,webquest,webreaper,websitepulse,websnarf,webstolperer,webvac,webwalk,webwatch,webwombat,webzinger,whizbang,whowhere,wild ferret,worldlight,wwwc,wwwster,xenu,xget,xift,xirq,yandex,yanga,yeti,yodao,zao,zippp,zyborg', 0, 1, 1, '2018-04-10 10:26:05', '2018-10-09 14:05:42'),
(33, 'server', 'output_compression_level', '0', 0, 1, 1, '2018-04-10 10:27:46', '2018-10-09 14:05:42'),
(34, 'server', 'allow_forgotten_password', '1', 0, 1, 1, '2018-04-10 10:29:53', '2018-10-09 14:05:42'),
(35, 'server', 'encryption_key', 'juRq5x72Es54RLJGDJe5MZ6Unfh86zlE', 0, 1, 1, '2018-04-10 10:36:01', '2021-02-10 10:42:44');

-- --------------------------------------------------------

--
-- Table structure for table `system_logs`
--

DROP TABLE IF EXISTS `system_logs`;
CREATE TABLE IF NOT EXISTS `system_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `access` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `browser` text,
  `cpu` text,
  `device` text,
  `engine` text,
  `os` text,
  `url` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `param` varchar(255) DEFAULT NULL,
  `body` text,
  `response` text,
  `created_by` int(10) DEFAULT '1',
  `updated_by` int(10) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(80) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(60) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `user_type` int(10) DEFAULT '1',
  `user_role` int(10) DEFAULT '1',
  `social_token` text,
  `login_source` varchar(20) DEFAULT 'web',
  `block` enum('Y','N') DEFAULT 'N',
  `activation_key` varchar(60) DEFAULT NULL,
  `forget_key` varchar(60) DEFAULT NULL,
  `player_id` varchar(255) DEFAULT NULL,
  `player_id_mobile` varchar(255) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `telephone_otp` varchar(10) DEFAULT NULL,
  `telephone_verified` int(1) DEFAULT '0',
  `current_token` varchar(255) DEFAULT NULL,
  `created_by` int(10) DEFAULT '1',
  `updated_by` int(10) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `fullname`, `user_type`, `user_role`, `social_token`, `login_source`, `block`, `activation_key`, `forget_key`, `player_id`, `player_id_mobile`, `telephone`, `telephone_otp`, `telephone_verified`, `current_token`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'super@engine.com', '$2a$10$NqO/wqAmAKH/qD/bWdvMkuBtds/ns/pmLYJbHsRZRh3GJ9odXWlLa', 'Super Administrator', 1, 1, '', 'web', 'N', NULL, NULL, NULL, NULL, NULL, NULL, 0, '20210210092239', 1, 1, '2018-09-25 00:00:00', '2021-02-10 09:22:39'),
(2, 'administrator', 'admin@engine.com', '$2a$10$EwQ8ij7Nf.A3OD9PQliq4.P8fMRdml8xRspOg3T4gCW.jxSCKgPh.', 'Administrator', 1, 2, '', 'web', 'N', '$2a$10$oZlI4GLknJ/WjdhWmBS9cOUv5uuFw8/HqlVWNpRt4t83eUDC3sH1m', NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 1, '2018-10-04 19:51:45', '2020-11-25 11:41:45');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
