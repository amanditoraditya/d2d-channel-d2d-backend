# EmptyEngineNext

Ini adalah engine untuk pembuatan sistem dengan NodeJS yang telah dilengkapi dengan AdonisJS, ReactJS dan NextJS untuk support SSR.

Contact : tech@goapotik.com

### Tech

EmptyEngineNext dibangun dengan teknologi :

* [NodeJS] - Evented I/O for the backend
* [AdonisJS] - NodeJS Framework
* [ReactJS] - ReactJS is an open-source JavaScript framework for building user interfaces
* [Redis] - Redis, RED-iss is an in-memory data structure
* [Elasticsearch] - Elasticsearch is a search engine based on Lucene
* [Bootstrap] - Great UI boilerplate for modern web apps
* [jQuery] - Write less, do more

### Installation

EmptyEngineNext membutuhkan [Node.js](https://nodejs.org/) v8+ untuk berjalan normal.

Ambil source dari git :

```sh
$ cd emptyenginenext
$ git init
$ git remote add origin https://itptgue@bitbucket.org/it-ptgue/emptyenginenext.git
$ git pull origin development
```

Pasang kebutuhan awal :

```sh
$ npm install -g @adonisjs/cli
$ npm install --save
```

Menjalankan project :

Development
```sh
$ npm run dev
```

Build Production
```sh
$ npm run build
```

Production
```sh
$ npm run start
```

### Team

Product Owner :

* Javear Pendang

IT Team :

* Jenuar Dalapang
* Bayu D Rieswanto
* Arief Hidayat

QA Team :

* Sugeng Triyono
* Migel Tongkukut

UI/UX Team :

* Gabriela A. J

Data & Support Team :

* Willy Gunadi
* Jennifer
* Fandy Darsono