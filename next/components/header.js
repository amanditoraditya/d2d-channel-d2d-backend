import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import crypto from 'crypto'
import { Offline, Online } from 'react-detect-offline'
import AuthServices from '../components/auth'
import _lo from 'lodash'

const goaConfig = require('../config')
const Auth = new AuthServices()

export default class Header extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			user: null,
			is_admin:false,
			channelid: null,
		}
		
		this.logout = this.logout.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		this.setState({
			user: Auth.getProfile(),
			is_admin: Auth.isAdmin()
		})
	}
	
	logout() {
		localStorage.removeItem('user')
		localStorage.removeItem('current_channel')
		Router.push({
			pathname: '/logout'
		})
	}
	componentDidUpdate(prevProps) {
		// Typical usage (don't forget to compare props):
		this.setUserHeaders()
	  }
	setUserHeaders() {
		let currentChannel = Auth.getCurrentChannel();

		if(_lo.isEmpty(currentChannel) == false){
			if (!this.state.is_admin) {
				if (this.state.channelid != currentChannel) {
					this.setState({
						channelid: currentChannel
					})
				}
			}
		}
	}
 
	render() {
		return (
			<div id="kt_header" className="header header-fixed">
				<div className="header-wrapper rounded-top-xl d-flex flex-grow-1 align-items-center">
					<div className="container-fluid d-flex align-items-center justify-content-end justify-content-lg-between flex-wrap">
						<div className="header-menu-wrapper header-menu-wrapper-left py-lg-2" id="kt_header_menu_wrapper">
							<div id="kt_header_menu" className="header-menu header-menu-mobile header-menu-layout-default">
								<ul className="menu-nav">
									{this.state.is_admin && 
										<li className={ this.props.selected == "dashboard" ? "menu-item menu-item-active" : "menu-item" } aria-haspopup="true">
											<Link href="/dashboard" as="/dashboard" passHref><a href="/dashboard" className="menu-link"><span className="menu-text">Dashboard</span></a></Link>
										</li>
									}
									{this.state.is_admin && 
										<li className={ this.props.selected == "setting" ? "menu-item menu-item-active" : "menu-item" } aria-haspopup="true">
											<Link href="/module/setting/index" as="/setting" passHref><a href="/setting" className="menu-link"><span className="menu-text">Setting</span></a></Link>
										</li>
									}

									{/* menu channel selalu ada */}
									<li className={ this.props.selected == "channel" ? "menu-item menu-item-submenu menu-item-rel menu-item-active" : "menu-item menu-item-submenu menu-item-rel" } data-menu-toggle="click" aria-haspopup="true">
										<a href="javascript:;" className="menu-link menu-toggle">
											<span className="menu-text">Channels</span>
											<span className="menu-desc"></span>
											<i className="menu-arrow"></i>
										</a>
										<div className="menu-submenu menu-submenu-classNameic menu-submenu-left">
											<ul className="menu-subnav">
												<li className="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
													<a href="javascript:;" className="menu-link menu-toggle">
														<span className="svg-icon menu-icon">
															<img src="/static/one/media/svg/icons/General/User.svg"/>
														</span>
														<span className="menu-text">Channels</span>
														<i className="menu-arrow"></i>
													</a>
													<div className="menu-submenu menu-submenu-classNameic menu-submenu-right">
														<ul className="menu-subnav">
															<li className="menu-item" aria-haspopup="true">
																<Link href="/module/channel/index" as="/channel" passHref>
																		<a href="/channel" className="menu-link">
																		<i className="menu-bullet menu-bullet-dot">
																			<span></span>
																		</i>
																		<span className="menu-text">List Channel</span>
																	</a>
																</Link>
															</li>
															<li className="menu-item" aria-haspopup="true">
																<Link href="/module/channel/create" as="/channel/create" passHref>
																	<a href="/channel/create" className="menu-link">
																		<i className="menu-bullet menu-bullet-dot">
																			<span></span>
																		</i>
																		<span className="menu-text">Add New</span>
																	</a>
																</Link>
															</li>
														</ul>
													</div>
												</li>
											</ul>
										</div>
									</li>
									
									<li className={ this.props.selected == "user-channel" ? "menu-item menu-item-submenu menu-item-rel menu-item-active" : "menu-item menu-item-submenu menu-item-rel" } data-menu-toggle="click" aria-haspopup="true">
										<a href="javascript:;" className="menu-link menu-toggle">
											<span className="menu-text">User Channel</span>
											<span className="menu-desc"></span>
											<i className="menu-arrow"></i>
										</a>
										<div className="menu-submenu menu-submenu-classNameic menu-submenu-left">
											<ul className="menu-subnav">
												<li className="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
													<a href="javascript:;" className="menu-link menu-toggle">
														<span className="svg-icon menu-icon">
															<img src="/static/one/media/svg/icons/General/User.svg"/>
														</span>
														<span className="menu-text">User Channel</span>
														<i className="menu-arrow"></i>
													</a>
													<div className="menu-submenu menu-submenu-classNameic menu-submenu-right">
														<ul className="menu-subnav">
															<li className="menu-item" aria-haspopup="true">
																<Link href="/module/user-channel/index" as="/user-channel" passHref>
																		<a href="/user-channel" className="menu-link">
																		<i className="menu-bullet menu-bullet-dot">
																			<span></span>
																		</i>
																		<span className="menu-text">List User Channel</span>
																	</a>
																</Link>
															</li>
															<li className="menu-item" aria-haspopup="true">
																<Link href="/module/user-channel/create" as="/user-channel/create" passHref>
																	<a href="/user-channel/create" className="menu-link">
																		<i className="menu-bullet menu-bullet-dot">
																			<span></span>
																		</i>
																		<span className="menu-text">Add New</span>
																	</a>
																</Link>
															</li>
														</ul>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li className={ this.props.selected == "menu" ? "menu-item menu-item-submenu menu-item-rel menu-item-active" : "menu-item menu-item-submenu menu-item-rel" } data-menu-toggle="click" aria-haspopup="true">
										<a href="javascript:;" className="menu-link menu-toggle">
											<span className="menu-text">Menus</span>
											<span className="menu-desc"></span>
											<i className="menu-arrow"></i>
										</a>
										<div className="menu-submenu menu-submenu-classNameic menu-submenu-left">
											<ul className="menu-subnav">
												<li className="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
													<a href="javascript:;" className="menu-link menu-toggle">
														<span className="svg-icon menu-icon">
															<img src="/static/one/media/svg/icons/General/User.svg"/>
														</span>
														<span className="menu-text">Menus</span>
														<i className="menu-arrow"></i>
													</a>
													<div className="menu-submenu menu-submenu-classNameic menu-submenu-right">
														<ul className="menu-subnav">
															<li className="menu-item" aria-haspopup="true">
																<Link href="/module/menu/index" as="/menu" passHref>
																		<a href="/menu" className="menu-link">
																		<i className="menu-bullet menu-bullet-dot">
																			<span></span>
																		</i>
																		<span className="menu-text">List Menu</span>
																	</a>
																</Link>
															</li>
															<li className="menu-item" aria-haspopup="true">
																<Link href="/module/menu/create" as="/menu/create" passHref>
																	<a href="/menu/create" className="menu-link">
																		<i className="menu-bullet menu-bullet-dot">
																			<span></span>
																		</i>
																		<span className="menu-text">Add New</span>
																	</a>
																</Link>
															</li>
														</ul>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li className={ this.props.selected == "channelroles" ? "menu-item menu-item-submenu menu-item-rel menu-item-active" : "menu-item menu-item-submenu menu-item-rel" } data-menu-toggle="click" aria-haspopup="true">
											<a href="javascript:;" className="menu-link menu-toggle">
												<span className="menu-text">Roles Management</span>
												<span className="menu-desc"></span>
												<i className="menu-arrow"></i>
											</a>
											<div className="menu-submenu menu-submenu-classNameic menu-submenu-left">
												<ul className="menu-subnav">
													<li className="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
														<a href="javascript:;" className="menu-link menu-toggle">
															<span className="svg-icon menu-icon">
																<img src="/static/one/media/svg/icons/General/User.svg"/>
															</span>
															<span className="menu-text">Roles Management</span>
															<i className="menu-arrow"></i>
														</a>
														<div className="menu-submenu menu-submenu-classNameic menu-submenu-right">
															<ul className="menu-subnav">
																<li className="menu-item" aria-haspopup="true">
																	<Link href="/module/channelroles/index" as="/channelroles" passHref>
																			<a href="/channelroles" className="menu-link">
																			<i className="menu-bullet menu-bullet-dot">
																				<span></span>
																			</i>
																			<span className="menu-text">List Roles</span>
																		</a>
																	</Link>
																</li>
																<li className="menu-item" aria-haspopup="true">
																	<Link href="/module/channelroles/create" as="/channelroles/create" passHref>
																		<a href="/channelroles/create" className="menu-link">
																			<i className="menu-bullet menu-bullet-dot">
																				<span></span>
																			</i>
																			<span className="menu-text">Add New</span>
																		</a>
																	</Link>
																</li>
															</ul>
														</div>
													</li>
												</ul>
											</div>
										</li>
									{this.state.is_admin && 
											<li className={ this.props.selected == "users" || this.props.selected == "role" ? "menu-item menu-item-submenu menu-item-rel menu-item-active" : "menu-item menu-item-submenu menu-item-rel" } data-menu-toggle="click" aria-haspopup="true">
												<a href="javascript:;" className="menu-link menu-toggle">
													<span className="menu-text">Users</span>
													<span className="menu-desc"></span>
													<i className="menu-arrow"></i>
												</a>
												<div className="menu-submenu menu-submenu-classNameic menu-submenu-left">
													<ul className="menu-subnav">
														<li className="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
															<a href="javascript:;" className="menu-link menu-toggle">
																<span className="svg-icon menu-icon">
																	<img src="/static/one/media/svg/icons/General/User.svg"/>
																</span>
																<span className="menu-text">Users</span>
																<i className="menu-arrow"></i>
															</a>
															<div className="menu-submenu menu-submenu-classNameic menu-submenu-right">
																<ul className="menu-subnav">
																	<li className="menu-item" aria-haspopup="true">
																		<Link href="/module/users/index" as="/users" passHref>
																				<a href="/users" className="menu-link">
																				<i className="menu-bullet menu-bullet-dot">
																					<span></span>
																				</i>
																				<span className="menu-text">List Users</span>
																			</a>
																		</Link>
																	</li>
																	<li className="menu-item" aria-haspopup="true">
																		<Link href="/module/users/create" as="/users/create" passHref>
																			<a href="/users/create" className="menu-link">
																				<i className="menu-bullet menu-bullet-dot">
																					<span></span>
																				</i>
																				<span className="menu-text">Add New</span>
																			</a>
																		</Link>
																	</li>
																</ul>
															</div>
														</li>
														<li className="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
															<a href="javascript:;" className="menu-link menu-toggle">
																<span className="svg-icon menu-icon">
																	<img src="/static/one/media/svg/icons/Communication/Shield-user.svg"/>
																</span>
																<span className="menu-text">Roles</span>
																<i className="menu-arrow"></i>
															</a>
															<div className="menu-submenu menu-submenu-classNameic menu-submenu-right">
																<ul className="menu-subnav">
																	<li className="menu-item" aria-haspopup="true">
																		<Link href="/module/role/index" as="/role" passHref>
																			<a href="/role" className="menu-link">
																				<i className="menu-bullet menu-bullet-dot">
																					<span></span>
																				</i>
																				<span className="menu-text">List Roles</span>
																			</a>
																		</Link>
																	</li>
																	<li className="menu-item" aria-haspopup="true">
																		<Link href="/module/role/create" as="/role/create" passHref>
																			<a href="/role/create" className="menu-link">
																				<i className="menu-bullet menu-bullet-dot">
																					<span></span>
																				</i>
																				<span className="menu-text">Add New</span>
																			</a>
																		</Link>
																	</li>
																</ul>
															</div>
														</li>
													</ul>
												</div>
											</li>
									}
									{this.state.is_admin && 
										<li className={ this.props.selected == "log" ? "menu-item menu-item-active" : "menu-item" } aria-haspopup="true">
											<Link href="/module/activity-log/index" as="/activity-log" passHref><a href="/activity-log" className="menu-link"><span className="menu-text">Log</span></a></Link>
										</li>
									}
									
									<li className="menu-item" aria-haspopup="true">
										<a href="javascript:;" className="menu-link" onClick={this.logout}><span className="menu-text">Sign Out</span></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}