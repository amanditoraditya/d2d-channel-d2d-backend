import crypto from 'crypto'

const goaConfig = require('../config')

export default class Auth {
	constructor() {
		this.loggedIn = this.loggedIn.bind(this)
		this.getProfile = this.getProfile.bind(this)
	}
	
	loggedIn() {
		const user = localStorage.getItem('user')
		
		if(user) {
			return true
		} else {
			return false
		}
	}
	isAdmin() {
		const user = localStorage.getItem('user')
		if(user) {
			let decipher = crypto.createDecipher('aes-256-cbc', goaConfig.APP_KEY)
			let decrypted = decipher.update(user, 'hex', 'utf-8')
			decrypted += decipher.final('utf-8')
			decrypted = JSON.parse(decrypted)
			return decrypted.is_admin == 1 ? true : false
		} else {
			return false
		}
	}

	getProfile() {
		const user = localStorage.getItem('user')
		
		if(user) {
			let decipher = crypto.createDecipher('aes-256-cbc', goaConfig.APP_KEY)
			let decrypted = decipher.update(user, 'hex', 'utf-8')
			decrypted += decipher.final('utf-8')
			
			return JSON.parse(decrypted)
		} else {
			return false
		}
	}

	getCurrentChannel() {
		const current_channel = localStorage.getItem('current_channel')
		
		if(current_channel) {
			let decipher = crypto.createDecipher('aes-256-cbc', goaConfig.APP_KEY)
			let decrypted = decipher.update(current_channel, 'hex', 'utf-8')
			decrypted += decipher.final('utf-8')
			
			return JSON.parse(decrypted)
		} else {
			return false
		}
	}

	setCurrentChannel(channel) {
		const current_channel = localStorage.getItem('current_channel')
		let cipher = crypto.createCipher('aes-256-cbc', goaConfig.APP_KEY)
		let crypted = cipher.update(JSON.stringify(channel), 'utf-8', 'hex')
		crypted += cipher.final('hex')

		if(current_channel) {
			localStorage.removeItem('current_channel')
		}
		
		localStorage.setItem('current_channel', crypted)

		return true
	}
}