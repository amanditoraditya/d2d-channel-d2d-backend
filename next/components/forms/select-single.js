import React from 'react'
import { connectField, filterDOMProps } from 'uniforms'
import { wrapField } from 'uniforms-bootstrap4'
import ReactSelect from 'react-select'
import AsyncSelect from 'react-select/async'
import axios from 'axios'

const goaConfig = require('../../config')
const Select2 = AsyncSelect

const filterDatas = (url, inputValue, callback) => {
	axios({
		url: goaConfig.BASE_API_URL + url + '?phrase=' + inputValue,
		method: 'GET',
		timeout: goaConfig.TIMEOUT
	}).then(function (response) {
		const items = response.data
		let options = items.map(function(item) {
			return {
				value: item.value,
				label: item.label,
			}
		})
		callback(options)
	})
}

const promiseOptions = (url, inputValue, callback) =>
	new Promise(resolve => {
		setTimeout(() => {
			resolve(filterDatas(url, inputValue, callback))
		}, 1000)
	}
)

const renderSelect = (props) => {
	const beforePromiseOptions = (e, callback) => {
		promiseOptions(props.url, e, callback)
	}

	return (
		<Select2
			className={props.className}
			clearValue={props.clearValue}
			getValue={props.getValue}
			isMulti={props.isMulti}
			selectOption={props.selectOption}
			selectProps={props.selectProps}
			setValue={props.value}
			options={props.options}
			defaultInputValue={props.defaultInputValue}
			inputValue={props.inputValue}
			defaultValue={props.defaultValue}
			value={props.value || (props.fieldType === Array ? [] : undefined)}
			isDisabled={props.disabled}
			isSearchable={props.isSearchable}
			isClearable={props.isClearable}
			name={props.name}
			tabIndex={props.tabIndex}
			autoFocus={props.autoFocus}
			placeholder={props.placeholder}
			ref={props.inputRef}
			id='select-custom'
			inputValueId={props.id}
			onChange={(option, action) =>
				props.onChange(option)
			}
			defaultOptions
			loadOptions={(query, callback) => beforePromiseOptions(query, callback)}
			theme={{
				borderRadius: 0,
				spacing: {
					baseUnit: 5.5,
				}
			}}
			height={43}
			noOptionsMessage={() => "Type to search option data..." }
		/>
	)
}

const Select = props =>
	renderSelect(props)

export default Select