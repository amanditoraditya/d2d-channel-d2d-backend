import React from 'react'
import { connectField, filterDOMProps } from 'uniforms'
import { wrapField } from 'uniforms-bootstrap4'
import InputTimePicker from 'react-time-picker/dist/entry.nostyle'

const DatePicker = props =>
	wrapField(
		props,
		<InputTimePicker
			className="form-control-time"
			disabled={props.disabled}
			format={props.format}
			onChange={date => props.onChange(date)}
			name={props.name}
			value={props.value}
			required={props.required}
		/>,
	)

DatePicker.defaultProps = {
	disabled: false,
	format: 'HH:mm',
	name: 'time',
	required: false
}

export default connectField(DatePicker)