import React from 'react'
import InputDatePicker from 'react-date-picker/dist/entry.nostyle'

const DatePicker = props =>
	<InputDatePicker
		className="form-control-date"
		disabled={props.disabled}
		format={props.format}
		onChange={date => props.onChange(date)}
		name={props.name}
		value={props.value}
		required={props.required}
		locale="en-US"
		clearIcon={props.clearIcon}
	/>

DatePicker.defaultProps = {
	disabled: false,
	format: 'y-MM-dd',
	name: 'date',
	required: false
}

export default DatePicker