import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2Field from '../components/forms/select'

const schema = {
	title: 'User Channel',
	type: 'object',
	properties: {
		// channel_id: { 
		// 	type: 'integer', 
		// 	defaultValue: 0,
		// 	description: 'channel' 
		// },
		channel_id: { 
			type: 'integer', 
			defaultValue: 0, 
			description: 'Channel', 
			uniforms: { component: Select2Field }
		},
		first_name: { type: 'string', label: 'First Name' },
		last_name: { type: 'string', label: 'Last Name' },
		email: { type: 'string', label: 'Email' },
		password: { type: 'string', label: 'Password' },
		channel_roles: { type: 'string', defaultValue: '0', label: 'Channel Role', isMulti: true, uniforms: { component: Select2Field }},
		block: { 
			type: 'integer', 
			defaultValue: 0, 
			description: 'Status' 
		},
	},
	required: ['first_name', 'last_name','email','password']
}

function createValidator(schema) {
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge