import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2Field from '../components/forms/select'

const schema = {
	title: 'Menu',
	type: 'object',
	properties: {
		name: { type: 'string', label: 'Name' },
		description: { type: 'string', label: 'Description' },
		link: { type: 'string', label: 'Link' },
		target: { type: 'string', label: 'Target' },
		status: { 
			type: 'integer', 
			defaultValue: 1, 
			description: 'Status' 
		},
		is_published: { 
			type: 'integer', 
			defaultValue: 1, 
			description: 'Published',
		},
		parent_id: { 
			description: 'Parent ID',
			type: 'integer' 
		},
		position: { 
			description: 'Position Starts from 0',
			type: 'integer',
			minimum: 0,
			maximum: 1000 
		},
	},
	required: ['name','link', 'target']
}

function createValidator(schema) {
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge