import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2Field from '../components/forms/select'

const schema = {
	title: 'Channel',
	type: 'object',
	properties: {
		channel_type_id: { type: 'string', defaultValue: '1', label: 'Channel Type', uniforms: { component: Select2Field }},
		country_code: { type: 'string', label: 'Country' },
		channel_name: { type: 'string', label: 'Channel Name' },
		is_admin: { 
			type: 'integer', 
			defaultValue: 0, 
			description: 'Admin' 
		},
		description: { type: 'string', label: 'Description' },
		status: { 
			type: 'integer', 
			defaultValue: 1, 
			description: 'Status' 
		},
		menu_id: { type: 'string', defaultValue: '1', label: 'Menus', isMulti: true, uniforms: { component: Select2Field }},
	},
	required: ['country_code', 'channel_name']
}

function createValidator(schema) {
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge