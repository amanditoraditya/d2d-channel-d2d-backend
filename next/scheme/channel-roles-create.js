import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2Field from '../components/forms/select'

const schema = {
	title: 'ChannelRoles',
	type: 'object',
	properties: {
		xid: { type: 'string', label: 'xid' },
        name: { type: 'string', label: 'Roles Name' },
		read: { type: 'string', defaultValue: '1', label: 'Read' },
		write: { type: 'string', defaultValue: '1', label: 'Write' },
		update: { type: 'string', defaultValue: '1', label: 'Update' },
		delete2: { type: 'string', defaultValue: '1', label: 'Delete' },
		channel_id: { type: 'string', defaultValue: '1', label: 'Channel', uniforms: { component: Select2Field }},
		channel_menu_id: { type: 'string', defaultValue: '1', label: 'Menu'},
		status: { type: 'string', defaultValue: '1', label: 'Status' }
	},
	required: ['name']
}

function createValidator(schema) {
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge