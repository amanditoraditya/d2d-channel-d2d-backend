import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
	render() {
		return (
			<Html dir="ltr" lang="id">
				<Head></Head>
				<body id="kt_body" className="header-mobile-fixed subheader-enabled sidebar-enabled page-loading">
					<Main />
					<NextScript />
				</body>
			</Html>
		)
	}
}