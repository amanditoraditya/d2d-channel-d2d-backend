import React from 'react'
import App from 'next/app'
import NextNprogress from 'nextjs-progressbar'
import 'pikaday/css/pikaday.css'
import 'lightpick/css/lightpick.css'
import '../../node_modules/react-date-picker/dist/DatePicker.css'
import '../../node_modules/react-calendar/dist/Calendar.css'
import '../../node_modules/react-time-picker/dist/TimePicker.css'
import '../../node_modules/react-clock/dist/Clock.css'
import '../../node_modules/react-checkbox-tree/lib/react-checkbox-tree.css'
import '../public/static/one/css/pages/login/classic/login-4.css'

export default class MyApp extends App {
	render () {
		const { Component, pageProps } = this.props
		return (
			<React.Fragment>
				<NextNprogress height="2" options={{ easing: 'ease', speed: 500 }} />
				<Component {...pageProps} />
			</React.Fragment>
		)
	}
}