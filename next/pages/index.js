import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import crypto from 'crypto'
import Head from '../components/head'
import loadjs from 'loadjs'

const goaConfig = require('../config')

export default class extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			email: '',
			password: '',
			remember: '1',
			error: false,
			errorMessage: '',
		}
		
		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}
	
	componentDidMount() {
		let user = localStorage.getItem('user')
		if (user) {
			Router.push({
				pathname: '/dashboard'
			})
		} else {
			this.setState({
				login: 1
			})
		}
		
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
			'../../../../static/one/js/pages/custom/login/login-general.js'
		])
	}
	
	handleChange(e) {
		this.setState({
			error: false
		})
		this.setState({ [e.target.name]: e.target.value })
	}
	
	handleSubmit(e) {
		e.preventDefault()
		
		let self = this
		$('.error-bar').delay(1000).show()
		console.log({
			email: this.state.email,
			password: this.state.password,
			remember: this.state.remember
		})
		axios({
			url: goaConfig.BASE_API_URL + '/login',
			method: 'POST',
			data: {
				email: this.state.email,
				password: this.state.password,
				remember: this.state.remember
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let cipher = crypto.createCipher('aes-256-cbc', goaConfig.APP_KEY)
				let crypted = cipher.update(JSON.stringify(response.data.data), 'utf-8', 'hex')
				crypted += cipher.final('hex')
				
				localStorage.setItem('user', crypted)
				
				if (document.referrer != '') {
					if (document.referrer.indexOf(window.location.host) !== -1) {
						Router.back()
					} else {
						Router.push({
							pathname: '/dashboard'
						})
					}
				} else {
					Router.push({
						pathname: '/dashboard'
					})
				}
			} else {
				self.setState({
					error: true,
					errorMessage: response.data.message
				})
				$('.error-bar').delay(2000).fadeOut()
			}
		})
	}
	
	// handleSubmitByName() {
	// 	let self = this
	// 	$('.error-bar').delay(1000).show()
		
	// 	axios({
	// 		url: goaConfig.BASE_API_URL + '/login-by-name',
	// 		method: 'POST',
	// 		data: {
	// 			email: this.state.email,
	// 			password: this.state.password,
	// 			remember: this.state.remember
	// 		},
	// 		timeout: goaConfig.TIMEOUT
	// 	}).then(function (response) {
	// 		if (response.data.code == '2000') {
	// 			let cipher = crypto.createCipher('aes-256-cbc', goaConfig.APP_KEY)
	// 			let crypted = cipher.update(JSON.stringify(response.data.data), 'utf-8', 'hex')
	// 			crypted += cipher.final('hex')
				
	// 			localStorage.setItem('user', crypted)
				
	// 			if (document.referrer != '') {
	// 				if (document.referrer.indexOf(window.location.host) !== -1) {
	// 					Router.back()
	// 				} else {
	// 					Router.push({
	// 						pathname: '/dashboard'
	// 					})
	// 				}
	// 			} else {
	// 				Router.push({
	// 					pathname: '/dashboard'
	// 				})
	// 			}
	// 		} else {
	// 			self.setState({
	// 				error: true,
	// 				errorMessage: response.data.message
	// 			})
	// 			$('.error-bar').delay(2000).fadeOut()
	// 		}
	// 	})
	// }
	
	render() {
		return (
			<div>
				{this.state.login == 1 ?
					<React.Fragment>
						<Head title="Login" />
						
						<div className="d-flex flex-column flex-root">
							<div className="login login-4 login-signin-on d-flex flex-row-fluid" id="kt_login">
								<div className="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat" style={{ backgroundImage: 'url("/static/one/media/bg/bg-3.jpg")' }}>
									<div className="login-form text-center p-7 position-relative overflow-hidden">
										<div className="d-flex flex-center mt-15 mb-15">
											<a href="./"><img src="/static/one/media/logos/logo-letter-13.png" className="max-h-75px" alt="" /></a>
										</div>
										
										<div className="login-signin">
											<div className="mb-20">
												<h3>Sign In To Admin</h3>
												<div className="text-muted font-weight-bold">Enter your details to login to your account:</div>
											</div>
											<form className="form" id="kt_login_signin_form" onSubmit={this.handleSubmit}>
												{ this.state.error &&
													<div className="alert alert-solid alert-danger error-bar">{ this.state.errorMessage }</div>
												}
												<div className="form-group mb-5">
													<input className="form-control h-auto form-control-solid py-4 px-8" type="text" placeholder="Email" name="email" autoComplete="off" value={this.state.email} onChange={this.handleChange} />
												</div>
												<div className="form-group mb-5">
													<input className="form-control h-auto form-control-solid py-4 px-8" type="password" placeholder="Password" name="password" value={this.state.password} onChange={this.handleChange} />
												</div>
												<div className="form-group d-flex flex-wrap justify-content-between align-items-center">
													<div className="checkbox-inline">
														<label className="checkbox m-0 text-muted">
														<input type="checkbox" name="remember" />
														<span></span>Remember me</label>
													</div>
													<a href="javascript:;" id="kt_login_forgot" className="text-muted text-hover-primary">Forget Password ?</a>
												</div>
												<button type="submit" id="kt_login_signin_submit_not_fire" className="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">Sign In</button>
											</form>
											<div className="mt-10">
												<span className="opacity-70 mr-4">Don't have an account yet?</span>
												<a href="javascript:;" id="kt_login_signup" className="text-muted text-hover-primary font-weight-bold">Sign Up!</a>
											</div>
										</div>
										
										<div className="login-signup">
											<div className="mb-20">
												<h3>Sign Up</h3>
												<div className="text-muted font-weight-bold">Enter your details to create your account</div>
											</div>
											<form className="form" id="kt_login_signup_form">
												<div className="form-group mb-5">
													<input className="form-control h-auto form-control-solid py-4 px-8" type="text" placeholder="Fullname" name="fullname" />
												</div>
												<div className="form-group mb-5">
													<input className="form-control h-auto form-control-solid py-4 px-8" type="text" placeholder="Email" name="email" autoComplete="off" />
												</div>
												<div className="form-group mb-5">
													<input className="form-control h-auto form-control-solid py-4 px-8" type="password" placeholder="Password" name="password" />
												</div>
												<div className="form-group mb-5">
													<input className="form-control h-auto form-control-solid py-4 px-8" type="password" placeholder="Confirm Password" name="cpassword" />
												</div>
												<div className="form-group mb-5 text-left">
													<div className="checkbox-inline">
														<label className="checkbox m-0">
														<input type="checkbox" name="agree" />
														<span></span>I Agree the
														<a href="./" className="font-weight-bold ml-1">terms and conditions</a>.</label>
													</div>
													<div className="form-text text-muted text-center"></div>
												</div>
												<div className="form-group d-flex flex-wrap flex-center mt-10">
													<button id="kt_login_signup_submit" className="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2">Sign Up</button>
													<button id="kt_login_signup_cancel" className="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-2">Cancel</button>
												</div>
											</form>
										</div>
										
										<div className="login-forgot">
											<div className="mb-20">
												<h3>Forgotten Password ?</h3>
												<div className="text-muted font-weight-bold">Enter your email to reset your password</div>
											</div>
											<form className="form" id="kt_login_forgot_form">
												<div className="form-group mb-10">
													<input className="form-control form-control-solid h-auto py-4 px-8" type="text" placeholder="Email" name="email" autoComplete="off" />
												</div>
												<div className="form-group d-flex flex-wrap flex-center mt-10">
													<button id="kt_login_forgot_submit" className="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2">Request</button>
													<button id="kt_login_forgot_cancel" className="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-2">Cancel</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</React.Fragment>
				:
					<React.Fragment>
						<Head title="Login" />
					</React.Fragment>
				}
			</div>
		)
	}
}