import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Schema from '../../../scheme/menu-create'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			statusEnum: [
				{
					label: 'Active',
					value: 1
				},{
					label: 'Disabled',
					value: 0
				}
			],
			tfEnum: [
				{
					label: 'Yes',
					value: 1
				},{
					label: 'No',
					value: 0
				}
			],
			info: false,
			infoStatus: '',
			infoMessage: '',
			status: 1,
			is_published: 1,
			icon: '',
			icon_image: '',
		}
		
		this.changeRole 		= this.changeChannelType.bind(this)
		this.handleSubmit 		= this.handleSubmit.bind(this)
		this.flashInfo 			= this.flashInfo.bind(this)
		this.changeStatus 		= this.changeStatus.bind(this)
		this.changeImages 		= this.changeImages.bind(this)
		this.removeImage 		= this.removeImage.bind(this)
		this.changeIsPublished 	= this.changeIsPublished.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			this.setState({
				login: 1
			})
		}
		
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
    }
	
	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()
				
		data.remove_image		= self.state.remove_image
		data.status			 	= self.state.status
		data.is_published		= self.state.is_published

		const data_form = new FormData()
		data_form.append('data', JSON.stringify(data))
		data_form.append('logo', self.state.logo)
		axios({
			url: goaConfig.BASE_API_URL + '/menu/create',
			method: 'POST',
			data: data_form,
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.flashInfo('success', response.data.message)
				
				Router.push('/module/menu/index', '/menu')
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}
	
	changeChannelType(e) {
		this.setState({
			channel_type_id: e.value
		})
	}
	changeStatus(e) {
		this.setState({
			status: e
		})
	}
	changeIsPublished(e) {
		this.setState({
			is_published: e
		})
	}
	changeImages(e) {
		let self = this
		
		let icon = this.state.icon
		self.setState({
			icon: e.target.files[0]
		})
		
		this.setState({ 
			icon: e.target.files[0], 
			icon_image: URL.createObjectURL(e.target.files[0])
		})
	}
	removeImage(e) {
		e.preventDefault()
		this.setState({ 
			remove_image: true,
			icon_image: ''
		})
	}
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Tambah Menu" selected="perusahaan" module="Menu" permission="create">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Add Menu</h3>
										</div>
										<div className="card-toolbar">
											<Link href="/module/menu/index" as="/menu" passHref>
												<a href="/menu" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										<AutoForm schema={Schema} onSubmit={this.handleSubmit}>
											<div className="row mg-b-15">
												<div className="col-md-12 text-capitalize">
													<ErrorsField />
												</div>
												<div className="col-md-6">
													<AutoField name="name" />
												</div>
												{/* <div className="col-md-6">
													<AutoField name="icon" />
												</div> */}
												<div className="col-md-6">
													<AutoField name="description" />
												</div>
												<div className="col-md-6">
													<AutoField name="link" />
												</div>
												<div className="col-md-6">
													<AutoField name="target" />
												</div>
												<div className="col-md-6">
													<AutoField name="parent_id" />
												</div>
												<div className="col-md-6">
													<AutoField name="position" />
												</div>
												<div className="col-md-6">
													<SelectField name="status" value={this.state.status} options={this.state.statusEnum} placeholder="Select Status" onChange={(e) => this.changeStatus(e)}/>
												</div>
												<div className="col-md-6">
													<SelectField name="is_published" value={this.state.is_published} options={this.state.tfEnum} placeholder="Select Published" onChange={(e) => this.changeIsPublished(e)}/>
												</div>
												<div className="col-md-12">
													<div className="row form-group" key="0">
														<div className="col-md-2 pr-0">
															<img src={this.state.icon_image} className="img-fluid img-thumbnail" />
														</div>
														<div className="col-md-1">
															<button className="btn py-0 px-0" onClick={this.removeImage}>
																<span className="navi-icon">
																	<i className="flaticon2-cross icon-nm"></i>
																</span>
															</button>
														</div>

														<div className="col-md-9">
															<label className="text-capitalize">Icon</label>
															<div className="custom-file">
																<input type="file" name="icon" className="custom-file-input" id={'customFile'} onChange={(e) => this.changeImages(e)} />
																<label className="custom-file-label custom-file-label-primary" htmlFor={'customFile'}>{this.state.icon == null ? 'Choose File...' : this.state.icon.name }</label>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div className="form-layout-footer">
												<SubmitField value="Submit" />
											</div>
										</AutoForm>
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}