import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Schema from '../../../scheme/channel-create'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			statusEnum: [
				{
					label: 'Active',
					value: 1
				},{
					label: 'Disabled',
					value: 0
				}
			],
			status: 1,
			countryEnum: [
				{
					label: 'Indonesia',
					value: 'ID'
				},
				{
					label: 'Cambodia',
					value: 'KH'
				},
				{
					label: 'Myanmar',
					value: 'MM'
				},
				{
					label: 'Philippines',
					value: 'PH'
				},
			],
			role: '',
			logo: null,
			info: false,
			infoStatus: '',
			infoMessage: '',
			menu_id: [],
		}
		
		this.changeRole = this.changeChannelType.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
		this.changeImages = this.changeImages.bind(this)
		this.removeImage = this.removeImage.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			this.setState({
				login: 1
			})
		}
		
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
    }
	
	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()
		
		data.channel_type_id = this.state.channel_type_id
		data.channel_menus	 = this.state.channel_menus
		data.status			 = this.state.status
		
		const data_form = new FormData()
		data_form.append('data', JSON.stringify(data))
		data_form.append('logo', self.state.logo)
		axios({
			url: goaConfig.BASE_API_URL + '/channel/create',
			method: 'POST',
			data: data_form,
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.flashInfo('success', response.data.message)
				
				Router.push('/module/channel/index', '/channel')
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}
	
	changeChannelType(e) {
		this.setState({
			channel_type_id: e.value
		})
	}
	changeMenuID(e) {
		console.log(e)
		this.setState({
			channel_menus: e
		})
	}
	changeStatus(e) {
		this.setState({
			status: e
		})
	}
	changeImages(e) {
		let self = this
		
		let logo = this.state.logo
		console.log(e.target.files[0].name)
		self.setState({
			logo: e.target.files[0]
		})
		
		this.setState({ 
			logo: e.target.files[0], 
			logo_image: URL.createObjectURL(e.target.files[0])
		})
	}
	removeImage(e) {
		e.preventDefault()
		this.setState({ 
			remove_image: true,
			logo_image: ''
		})
	}
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Tambah Channel" selected="perusahaan" module="Channel" permission="create">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Add Channel</h3>
										</div>
										<div className="card-toolbar">
											<Link href="/module/channel/index" as="/channel" passHref>
												<a href="/channel" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										<AutoForm schema={Schema} onSubmit={this.handleSubmit}>
											<div className="row mg-b-15">
												<div className="col-md-12 text-capitalize">
													<ErrorsField />
												</div>
												{/* <div className="col-md-6">
													<AutoField name="channel_type_id" />
												</div> */}
												<div className="col-md-6">
													<AutoField name="channel_type_id" url="/channel/get-channel-type" isSearchable={true} options={[{label: '', value: ''}]} placeholder="Select Channel Type" onChange={(e) => this.changeChannelType(e)} />
												</div>
												{/* <div className="col-md-6">
													<AutoField name="country_code" />
												</div> */}
												<div className="col-md-6">
													<SelectField name="country_code" options={this.state.countryEnum} placeholder="Select Country" />
												</div>
												<div className="col-md-6">
													<AutoField name="channel_name" />
												</div>
												{/* <div className="col-md-6">
													<AutoField name="is_admin" />
												</div> */}
												<div className="col-md-6">
													<AutoField name="description" />
												</div>
												{/* <div className="col-md-6">
													<AutoField name="status" />
												</div> */}
												<div className="col-md-6">
													<SelectField name="status" value={this.state.status} options={this.state.statusEnum} placeholder="Select Status" onChange={(e) => this.changeStatus(e)}/>
												</div>
												<div className="col-md-6">
													<AutoField name="menu_id" url="/menu/get-menu" isSearchable={true} 
													options={[{label: '', value: ''}]} placeholder="Select Menus" onChange={(e) => this.changeMenuID(e)} />
												</div>
												<div className="col-md-12">
													<div className="row form-group" key="0">
														<div className="col-md-2 pr-0">
																<img src={this.state.logo_image} className="img-fluid img-thumbnail" />
															</div>
															<div className="col-md-1">
																<button className="btn py-0 px-0" onClick={this.removeImage}>
																	<span className="navi-icon">
																		<i className="flaticon2-cross icon-nm"></i>
																	</span>
																</button>
															</div>
														<div className="col-md-9">
															<label className="text-capitalize">Logo</label>
															<div className="custom-file">
																<input type="file" name="logo" className="custom-file-input" id={'customFile'} onChange={(e) => this.changeImages(e)} />
																<label className="custom-file-label custom-file-label-primary" htmlFor={'customFile'}>{this.state.logo == null ? 'Choose File...' : this.state.logo.name }</label>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div className="form-layout-footer">
												<SubmitField value="Submit" />
											</div>
										</AutoForm>
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}