import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import _lo from 'lodash'
import crypto from 'crypto'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Schema from '../../../scheme/userchannel-edit'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		let id = query ? query.id : '0'

		return { id }
	}
	
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			id: this.props.id,
			channelid: this.props.channelid,
			statusEnum: [
				{
					label: 'Active',
					value: 0
				},{
					label: 'Disabled / Blocked',
					value: 1
				}
			],
			info: false,
			infoStatus: '',
			infoMessage: '',
		}
		
		this.handleSubmit = this.handleSubmit.bind(this)
		this.changeChannelRoles = this.changeChannelRoles.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
	}
	
	async componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			await this.setState({
				login: 1,
				is_admin: Auth.isAdmin()
			})
		}
		// console.log('this.props.channelid', this.props.channelid)
		// let currentChannel = Auth.getCurrentChannel();
		// console.log('currentChannel', currentChannel)

		// if(_lo.isEmpty(currentChannel) == false){
		// 	await this.setState({
		// 		channelid: currentChannel
		// 	})
		// }
		
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
		
		axios({
			url: goaConfig.BASE_API_URL + '/user-channel/edit',
			method: 'POST',
			data: {
				id: self.state.id,
				channelid: self.state.channelid
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				console.log(response.data)
				let userChannel = response.data.data
				// console.log('userChannel.channel_roles', userChannel.channel_roles)
				// console.log('this.state.is_admin', this.state.is_admin)
				// console.log('this.state.channelid', this.state.channelid)
				// console.log('userChannel.channels[0].id', userChannel)
				self.setState({
					userChannel,
					channelid:userChannel.channel_id,
					channel_roles: userChannel.channel_roles
				})
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
    }
	
	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()
		
		// data.channel_id 		= self.state.userChannel.channel_id
		// data.first_name 		= self.state.userChannel.first_name
		// data.last_name 			= self.state.userChannel.last_name
		// data.email 				= self.state.userChannel.email
		data.status		 		= self.state.userChannel.status
		data.channel_roles		= self.state.channel_roles
		
		const data_form = new FormData()
		data_form.append('data', JSON.stringify(data))

		axios({
			url: goaConfig.BASE_API_URL + '/user-channel/update/' + self.state.id,
			method: 'POST',
			data: data_form,
			timeout: goaConfig.TIMEOUT
		}).then(async function (response) {
			if (response.data.code == '2000') {
				self.flashInfo('success', response.data.message)
				Router.push('/module/user-channel/index', '/user-channel')
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}
	
	changeChannelRoles(e) {
		this.setState({
			channel_roles: e
		})
	}

	changeChannelState(e) {
		this.setState({
			channelid: e.value
		})
	}
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Update Channel" selected="perusahaan" module="UserChannel" permission="update">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Update User Channel</h3>
										</div>
										<div className="card-toolbar">
											<Link href={"/module/user-channel/index"} 
												as={"/user-channel/"} passHref>
												<a href={"/user-channel/"} className="btn btn-sm btn-dark">
												<i className="fa fa-arrow-left"></i> Back</a>
											</Link>							
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										{ this.state.userChannel &&
											<AutoForm schema={Schema} onSubmit={this.handleSubmit}>
												<div className="row mg-b-15">
													<div className="col-md-12 text-capitalize">
														<ErrorsField />
													</div>
													{ this.state.is_admin &&
														<div className="col-md-6">
															<AutoField name="channel_id" url={"/channel/get-channels"} isSearchable={true} 
															options={this.state.userChannel.channels}
															defaultValue={this.state.userChannel.channels} 
															placeholder="Select a Channel" 
															onChange={(e) => this.changeChannelState(e)} />
														</div>
													}
													<div className="col-md-6">
														<AutoField name="first_name" initialValue={ this.state.userChannel.first_name } />
													</div>
													<div className="col-md-6">
														<AutoField name="last_name" initialValue={ this.state.userChannel.last_name } />
													</div>
													<div className="col-md-6">
														<AutoField name="email" initialValue={ this.state.userChannel.email } />
													</div>
													<div className="col-md-6">
														<TextField type="password" name="password" />
													</div>
													<div className="col-md-6">
														<SelectField name="block" value={ this.state.userChannel.block } options={this.state.statusEnum} placeholder="Select Status" 
														onChange={e => this.setState(prevState => ({userChannel: { ...prevState.userChannel, block: e.value}}))} />
													</div>
													<div className="col-md-6">
														<AutoField 
															name="channel_roles" url={"/user-channel/get-channel-role-list/"+this.state.channelid}
															isSearchable={true} 
															options={this.state.userChannel.channel_roles}
															defaultValue={this.state.userChannel.channel_roles} 
															placeholder={ 'Selected Roles' } 
															onChange={(e) => this.changeChannelRoles(e)} 
															disabled={(this.state.channelid == 'undefined')}
														/>
													</div>
												</div>
												<div className="form-layout-footer">
													<SubmitField value="Submit" />
												</div>
											</AutoForm>
										}
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}