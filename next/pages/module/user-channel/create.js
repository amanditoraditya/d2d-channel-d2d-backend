import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import _lo from 'lodash'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Schema from '../../../scheme/userchannel-create'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		let channelid = query ? query.channelid : 0

		return { channelid }
	}

	constructor(props) {
		super(props)
		
		this.state = {
			channelid: this.props.channelid,
			channel_role_id:[],
			login: 0,
			statusEnum: [
				{
					label: 'Active',
					value: 0
				},{
					label: 'Disabled / Blocked',
					value: 1
				}
			],
			role: '',
			info: false,
			infoStatus: '',
			infoMessage: '',
		}

		this.changeChannelRole = this.changeChannelRole.bind(this)
		this.changeStatusState = this.changeStatusState.bind(this)
		this.changeChannelState = this.changeChannelState.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
	}
	
	async componentDidMount() {
		let self = this
		if(Auth.loggedIn()) {
			this.setState({
				login: 1,
				is_admin: Auth.isAdmin()
			})
		}

		let currentChannel = await Auth.getCurrentChannel();
		console.log("current channel",currentChannel)
		if(currentChannel != false){
			await this.setState({
				channelid: currentChannel
			})
		}
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
    }
	
	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()

		data.channel_id 		= this.state.channelid;
		data.channel_role_id 	= this.state.channel_role_id;
		data.block 				= this.state.block;

		const data_form = new FormData()
		data_form.append('data', JSON.stringify(data))
		
		axios({
			url: goaConfig.BASE_API_URL + '/user-channel/create',
			method: 'POST',
			data: data_form,
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.flashInfo('success', response.data.message)
				
				Router.push('/module/user-channel/index', '/user-channel')
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}
	
	changeStatusState(e) {
		this.setState({
			block: e.value
		})
	}
	changeChannelState(e) {
		this.setState({
			channelid: e.value
		})
	}
	changeChannelRole(e) {
		this.setState({
			channel_role_id: e
		})
	}
	
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Tambah User Channel" selected="perusahaan" module="UserChannel" permission="create">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Add User Channel</h3>
										</div>
										<div className="card-toolbar">
											<Link href={"/module/user-channel/index" + this.state.channelid} as={"/user-channel/detail/" + this.state.channelid} 
											passHref>
												<a href={"/user-channel/detail/" + this.state.channelid} className="btn btn-sm btn-dark">
													<i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										<AutoForm schema={Schema} onSubmit={this.handleSubmit}>
											<div className="row mg-b-15">
												<div className="col-md-12 text-capitalize">
													<ErrorsField />
												</div>
												{ this.state.is_admin &&
													<div className="col-md-6">
														<AutoField name="channel_id" url={"/channel/get-channels"} isSearchable={true} 
														options={[{label: '', value: ''}]} placeholder="Select a Channel" 
														onChange={(e) => this.changeChannelState(e)} />
													</div>
												}
												{/* <div className="col-md-6">
													<AutoField name="channel_id" />
												</div> */}
												<div className="col-md-6">
													<AutoField name="first_name" />
												</div>
												<div className="col-md-6">
													<AutoField name="last_name" />
												</div>
												<div className="col-md-6">
													<AutoField name="email" />
												</div>
												<div className="col-md-6">
													<TextField type="password" name="password" />
												</div>
												<div className="col-md-6">
													<AutoField name="channel_role_id" url={"/user-channel/get-channel-role-list/"+this.state.channelid} isSearchable={true} 
													options={[{label: '', value: ''}]} placeholder="Select Channel Role" 
													disabled={(this.state.channelid == 'undefined')}
													onChange={(e) => this.changeChannelRole(e)} />
												</div>
												<div className="col-md-6">
													<SelectField name="block" value={this.state.block} options={this.state.statusEnum} placeholder="Select Status" 
													onChange={(e) => this.changeStatusState(e)}/>
												</div>
											</div>
											<div className="form-layout-footer">
												<SubmitField value="Submit" />
											</div>
										</AutoForm>
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}