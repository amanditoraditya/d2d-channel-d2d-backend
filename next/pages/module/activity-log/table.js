import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import { Modal } from 'reactstrap'
import DataTable from 'datatables.net'
import 'datatables.net-bs4'
import Select2 from 'select2'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			totalData: 0,
			idDel: 0,
			modalDel: false,
			modalDelAll: false,
		}

		this.actionEdit = this.actionEdit.bind(this)
		this.toggleDel = this.toggleDel.bind(this)
		this.toggleDelAll = this.toggleDelAll.bind(this)
		this.actionDelete = this.actionDelete.bind(this)
		this.actionDeleteAll = this.actionDeleteAll.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		$('#table-activity-log').DataTable({
			order: [[0, 'DESC']],
			columnDefs: [{
				targets: 'no-sort',
				orderable: false
			}],
			columns: [
				{ "name" : "id" },
				{ "name" : "fullname" },
				{ "name" : "access" },
				{ "name" : "ip" },
				{ "name" : "user_agent" },
				{ "name" : "browser" },
				{ "name" : "cpu" },
				{ "name" : "device" },
				{ "name" : "engine" },
				{ "name" : "os" },
				{ "name" : "url" },
				{ "name" : "method" },
				{ "name" : "param" },
				{ "name" : "body" },
				{ "name" : "response" }
			],
			stateSave: true,
			serverSide: true,
			processing: true,
			pageLength: 10,
			lengthMenu: [
				[10, 30, 50, 100, -1],
				[10, 30, 50, 100, 'All']
			],
			ajax: {
				type: 'post',
				url: goaConfig.BASE_API_URL + '/activity-log/datatable',
			},
			responsive: true,
			language: {
				paginate: {
					first: '<i class="ki ki-double-arrow-back"></i>',
					last: '<i class="ki ki-double-arrow-next"></i>',
					next: '<i class="ki ki-arrow-next"></i>',
					previous: '<i class="ki ki-arrow-back"></i>'
				}
			},
			"drawCallback": function(settings) {
				$("#titleCheck").click(function() {
					let checkedStatus = this.checked;
					$("table tbody tr td div:first-child input[type=checkbox]").each(function() {
						this.checked = checkedStatus
						if (checkedStatus == this.checked) {
							$(this).closest('table tbody tr').removeClass('table-select')
							$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
							self.setState({
								totalData: $('.table-body input[type=checkbox]:checked').length
							})
						}
						if (this.checked) {
							$(this).closest('table tbody tr').addClass('table-select')
							$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
							self.setState({
								totalData: $('.table-body input[type=checkbox]:checked').length
							})
						}
					})
				})
				
				$('table tbody tr td div:first-child input[type=checkbox]').on('click', function () {
					let checkedStatus = this.checked
					this.checked = checkedStatus
					
					if (checkedStatus == this.checked) {
						$(this).closest('table tbody tr').removeClass('table-select')
						$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
						self.setState({
							totalData: $('.table-body input[type=checkbox]:checked').length
						})
					}
					
					if (this.checked) {
						$(this).closest('table tbody tr').addClass('table-select')
						$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
						self.setState({
							totalData: $('.table-body input[type=checkbox]:checked').length
						})
					}
				})
				
				$('table tbody tr td div:first-child input[type=checkbox]').change(function() {
					$(this).closest('tr').toggleClass("table-select", this.checked)
				})
				
				$(".alertdel").click(function(){
					let id = $(this).attr("id")
					self.toggleDel()
					self.setState({
						idDel: id
					})
				})
				
				$('.btn-edit').on('click', function() {
					let href = $(this).attr('data-href')
					let as = $(this).attr('data-as')
					
					self.actionEdit(href, as)
				})
			}
		})
		
		let windowWidth = $(window).width()
		if (windowWidth < 767) {
			$('.table-wrapper').addClass('table-responsive')
		} else {
			$('.table-wrapper').removeClass('table-responsive')
		}
    }
	
	componentWillUnmount() {
		$('.data-table-wrapper').find('table').DataTable().destroy(true)
    }
	
	actionEdit(href, as) {
		Router.push(href, as)
	}
	
	toggleDel() {
		this.setState({
			modalDel: !this.state.modalDel
		})
	}
	
	toggleDelAll() {
		this.setState({
			modalDelAll: !this.state.modalDelAll
		})
	}
	
	actionDelete() {
		let self = this
		
		this.toggleDel()
		
		axios({
			url: goaConfig.BASE_API_URL + '/activity-log/delete',
			method: 'POST',
			data: {
				id: self.state.idDel
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let table = $('.table-wrapper')
					.find('table')
					.DataTable()
				table.clear().draw()
				table.state.clear()
				
				self.props.flashInfo('success', response.data.message)
			} else {
				self.props.flashInfo('error', response.data.message)
			}
		})
	}
	
	actionDeleteAll() {
		let self = this
		
		let values = $('.table-body input[type=checkbox]:checked').map(function(){
			let deldata = $(this).next()
			return deldata.val()
		}).get()
		
		this.toggleDelAll()
		
		axios({
			url: goaConfig.BASE_API_URL + '/activity-log/multidelete',
			method: 'POST',
			data: {
				totaldata: self.state.totalData,
				item: JSON.stringify(values)
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let table = $('.table-wrapper')
					.find('table')
					.DataTable()
				table.clear().draw()
				table.state.clear()
				
				self.props.flashInfo('success', response.data.message)
			} else {
				self.props.flashInfo('error', response.data.message)
			}
		})
	}
	
	render() {
		return (
			<div>
				<div className="table-responsive">
					<table id="table-activity-log" className="table table-separate table-checkable responsive nowrap">
						<thead>
							<tr>
								<th style={{ width:"30px" }}>Id</th>
								<th>User</th>
								<th>Access</th>
								<th>IP</th>
								<th>Agent</th>
								<th>Browser</th>
								<th>CPU</th>
								<th>Device</th>
								<th>Engine</th>
								<th>OS</th>
								<th>URL</th>
								<th>Method</th>
								<th>Param</th>
								<th>Body</th>
								<th>Response</th>
							</tr>
						</thead>
						<tbody className="table-body"></tbody>
					</table>
				</div>
				
				<Modal isOpen={this.state.modalDel} toggle={this.toggleDel} fade={true} centered={true}>
					<div className="modal-body text-center py-10 px-10">
						<i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
						<h4 className="text-danger mb-10">Delete Confirmation</h4>
						<p className="mb-10 mx-10">Are you sure you want to delete this data? Please confirm your choice.</p>
						<button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDelete}><i className="far fa-trash-alt"></i> Yes</button>
						<button type="button" className="btn btn-default px-15" onClick={this.toggleDel}><i className="fas fa-sign-out-alt"></i> No</button>
					</div>
				</Modal>
				
				<Modal isOpen={this.state.modalDelAll} toggle={this.toggleDelAll} fade={true} centered={true}>
					<div className="modal-body text-center py-10 px-10">
						<i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
						<h4 className="text-danger mb-10">Delete Confirmation</h4>
						<p className="mb-10 mx-10">Are you sure you want to delete all this data? Please confirm your choice.</p>
						<button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDeleteAll}><i className="far fa-trash-alt"></i> Yes</button>
						<button type="button" className="btn btn-default px-15" onClick={this.toggleDelAll}><i className="fas fa-sign-out-alt"></i> No</button>
					</div>
				</Modal>
			</div>
		)
	}
}