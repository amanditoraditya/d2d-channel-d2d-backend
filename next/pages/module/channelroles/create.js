import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField, RadioField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Schema from '../../../scheme/channel-roles-create'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		let channelid = query ? query.channelid : 0

		return { channelid }
	}

	constructor(props) {
		super(props)
		this.state = {
			login: 0,
			menuEnum: [],
			statusEnum: [
				{
					label: 'Active',
					value: '1'
				},{
					label: 'Disabled / Blocked',
					value: '0'
				}
			],
			role: '',
			info: false,
			infoStatus: '',
			infoMessage: '',
		}
		
		this.handleRead = this.handleRead.bind(this)
		this.handleWrite = this.handleWrite.bind(this)
		this.handleUpdate = this.handleUpdate.bind(this)
		this.handleDelete = this.handleDelete.bind(this)
		this.changeRole = this.changeChannelType.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			this.setState({
				login: 1,
				is_admin: Auth.isAdmin()
			})
		}

		let currentChannel = Auth.getCurrentChannel();
		if(!Auth.isAdmin())
		{
			this.setState({
			 	channel_id: currentChannel
			})
			axios.get(goaConfig.BASE_API_URL + '/channelroles/get-channel-menu')
			.then(response => {
				this.setState({menuEnum: response.data})
			});
		}

		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
    }
	
	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()
		
		data.channel_id = this.state.channel_id
		data.read = this.state.read
		data.write = this.state.write
		data.update = this.state.update
		data.delete2 = this.state.delete2
		
		console.log(data)
		axios({
			url: goaConfig.BASE_API_URL + '/channelroles/create',
			method: 'POST',
			data: data,
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.flashInfo('success', response.data.message)
				
				Router.push('/module/channelroles/index', '/channelroles')
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}
	
	changeChannelType(e) {
		this.setState({
			channel_id: e.value
		})

		axios.get(goaConfig.BASE_API_URL + '/channelroles/get-channel-menu', {params: {channel_id: e.value}})
		.then(response => {
			this.setState({menuEnum: response.data})
		});
	}

	handleRead(event) 
	{
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : 1;
		const name = target.name;

		this.setState({
		[name]: value
		});
	}

	handleWrite(event) 
	{
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : 1;
		const name = target.name;

		this.setState({
		[name]: value
		});
	}

	handleUpdate(event) 
	{
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : 1;
		const name = target.name;

		this.setState({
		[name]: value
		});
	}

	handleDelete(event) 
	{
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : 1;
		const name = target.name;

		this.setState({
		[name]: value
		});
	}

	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Tambah Channel Roles" selected="perusahaan" module="ChannelRoles" permission="create">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Add Roles</h3>
										</div>
										<div className="card-toolbar">
											<Link href="/module/channelroles/index" as="/channelroles" passHref>
												<a href="/channelroles" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										<AutoForm schema={Schema} onSubmit={this.handleSubmit}>
											<div className="row mg-b-15">
												<div className="col-md-12 text-capitalize">
													<ErrorsField />
												</div>
                                                <div className="col-md-6">
													<AutoField name="name" />
												</div>
												<div className="col-md-6">
													<SelectField name="status" options={this.state.statusEnum} placeholder="Select Status" />
												</div>
												{ this.state.is_admin &&
												<div className="col-md-6">
													<AutoField name="channel_id" url="/channelroles/get-channel" isSearchable={true} options={[{label: '', value: ''}]} placeholder="Select Channel" onChange={(e) => this.changeChannelType(e)} />
												</div>
												}
												<div className="col-md-6">
													<SelectField name="channel_menu_id" disabled={this.state.channel_id == null} isSearchable={true} options={this.state.menuEnum} placeholder="Select Menu" />
												</div>
											</div>
											<div className="row mg-b-15">
													<div className="col-md-12">
														<div className="form-group required">
															<label className="form-label">Access</label>
															<div className="table-responsive">
																<table id="table-role" className="table table-bordered" width="100%" cellSpacing="0">
																	<thead>
																		<tr>
																			<th className="text-center">Read</th>
																			<th className="text-center">Write</th>
																			<th className="text-center">Update</th>
																			<th className="text-center">Delete</th>
																		</tr>
																	</thead>
																	<tbody>
																		<td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name="read"  checked={this.state.read} type="checkbox" onChange={this.handleRead}/><span></span></label></div></td>
																		<td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name="write" checked={this.state.write} type="checkbox"  onChange={this.handleWrite}/><span></span></label></div></td>
																		<td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name="update" checked={this.state.update} type="checkbox"  onChange={this.handleUpdate}/><span></span></label></div></td>
																		<td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name="delete2" checked={this.state.delete} type="checkbox"  onChange={this.handleDelete}/><span></span></label></div></td>
																	</tbody>
																</table>
															</div>
														</div>
													</div>
											</div>
											<div className="form-layout-footer">
												<SubmitField value="Submit" />
											</div>
										</AutoForm>
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}