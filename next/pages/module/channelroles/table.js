import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import _lo from 'lodash'
import $ from 'jquery'
import { Modal } from 'reactstrap'
import DataTable from 'datatables.net'
import 'datatables.net-bs4'
import Select2 from 'select2'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			channelid:this.props.channelid,
			totalData: 0,
			idDel: 0,
			modalDel: false,
			modalDelAll: false,
		}

		this.actionEdit = this.actionEdit.bind(this)
		this.toggleDel = this.toggleDel.bind(this)
		this.toggleDelAll = this.toggleDelAll.bind(this)
		this.actionDelete = this.actionDelete.bind(this)
		this.actionDeleteAll = this.actionDeleteAll.bind(this)
	}
	
	async componentDidMount() {
		let self = this
		let is_admin = Auth.isAdmin();
		let columns = []
		let currentChannel = Auth.getCurrentChannel();

		// if(_lo.isEmpty(currentChannel) == false){
		// 	this.setState({
		// 		channelid: currentChannel
		// 	})
		// }

		if (is_admin) {
			columns = [
				{ "name" : "checkbox" },
				{ "name" : "id" },
				{ "name" : "channel_role_name" },
				{ "name" : "channel_name" },
				{ "name" : "menu_name"},
				{ "name" : "status" },
				{ "name" : "action" }
			]
			await this.setState({
				isAdmin: true
			})
		} else {
			columns = [
				{ "name" : "checkbox" },
				{ "name" : "id" },
				{ "name" : "channel_role_name" },
				{ "name" : "menu_name"},
				{ "name" : "status" },
				{ "name" : "action" }
			]
			await this.setState({
				isAdmin: false
			})
		}

		$('#table-channel-roles').DataTable({
			order: [[1, 'ASC']],
			columnDefs: [{
				targets: 'no-sort',
				orderable: false
			}],
			columns,
			stateSave: true,
			serverSide: true,
			processing: true,
			pageLength: 10,
			lengthMenu: [
				[10, 30, 50, 100, -1],
				[10, 30, 50, 100, 'All']
			],
			ajax: {
				type: 'post',
				url: goaConfig.BASE_API_URL + '/channelroles/datatable',
				data: {
					channelid: this.props.channelid
				},
			},
			responsive: true,
			language: {
				paginate: {
					first: '<i class="ki ki-double-arrow-back"></i>',
					last: '<i class="ki ki-double-arrow-next"></i>',
					next: '<i class="ki ki-arrow-next"></i>',
					previous: '<i class="ki ki-arrow-back"></i>'
				}
			},
			"drawCallback": function(settings) {
				$("#titleCheck").click(function() {
					let checkedStatus = this.checked;
					$("table tbody tr td div:first-child input[type=checkbox]").each(function() {
						this.checked = checkedStatus
						if (checkedStatus == this.checked) {
							$(this).closest('table tbody tr').removeClass('table-select')
							$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
							self.setState({
								totalData: $('.table-body input[type=checkbox]:checked').length
							})
						}
						if (this.checked) {
							$(this).closest('table tbody tr').addClass('table-select')
							$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
							self.setState({
								totalData: $('.table-body input[type=checkbox]:checked').length
							})
						}
					})
				})
				
				$('table tbody tr td div:first-child input[type=checkbox]').on('click', function () {
					let checkedStatus = this.checked
					this.checked = checkedStatus
					
					if (checkedStatus == this.checked) {
						$(this).closest('table tbody tr').removeClass('table-select')
						$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
						self.setState({
							totalData: $('.table-body input[type=checkbox]:checked').length
						})
					}
					
					if (this.checked) {
						$(this).closest('table tbody tr').addClass('table-select')
						$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
						self.setState({
							totalData: $('.table-body input[type=checkbox]:checked').length
						})
					}
				})
				
				$('table tbody tr td div:first-child input[type=checkbox]').change(function() {
					$(this).closest('tr').toggleClass("table-select", this.checked)
				})
				
				$(".alertdel").click(function(){
					let id = $(this).attr("id")
					self.toggleDel()
					self.setState({
						idDel: id
					})
				})
				
				$('.btn-edit').on('click', function() {
					let href = $(this).attr('data-href')
					let as = $(this).attr('data-as')
					
					self.actionEdit(href, as)
				})
			}
		})
		
		let windowWidth = $(window).width()
		if (windowWidth < 767) {
			$('.table-wrapper').addClass('table-responsive')
		} else {
			$('.table-wrapper').removeClass('table-responsive')
		}
    }
	
	componentWillUnmount() {
		$('.data-table-wrapper').find('table').DataTable().destroy(true)
    }
	
	actionEdit(href, as) {
		Router.push(href, as)
	}
	
	toggleDel() {
		this.setState({
			modalDel: !this.state.modalDel
		})
	}
	
	toggleDelAll() {
		this.setState({
			modalDelAll: !this.state.modalDelAll
		})
	}
	
	actionDelete() {
		let self = this
		
		this.toggleDel()
		
		axios({
			url: goaConfig.BASE_API_URL + '/channelroles/delete',
			method: 'POST',
			data: {
				id: self.state.idDel
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let table = $('.table-wrapper')
					.find('table')
					.DataTable()
				table.clear().draw()
				table.state.clear()
				
				self.props.flashInfo('success', response.data.message)
			} else {
				self.props.flashInfo('error', response.data.message)
			}
		})
	}
	
	actionDeleteAll() {
		let self = this
		
		let values = $('.table-body input[type=checkbox]:checked').map(function(){
			let deldata = $(this).next()
			
			return deldata.val()
			
		}).get()

		this.toggleDelAll()
		
		axios({
			url: goaConfig.BASE_API_URL + '/channelroles/multidelete',
			method: 'POST',
			data: {
				totaldata: self.state.totalData,
				item: JSON.stringify(values)
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let table = $('.table-wrapper')
					.find('table')
					.DataTable()
				table.clear().draw()
				table.state.clear()
				
				self.props.flashInfo('success', response.data.message)
			} else {
				self.props.flashInfo('error', response.data.message)
			}
		})
	}
	
	render() {
		return (
			<div>
				<div className="table-wrapper">
					<table id="table-channel-roles" className="table table-separate table-checkable responsive nowrap">
						<thead>
							<tr>
								<th className="no-sort" style={{ width:"10px" }}></th>
								<th style={{ width:"30px" }}>Id</th>
								<th>Role</th>
								{ this.state.isAdmin &&
									<th>Channel</th>
								}
								<th>Menu</th>
								<th>Status</th>
                                <th className="no-sort" style={{ width:"100px" }}>action</th>
							</tr>
						</thead>
						<tbody className="table-body"></tbody>
						<tfoot>
							<tr>
								<td style={{ width:"10px" }} className="text-center">
									<label className="checkbox checkbox-lg checkbox-inline">
										<input type="checkbox" id="titleCheck" />
										<span></span>
									</label>
								</td>
								<td colSpan="4">
									<button className="btn btn-sm btn-danger" type="button" onClick={this.toggleDelAll}><i className="far fa-trash-alt"></i> Delete Selected</button>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				
				<Modal isOpen={this.state.modalDel} toggle={this.toggleDel} fade={true} centered={true}>
					<div className="modal-body text-center py-10 px-10">
						<i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
						<h4 className="text-danger mb-10">Delete Confirmation</h4>
						<p className="mb-10 mx-10">Are you sure delete this data? Please confirm your choose.</p>
						<button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDelete}><i className="far fa-trash-alt"></i> Yes</button>
						<button type="button" className="btn btn-default px-15" onClick={this.toggleDel}><i className="fas fa-sign-out-alt"></i> No</button>
					</div>
				</Modal>
				
				<Modal isOpen={this.state.modalDelAll} toggle={this.toggleDelAll} fade={true} centered={true}>
					<div className="modal-body text-center py-10 px-10">
						<i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
						<h4 className="text-danger mb-10">Delete Confirmation</h4>
						<p className="mb-10 mx-10">Are you sure delete all this data? Please confirm your choose.</p>
						<button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDeleteAll}><i className="far fa-trash-alt"></i> Yes</button>
						<button type="button" className="btn btn-default px-15" onClick={this.toggleDelAll}><i className="fas fa-sign-out-alt"></i> No</button>
					</div>
				</Modal>
			</div>
		)
	}
}