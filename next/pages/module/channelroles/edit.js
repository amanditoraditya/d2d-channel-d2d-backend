import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import crypto from 'crypto'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Schema from '../../../scheme/channel-roles-edit'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		let id = query ? query.id : '0'

		return { id }
	}
	
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			id: this.props.id,
			menuEnum: [],
			channelroles: null,
			statusEnum: [
				{
					label: 'Active',
					value: '1'
				},{
					label: 'Disabled / Blocked',
					value: '0'
				}
			],
			info: false,
			infoStatus: '',
			infoMessage: '',
		}

		this.handleSubmit = this.handleSubmit.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
		this.encryptString = this.encryptString.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			this.setState({
				login: 1,
				is_admin: Auth.isAdmin()
			})
		}
		
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
		
		axios({
			url: goaConfig.BASE_API_URL + '/channelroles/edit',
			method: 'POST',
			data: {
				id: self.state.id
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let channelroles = response.data.data
				axios.get(goaConfig.BASE_API_URL + '/channelroles/get-channel-menu', {params: {channel_id:  channelroles.channel_id}})
				.then(response => {
					self.setState({menuEnum: response.data})
				});
				self.setState({
					channelroles,
					channel_menu_id: channelroles.channel_menu_id,
					channel_id: channelroles.channel_id,
					channel_role_id: channelroles.channel_role_id,
					read: channelroles.read,
					write: channelroles.write,
					update: channelroles.update,
					delete2: channelroles.delete
				})
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
    }
	
	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()

		data.channel_role_id = self.state.channel_role_id
		data.channel_id	= self.state.channel_id
		data.status		= self.state.channelroles.status
		data.channel_menu_id =  self.state.channelroles.channel_menu_id
		data.read =  self.state.read
		data.write =  self.state.write
		data.update =  self.state.update
		data.delete2 =  self.state.delete2

		console.log(data)

		axios({
			url: goaConfig.BASE_API_URL + '/channelroles/update/' + self.state.id,
			method: 'POST',
			data: data,
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.flashInfo('success', response.data.message)
				
				Router.push('/module/channelroles/index', '/channelroles')
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}
	
	changeChannelType(e) {
		this.setState({
			channel_id: e.value
		})

		axios.get(goaConfig.BASE_API_URL + '/channelroles/get-channel-menu', {params: {channel_id: e.value}})
		.then(response => {
			this.setState({menuEnum: response.data})
		});
	}

	handleRead(event) 
	{
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : 1;
		const name = target.name;

		this.setState({
		[name]: value
		});
	}

	handleWrite(event) 
	{
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : 1;
		const name = target.name;

		this.setState({
		[name]: value
		});
	}

	handleUpdate(event) 
	{
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : 1;
		const name = target.name;

		this.setState({
		[name]: value
		});
	}

	handleDelete(event) 
	{
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : 1;
		const name = target.name;

		this.setState({
		[name]: value
		});
	}
	
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	encryptString(string) {
		let cipher = crypto.createCipher('aes-256-cbc', goaConfig.APP_KEY)
		let crypted = cipher.update(string.toString(), 'utf-8', 'hex')
		crypted += cipher.final('hex')
		
		return crypted
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Update Channel Roles" selected="perusahaan" module="ChannelRoles" permission="update">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Update Roles</h3>
										</div>
										<div className="card-toolbar">
											<Link href="/module/channelroles/index" as="/channelroles" passHref>
												<a href="/channelroles" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										{ this.state.channelroles && 
											<AutoForm schema={Schema} onSubmit={this.handleSubmit}>
												<div className="row mg-b-15">
													<div className="col-md-12 text-capitalize">
														<ErrorsField />
													</div>
                                                    <div className="col-md-6">
                                                        <AutoField name="name" initialValue={ this.state.channelroles.channel_roles_name }/>
                                                    </div>
													<div className="col-md-6">
														<SelectField name="status" value={ this.state.channelroles.status } options={this.state.statusEnum} placeholder="Select Status" onChange={e => this.setState(prevState => ({channelroles: { ...prevState.channelroles, status: e}}))} />
													</div>
													{ this.state.is_admin &&
														<div className="col-md-6">
															<AutoField 
															name="channel_id" url="/channelroles/get-channel"  
															isSearchable={true} 
															options={[{label: this.state.channelroles.channel_name, value: this.state.channelroles.channel_id}]} 
															defaultValue={{label: this.state.channelroles.channel_name, value: this.state.channelroles.channel_id}} 
															placeholder={ 'Selected ' + this.state.channelroles.channel_name } 
															placeholder="Select Channel" onChange={(e) => this.changeChannelType(e)} />
														</div>
													}
													<div className="col-md-6">
														<SelectField name="channel_menu_id" value={ this.state.channelroles.channel_menu_id } options={this.state.menuEnum} placeholder="Select Menu" onChange={e => this.setState(prevState => ({channelroles: { ...prevState.channelroles, channel_menu_id: e}}))} />
													</div>
												</div>
												<div className="row mg-b-15">
													<div className="col-md-12">
														<div className="form-group required">
															<label className="form-label">Access</label>
															<div className="table-responsive">
																<table id="table-role" className="table table-bordered" width="100%" cellSpacing="0">
																	<thead>
																		<tr>
																			<th className="text-center">Read</th>
																			<th className="text-center">Write</th>
																			<th className="text-center">Update</th>
																			<th className="text-center">Delete</th>
																		</tr>
																	</thead>
																	<tbody>
																		<td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name="read" value="1" type="checkbox" defaultChecked={ this.state.read == '1' ? true : false} onChange={(e) => this.handleRead(e)}/><span></span></label></div></td>
																		<td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name="write" value="1" type="checkbox" defaultChecked={ this.state.write == '1' ? true : false} onChange={(e) => this.handleWrite(e)}/><span></span></label></div></td>
																		<td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name="update"  value="1" type="checkbox" defaultChecked={ this.state.update == '1' ? true : false} onChange={(e) => this.handleUpdate(e)}/><span></span></label></div></td>
																		<td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name="delete2" value="1" type="checkbox" defaultChecked={ this.state.delete2 == '1' ? true : false} onChange={(e) => this.handleDelete(e)}/><span></span></label></div></td>
																	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
												<div className="form-layout-footer">
													<SubmitField value="Submit" />
												</div>
											</AutoForm>
										}
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}

