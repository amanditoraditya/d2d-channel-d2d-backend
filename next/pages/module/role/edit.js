import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import crypto from 'crypto'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		let id = query ? query.id : '0'

		return { id }
	}
	
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			id: this.props.id,
			role: null,
			modulelist: null,
			info: false,
			infoStatus: '',
			infoMessage: '',
		}
		
		this.handleSubmit = this.handleSubmit.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
		this.encryptString = this.encryptString.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			this.setState({
				login: 1
			})
		}
		
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
		
		axios({
			url: goaConfig.BASE_API_URL + '/role/edit',
			method: 'POST',
			data: {
				id: self.state.id
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.setState({
					role: response.data.data.role,
					modulelist: response.data.data.files
				})
				
				$("#checkallrole").click(function(){
					$('#table-role input:checkbox').not(this).prop('checked', this.checked)
				})
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
    }
	
	handleSubmit(e) {
		e.preventDefault()
		
		let self = this
		$('.error-bar').delay(1000).show()
		
		let data = new FormData(event.target)
		
		axios({
			url: goaConfig.BASE_API_URL + '/role/update/' + self.state.id,
			method: 'POST',
			data: data,
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.flashInfo('success', response.data.message)
				
				Router.push('/module/role/index', '/role')
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}
	
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	encryptString(string) {
		let cipher = crypto.createCipher('aes-256-cbc', goaConfig.APP_KEY)
		let crypted = cipher.update(string.toString(), 'utf-8', 'hex')
		crypted += cipher.final('hex')
		
		return crypted
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Update Role" selected="perusahaan" module="Role" permission="update">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Update Role</h3>
										</div>
										<div className="card-toolbar">
											<Link href="/module/role/index" as="/role" passHref>
												<a href="/role" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										{ this.state.role &&
											<form onSubmit={this.handleSubmit}>
												<div className="row mg-b-15">
													<div className="col-md-6">
														<div className="form-group required">
															<label htmlFor="role_title">Title</label>
															<input type="text" className="form-control" name="role_title" defaultValue={ this.state.role.role_title } required="required" />
														</div>
													</div>
													<div className="col-md-6">
														<div className="form-group required">
															<label htmlFor="role_slug">Slug</label>
															<input type="text" className="form-control" name="role_slug" defaultValue={ this.state.role.role_slug } required="required" />
														</div>
													</div>
													{ this.state.modulelist &&
														<div className="col-md-12">
															<div className="form-group">
																<label className="form-label" htmlFor="role_title">Access</label>
																<div className="table-responsive">
																	<table id="table-role" className="table table-bordered" width="100%" cellSpacing="0">
																		<thead>
																			<tr>
																				<th className="text-center">Module</th>
																				<th className="text-center">Create</th>
																				<th className="text-center">Read</th>
																				<th className="text-center">Update</th>
																				<th className="text-center">Delete</th>
																			</tr>
																		</thead>
																		<tbody>
																			{this.state.modulelist.map((data, i) => {
																				return (<tr key={i}>
																					<td className="text-center"><input name={'item['+i+'][component]'} value={data.component} type="hidden" />{data.component}</td>
																					<td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name={'item['+i+'][create]'} value="1" type="checkbox" defaultChecked={ data.create == '1' ? true : false } /><span></span></label></div></td>
																					<td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name={'item['+i+'][read]'} value="1" type="checkbox" defaultChecked={ data.read == '1' ? true : false } /><span></span></label></div></td>
																					<td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name={'item['+i+'][update]'} value="1" type="checkbox" defaultChecked={ data.update == '1' ? true : false } /><span></span></label></div></td>
																					<td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name={'item['+i+'][delete]'} value="1" type="checkbox" defaultChecked={ data.delete == '1' ? true : false } /><span></span></label></div></td>
																				</tr>)
																			})}
																		</tbody>
																		<tfoot>
																			<tr>
																				<td colSpan="5" className="text-center">
																					<div className="mg-t-20 d-flex justify-content-center">
																						<label className="checkbox checkbox-lg checkbox-inline">
																							<input type="checkbox" id="checkallrole" />
																							<span></span> &nbsp; Select All
																						</label>
																					</div>
																				</td>
																			</tr>
																		</tfoot>
																	</table>
																</div>
															</div>
														</div>
													}
												</div>
												<div className="form-layout-footer">
													<button className="btn btn-primary" type="submit">Submit</button>
												</div>
											</form>
										}
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}