import React from 'react'
import Link from 'next/link'
import $ from 'jquery'
import axios from 'axios'
import crypto from 'crypto'
import Wrapper from '../../../../components/wrapper'
import AuthServices from '../../../../components/auth'
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap'
import classnames from 'classnames'
import loadjs from 'loadjs'

const goaConfig = require('../../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			generals: null,
			profiles: null,
			locals: null,
			images: null,
			mails: null,
			servers: null,
			info: false,
			infoStatus: '',
			infoMessage: '',
			activeTab: '1',
		}
		
		this.setActiveTab = this.setActiveTab.bind(this)
		this.changeGenerals = this.changeGenerals.bind(this)
		this.changeProfiles = this.changeProfiles.bind(this)
		this.changeLocals = this.changeLocals.bind(this)
		this.changeImages = this.changeImages.bind(this)
		this.changeMails = this.changeMails.bind(this)
		this.changeServers = this.changeServers.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
		this.encryptString = this.encryptString.bind(this)
		this.formatName = this.formatName.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			this.setState({
				login: 1
			})
		}
		
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/get-setting',
			method: 'POST',
			data: {
				groups: 'general'
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			self.setState({
				generals: response.data.data
			})
		})
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/get-setting',
			method: 'POST',
			data: {
				groups: 'profile'
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			self.setState({
				profiles: response.data.data
			})
		})
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/get-setting',
			method: 'POST',
			data: {
				groups: 'local'
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			self.setState({
				locals: response.data.data
			})
		})
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/get-setting',
			method: 'POST',
			data: {
				groups: 'image'
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			self.setState({
				images: response.data.data
			})
		})
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/get-setting',
			method: 'POST',
			data: {
				groups: 'mail'
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			self.setState({
				mails: response.data.data
			})
		})
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/get-setting',
			method: 'POST',
			data: {
				groups: 'server'
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			self.setState({
				servers: response.data.data
			})
		})
    }
	
	setActiveTab(tab) {
		this.setState({
			activeTab: tab
		})
	}
	
	changeGenerals(e, idx, id) {
		let self = this
		
		let generals = this.state.generals
		generals[idx]['value'] = e.target.value
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/update/' + self.encryptString(id),
			method: 'POST',
			data: {
				groups: 'general',
				options: e.target.name,
				value: e.target.value,
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
			} else {
				self.flashInfo('error', response.data.message)
			}
		}).catch(e => {
			self.flashInfo('error', e.message)
		})
		
		this.setState({ generals: generals })
	}
	
	changeProfiles(e, idx, id) {
		let self = this
		
		let profiles = this.state.profiles
		profiles[idx]['value'] = e.target.value
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/update/' + self.encryptString(id),
			method: 'POST',
			data: {
				groups: 'profile',
				options: e.target.name,
				value: e.target.value,
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
			} else {
				self.flashInfo('error', response.data.message)
			}
		}).catch(e => {
			self.flashInfo('error', e.message)
		})
		
		this.setState({ profiles: profiles })
	}
	
	changeLocals(e, idx, id) {
		let self = this
		
		let locals = this.state.locals
		locals[idx]['value'] = e.target.value
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/update/' + self.encryptString(id),
			method: 'POST',
			data: {
				groups: 'local',
				options: e.target.name,
				value: e.target.value,
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
			} else {
				self.flashInfo('error', response.data.message)
			}
		}).catch(e => {
			self.flashInfo('error', e.message)
		})
		
		this.setState({ locals: locals })
	}
	
	changeImages(e, idx, id) {
		let self = this
		
		let images = this.state.images
		
		const data = new FormData()
		data.append('groups', 'image')
		data.append('options', e.target.name)
		data.append('value', e.target.files[0])
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/update-with-file/' + self.encryptString(id),
			method: 'POST',
			data: data,
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				images[idx]['value'] = response.data.data.name
				
				self.setState({
					images: images
				})
			} else {
				self.flashInfo('error', response.data.message)
			}
		}).catch(e => {
			self.flashInfo('error', e.message)
		})
		
		this.setState({ images: images })
	}
	
	changeMails(e, idx, id) {
		let self = this
		
		let mails = this.state.mails
		mails[idx]['value'] = e.target.value
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/update/' + self.encryptString(id),
			method: 'POST',
			data: {
				groups: 'mail',
				options: e.target.name,
				value: e.target.value,
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
			} else {
				self.flashInfo('error', response.data.message)
			}
		}).catch(e => {
			self.flashInfo('error', e.message)
		})
		
		this.setState({ mails: mails })
	}
	
	changeServers(e, idx, id) {
		let self = this
		
		let servers = this.state.servers
		servers[idx]['value'] = e.target.value
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/update/' + self.encryptString(id),
			method: 'POST',
			data: {
				groups: 'server',
				options: e.target.name,
				value: e.target.value,
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
			} else {
				self.flashInfo('error', response.data.message)
			}
		}).catch(e => {
			self.flashInfo('error', e.message)
		})
		
		this.setState({ servers: servers })
	}
	
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	encryptString(string) {
		let cipher = crypto.createCipher('aes-256-cbc', goaConfig.APP_KEY)
		let crypted = cipher.update(string.toString(), 'utf-8', 'hex')
		crypted += cipher.final('hex')
		
		return crypted
	}
	
	formatName(string) {
		return string.replace(/_/g, ' ')
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Setting" selected="setup" module="Setting" permission="read">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Setting</h3>
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										<div className="mg-b-15">
											<Nav tabs className="nav-justified">
												<NavItem>
													<NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.setActiveTab('1') }}>General</NavLink>
												</NavItem>
												<NavItem>
													<NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.setActiveTab('2'); }}>Profile</NavLink>
												</NavItem>
												<NavItem>
													<NavLink className={classnames({ active: this.state.activeTab === '3' })} onClick={() => { this.setActiveTab('3'); }}>Local</NavLink>
												</NavItem>
												<NavItem>
													<NavLink className={classnames({ active: this.state.activeTab === '4' })} onClick={() => { this.setActiveTab('4'); }}>Image</NavLink>
												</NavItem>
												<NavItem>
													<NavLink className={classnames({ active: this.state.activeTab === '5' })} onClick={() => { this.setActiveTab('5'); }}>Mail</NavLink>
												</NavItem>
												<NavItem>
													<NavLink className={classnames({ active: this.state.activeTab === '6' })} onClick={() => { this.setActiveTab('6'); }}>Server</NavLink>
												</NavItem>
											</Nav>
											<TabContent activeTab={this.state.activeTab}>
												<TabPane tabId="1">
													{ this.state.generals &&
														<div className="mt-5">
															{this.state.generals.map((data, i) => {
																return (<div className="form-group" key={i}>
																	<label className="text-capitalize">{this.formatName(data.options)}</label>
																	<input type="text" className="form-control" name={data.options} value={data.value} onChange={(e) => this.changeGenerals(e, i, data.id)} />
																</div>)
															})}
														</div>
													}
												</TabPane>
												<TabPane tabId="2">
													{ this.state.profiles &&
														<div className="mt-5">
															{this.state.profiles.map((data, i) => {
																return (<div className="form-group" key={i}>
																	<label className="text-capitalize">{this.formatName(data.options)}</label>
																	<input type="text" className="form-control" name={data.options} value={data.value} onChange={(e) => this.changeProfiles(e, i, data.id)} />
																</div>)
															})}
														</div>
													}
												</TabPane>
												<TabPane tabId="3">
													{ this.state.locals &&
														<div className="mt-5">
															{this.state.locals.map((data, i) => {
																return (<div className="form-group" key={i}>
																	<label className="text-capitalize">{this.formatName(data.options)}</label>
																	<input type="text" className="form-control" name={data.options} value={data.value} onChange={(e) => this.changeLocals(e, i, data.id)} />
																</div>)
															})}
														</div>
													}
												</TabPane>
												<TabPane tabId="4">
													{ this.state.images &&
														<div className="mt-5">
															{this.state.images.map((data, i) => {
																return (<div className="row form-group" key={i}>
																	<div className="col-md-2">
																		{data.value == '' || data.value == null ?
																			<img src='/static/one/media/logos/logo-letter-13.png' className="img-fluid img-thumbnail" />
																		:
																			<img src={'/static/uploads/images/' + data.value} className="img-fluid img-thumbnail" />
																		}
																	</div>
																	<div className="col-md-10">
																		<label className="text-capitalize">{this.formatName(data.options)}</label>
																		<div className="custom-file">
																			<input type="file" name={data.options} className="custom-file-input" id={'customFile' + (i+1)} onChange={(e) => this.changeImages(e, i, data.id)} />
																			<label className="custom-file-label custom-file-label-primary" htmlFor={'customFile' + (i+1)}>Choose File...</label>
																		</div>
																	</div>
																</div>)
															})}
														</div>
													}
												</TabPane>
												<TabPane tabId="5">
													{ this.state.mails &&
														<div className="mt-5">
															{this.state.mails.map((data, i) => {
																return (<div className="form-group" key={i}>
																	<label className="text-capitalize">{this.formatName(data.options)}</label>
																	<input type="text" className="form-control" name={data.options} value={data.value} onChange={(e) => this.changeMails(e, i, data.id)} />
																</div>)
															})}
														</div>
													}
												</TabPane>
												<TabPane tabId="6">
													{ this.state.servers &&
														<div className="mt-5">
															{this.state.servers.map((data, i) => {
																return (<div className="form-group" key={i}>
																	<label className="text-capitalize">{this.formatName(data.options)}</label>
																	<input type="text" className="form-control" name={data.options} value={data.value} onChange={(e) => this.changeServers(e, i, data.id)} />
																</div>)
															})}
														</div>
													}
												</TabPane>
											</TabContent>
										</div>
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}