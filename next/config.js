const goaConfig = {
	APP_KEY: process.env.APP_KEY,
	BASE_URL: process.env.NEXT_BASE_URL,
	BASE_API_URL: process.env.NEXT_BASE_API_URL,
	TIMEOUT: process.env.NEXT_TIMEOUT,
	ONESIGNAL_APPID: process.env.ONESIGNAL_APPID,
	ONESIGNAL_APPID_SAFARI: process.env.ONESIGNAL_APPID_SAFARI,
	ENVIRONMENT: process.env.NODE_ENV
}

module.exports = goaConfig